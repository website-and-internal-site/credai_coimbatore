
$("#bankdetailsbtn").click(function() {
	$("#bankdetails").toggle();
});

$(document).ready(function(){
    $('#cgrfForm').click(function(){
        if($(this).prop("checked") == true){
           $("#cgrsubmit").removeClass("display_none");
        }
        else if($(this).prop("checked") == false){
            $("#cgrsubmit").addClass("display_none");
        }
    });
});




    $("#contactUs").validate({
        rules: {
            name: {
                required: true
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            }
        },
        messages: {
            name: {
                required: "Please Enter your Name",
            },
            phone: {
                required: "Please Enter your Mobile Number",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: base_path + "resource/ajax_redirect.php?page=contactUs",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "contact?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

   $("#applyJob").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('applyJob');
            var formData = new FormData(formname);
            if (photo != "") {
                var content = $(form).serialize();
                $.ajax({
                    url: base_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#customerImage").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: base_path + "resource/ajax_redirect.php?page=applyJob",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = base_path + "career?a=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });

            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: base_path + "resource/ajax_redirect.php?page=applyJob",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = base_path + "career?a=success";
                            return true;
                        } else {
                            $(".form-error").html(data);
                        }
                        return false;
                    }
                });
            }
            return false;
        }
    });



    $("#applyMembership").validate({
        rules: {
            company_name: {
                required: true
            }
        },
        messages: {
            company_name: {
                required: "Please Enter Company Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('applyMembership');
            var formData = new FormData(formname);
            if (photo != "") {
                var content = $(form).serialize();
                $.ajax({
                    url: base_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#customerImage").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: base_path + "resource/ajax_redirect.php?page=applyMembership",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = base_path + "register?a=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });

            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: base_path + "resource/ajax_redirect.php?page=applyMembership",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = base_path + "register?a=success";
                            return true;
                        } else {
                            $(".form-error").html(data);
                        }
                        return false;
                    }
                });
            }
            return false;
        }
    });



    $("form[name='addCGRF']").submit(function() {
    var flag = true;
    if (flag) {
        $.ajax({
            url: base_path + "resource/ajax_multiple_file.php?page=addCGRF",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();

                if (data == 1) {
                    window.location = base_path + "complaints?a=success";
                } else {
                    $(".form-error").show();
                }
            }
        });
    }
    return false;
});