(function ($) {
	"use strict";

 	 	// Sticky Header 
    	$(".sticky-on").sticky({topSpacing: 0});
 
 		// Mainmenu 
		$(".cssmenu").each(function(){
		      var title = ($(this).data('title'))? $(this).data('title') : "Menu";
		      $(this).menumaker({
		        title: title,
		        format: "multitoggle"
		    });
		})      

	 	// Home Slider One
	 	$("#js-slider-area").owlCarousel({
	        items:1,
	        loop:true,
	        autoplay:false,
	        dots: false,    
	        animateOut: 'fadeOut',
	        nav:true,
	        navText:['<img src='+IMGPATH+'slider/angle-left.png>', '<img src='+IMGPATH+'slider/angle-right.png>'],
	       	responsive:{
		        0:{ 
		            nav:false,
		             dots: true,  
		        },
		        480:{ 
		            nav:false,
		             dots: true,  
		        }, 		        
		        600:{
		            nav:false,
		            dots: true, 
		        },
		        1000:{
		            nav:true, 
		            dots: false, 
		        },
		        1920:{
		            nav:true,  
		            dots: false,
		        }
		    }
	      });		 	

	    // Home Slider Two
	 	$("#js-slider-style2").owlCarousel({
	        items:1,
	        loop:true,
	        autoplay:false,
	        dots: false,    
	        animateOut: 'fadeOut',
	        nav:true,
	        navText:['<img src="assets/images/slider/angle-left.png"/>', '<img src="assets/images/slider/angle-right.png"/>'],
	       	responsive:{
		        0:{ 
		            nav:false,
		             dots: true,  
		        },
		        480:{ 
		            nav:false,
		             dots: true,  
		        }, 		        
		        600:{
		            nav:false,
		            dots: true, 
		        },
		        1000:{
		            nav:true, 
		            dots: false, 
		        },
		        1920:{
		            nav:true,  
		            dots: false,
		        }
		    }
	      });	 

	     // Home Slider Three
	 	$("#js-slider-style3").owlCarousel({
	        items:1,
	        loop:true,
	        autoplay:false,
	        dots: false,    
	        animateOut: 'fadeOut',
	        nav:true,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],
	       	responsive:{
		        0:{ 
		            nav:false,
		             dots: true,  
		        },
		        480:{ 
		            nav:false,
		             dots: true,  
		        }, 
		        600:{
		            nav:false,
		            dots: true, 
		        },
		        1000:{
		            nav:true, 
		            dots: false, 
		        },
		        1920:{
		            nav:true,  
		            dots: false,
		        }
		    }
	      }); 
 	     
 	     // Home Slider Four
	 	$("#js-slider-style4").owlCarousel({
	        items:1,
	        loop:true,
	        autoplay:false,
	        dots: true,    
	        animateOut: 'fadeOut',
	        nav:false,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],
	       	responsive:{
		        0:{ 
		            nav:false,
		             dots: true,  
		        },
		        480:{ 
		            nav:false,
		             dots: true,  
		        }, 
		        600:{
		            nav:false,
		            dots: true, 
		        },
		        1000:{
		            nav:true, 
		            dots: false, 
		        },
		        1920:{
		            nav:true,  
		            dots: false,
		        }
		    }
	      }); 
 
  	     // Home Slider Five
	 	$("#js-slider-style5").owlCarousel({
	        items:1,
	        loop:true,
	        autoplay:false,
	        dots: false,    
	        animateOut: 'fadeOut',
	        nav: true,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],
	        responsive:{
		        0:{ 
		            nav:false,
		             dots: true,  
		        },
		        480:{ 
		            nav:false,
		             dots: true,  
		        }, 
		        600:{
		            nav:false,
		            dots: true, 
		        },
		        1000:{
		            nav:true, 
		            dots: false, 
		        },
		        1920:{
		            nav:true,  
		            dots: false,
		        }
		    }
	      }); 
 		
 		// agent Slider  
	 	$(".js-agent-sliders").owlCarousel({
	        items:3,
	        loop:true,
	        margin: 30,
	        autoplay:true,
	        autoplayTimeout:1500,
	        dots: true,    
	        animateOut: 'fadeOut',
	        nav: false,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],
	       	responsive:{
		        0:{
		            items:1,
		            nav:false
		        },
		        480:{
		            items:2,
		            nav:false
		        },
		        600:{
		            items:2,
		            nav:false
		        },
		        1000:{
		            items:4,
		            nav:false,
		            loop:false
		        },
		        1920:{
		            items:4,
		            nav:false,
		            loop:false
		        }
		    }
	      }); 
 		
 		// testimonial Slider  
	 	$(".js-testimonial-slides").owlCarousel({
	        items:1,
	        loop:true,
	        margin: 10,
	        autoplay:false,
	        dots: true,    
	        animateOut: 'fadeOut',
	        nav: false,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],
	      }); 
 		 		
 		// testimonial Slider  
	 	$(".js-service-slides").owlCarousel({
	        items:3,
	        loop:true,
	        margin: 30,
	        autoplay:false,
	        dots: true,    
	        animateOut: 'fadeOut',
	        nav: false,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],	       
	       	responsive:{
		        0:{
		            items:1, 
		        },
		        480:{
		            items:1, 
		        },
		        600:{
		            items:2, 
		        },
		        1000:{
		            items:3, 
		        }
		    }
	      });  		// testimonial Slider  
	 	$("#creative-service").owlCarousel({
	        items:4,
	        loop:true,
	        margin: 0,
	        autoplay:false,
	        dots: true,    
	        animateOut: 'fadeOut',
	        nav: false,
	        navText:['<i class="pe-7s-angle-left"></i>', '<i class="pe-7s-angle-right"></i>'],	       
	       	responsive:{
		        0:{
		            items:1, 
		        },
		        480:{
		            items:1, 
		        },
		        575:{
		            items:2, 
		        },
		        600:{
		            items:2, 
		        },
		        700:{
		            items:3, 
		        },
		        991:{
		            items:4, 
		        },
		        1000:{
		            items:4, 
		        }
		    }
	      }); 


		 // magnificPopup videos popup   
		$('.js-video-popup').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});

		// Scroll To Top
 		if ($('.scrolltop').length > 0) {
          $(".scrolltop").on("click",function(){
            $('html,body').animate({scrollTop:0},1000);
          });

          $(window).scroll(function(){
            if($(window).scrollTop() > 400){
              $(".scrolltop").fadeIn();
            }
            else{
              $(".scrolltop").fadeOut();
            }
            return false;
          });
        }

 		 // Home parallaxbg
 		$('#parallaxbg').parallax("50%", 0.5);
 
      	 
		// Magnific Popup 
		$('.js-gallary-list').magnificPopup({
			delegate: '.js-full-size-image',
			type: 'image',
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			image: {
				verticalFit: true,
				titleSrc: function(item) {
					return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>';
				}
			},
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				easing: 'ease-in-out',
				duration: 300,
				opener: function(element) {
					return element.closest('.js-gallary-list').find('img');
				}
			}
		});


  
 		 


}(jQuery));


