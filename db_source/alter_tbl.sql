		/*--Venkat 30-03-2022--*/

CREATE TABLE `venpep_credai_db`.`partners_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `title` VARCHAR(255) NOT NULL ,  `partner_image` VARCHAR(255) NOT NULL ,  `link` VARCHAR(255) NOT NULL ,  `sort_order` INT(10) NOT NULL ,  `status` INT(2) NOT NULL ,  `delete_status` INT(2) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,  `token` VARCHAR(255) NOT NULL ,  `added_by` INT(11) NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `venpep_credai_db`.`csr_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `token` VARCHAR(255) NOT NULL ,  `title` VARCHAR(255) NOT NULL ,  `description` TEXT NULL DEFAULT NULL ,  `sort_order` INT(10) NOT NULL DEFAULT '0' ,  `status` INT(2) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `venpep_credai_db`.`csr_image_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `csr_id` INT(11) NOT NULL ,  `image` VARCHAR(255) NOT NULL ,  `status` INT(100) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `venpep_credai_db`.`csr_video_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `csr_id` INT(11) NOT NULL ,  `video` VARCHAR(255) NOT NULL ,  `status` INT(2) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `venpep_credai_db`.`tree_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `token` VARCHAR(255) NOT NULL ,  `title` VARCHAR(255) NOT NULL ,  `description` TEXT NULL DEFAULT NULL ,  `sort_order` INT(10) NOT NULL DEFAULT '0' ,  `status` INT(2) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;

CREATE TABLE `venpep_credai_db`.`tree_image_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `tree_id` INT(11) NOT NULL ,  `image` VARCHAR(255) NOT NULL ,  `status` INT(100) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB

CREATE TABLE `venpep_credai_db`.`tree_video_tbl` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `tree_id` INT(11) NOT NULL ,  `video` VARCHAR(255) NOT NULL ,  `status` INT(2) NOT NULL ,  `created_at` DATETIME NOT NULL ,  `updated_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
