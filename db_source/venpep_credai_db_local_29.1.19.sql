-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2019 at 08:21 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `venpep_credai_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE IF NOT EXISTS `admin_tbl` (
`id` int(11) NOT NULL,
  `ad_token` varchar(250) NOT NULL,
  `ad_name` varchar(250) NOT NULL,
  `ad_mobile` varchar(50) NOT NULL,
  `ad_email` varchar(250) NOT NULL,
  `ad_password` varchar(250) NOT NULL,
  `ad_type` int(1) NOT NULL,
  `ad_super_admin` int(1) NOT NULL,
  `ad_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `ad_token`, `ad_name`, `ad_mobile`, `ad_email`, `ad_password`, `ad_type`, `ad_super_admin`, `ad_status`, `created_at`, `updated_at`) VALUES
(1, 'Credai Coimbatore', 'Credai Coimbatore', '9876543210', 'admin@credaicoimbatore.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 1, 1, 1, '2016-06-01 10:00:00', '2018-12-28 12:03:46');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_tbl`
--

CREATE TABLE IF NOT EXISTS `blog_category_tbl` (
`id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `blog_category_tbl`
--

INSERT INTO `blog_category_tbl` (`id`, `token`, `category`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'event-4', 'Event 4', 1, 1, '2019-01-23 12:05:45', '2019-01-04 18:19:31'),
(2, 'blog-2', 'Blog 2', 1, 1, '2019-01-05 14:14:54', '2019-01-05 14:14:54'),
(3, 'blog-3', 'Blog 3', 1, 1, '2019-01-07 18:56:40', '2019-01-07 18:56:40'),
(4, 'blog-4', 'Blog 4', 1, 1, '2019-01-08 15:24:10', '2019-01-08 15:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tbl`
--

CREATE TABLE IF NOT EXISTS `blog_tbl` (
`id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `blog_tbl`
--

INSERT INTO `blog_tbl` (`id`, `token`, `name`, `category`, `short_description`, `description`, `image`, `date`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'account-opening-validation-process', 'Account opening validation process', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '<p>Lorem ipsum dolor sit amet, his ea unum ponderum, detracto ponderum an vim. Equidem evertitur vel ea, has ad inermis principes. Mentitum appareat conceptam id quo. Vis evertitur referrentur ad, no laoreet consetetur reformidans pro, sit habeo omnes eu. </p>              <br>              <p> Iisque quaeque propriae has no. Modo blandit tincidunt ne eos, diam periculis disputationi usu cu. Ea eos suscipit expetendis. Vix ea quot modus, id novum vocibus pro. Nec autem ullum albucius no, vel ne tritani omnesque omittantur. Pro fierent hendrerit assueverit et, per quod luptatum comprehensam in.</p>              <br>              <p> No consulatu comprehensam nam. Ferri dolore sententiae te eum, nibh dictas definitiones mea ei. Nec cu augue dicunt, est id verear labitur necessitatibus. Salutandi quaerendum ex nec.</p>              <br>              <p> Cu quo audire apeirian. Dolore necessitatibus sea id, nihil mandamus an ius, per recusabo interesset reprehendunt ea. In vix autem vituperatoribus, mei sint ipsum dolorem ex, quodsi vitupera</p>', 'VWcx015IMF08012019120340blog-5.jpg', '2018-01-11', 1, 1, '2019-01-08 11:34:30', '2019-01-08 12:09:05'),
(2, 'automate-mortgage-processing-activity', 'Automate mortgage processing activity', 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '<p>Lorem ipsum dolor sit amet, his ea unum ponderum, detracto ponderum an vim. Equidem evertitur vel ea, has ad inermis principes. Mentitum appareat conceptam id quo. Vis evertitur referrentur ad, no laoreet consetetur reformidans pro, sit habeo omnes eu. </p>              <br>              <p> Iisque quaeque propriae has no. Modo blandit tincidunt ne eos, diam periculis disputationi usu cu. Ea eos suscipit expetendis. Vix ea quot modus, id novum vocibus pro. Nec autem ullum albucius no, vel ne tritani omnesque omittantur. Pro fierent hendrerit assueverit et, per quod luptatum comprehensam in.</p>              <br>              <p> No consulatu comprehensam nam. Ferri dolore sententiae te eum, nibh dictas definitiones mea ei. Nec cu augue dicunt, est id verear labitur necessitatibus. Salutandi quaerendum ex nec.</p>              <br>              <p> Cu quo audire apeirian. Dolore necessitatibus sea id, nihil mandamus an ius, per recusabo interesset reprehendunt ea. In vix autem vituperatoribus, mei sint ipsum dolorem ex, quodsi vitupera</p>', 'VfmZ7lMCfz08012019120349blog-5.jpg', '2019-01-09', 1, 1, '2019-01-08 11:35:33', '2019-01-08 12:03:49');

-- --------------------------------------------------------

--
-- Table structure for table `contact_tbl`
--

CREATE TABLE IF NOT EXISTS `contact_tbl` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `delete_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `contact_tbl`
--

INSERT INTO `contact_tbl` (`id`, `name`, `mobile`, `email`, `subject`, `message`, `delete_status`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Selva', '1234567989', 'admin@cintanatech.com', 'Sample', 'ddfdf', 0, 1, '2018-12-31 11:17:16', '2018-12-31 11:17:16'),
(5, 'Selva', '1234567989', 'ragav@venpep.com', 'Faculty Member New asileds', 'dfdfdf', 0, 1, '2018-12-31 11:17:29', '2018-12-31 11:17:29'),
(6, 'Sports', '1234567980', 'selva@webykart.com', 'Faculty Member New ileds', 'hgh', 0, 1, '2018-12-31 11:17:44', '2018-12-31 11:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `events_tbl`
--

CREATE TABLE IF NOT EXISTS `events_tbl` (
`id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `venue` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `added_by` int(11) unsigned zerofill NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `session_tbl`
--

CREATE TABLE IF NOT EXISTS `session_tbl` (
`id` int(11) NOT NULL,
  `logged_id` int(11) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `admin_type` varchar(250) NOT NULL,
  `finance_type` varchar(100) NOT NULL,
  `auth_referer` varchar(50) NOT NULL,
  `auth_medium` varchar(50) NOT NULL,
  `auth_user_agent` text NOT NULL,
  `auth_ip_address` varchar(250) NOT NULL,
  `session_in` datetime NOT NULL,
  `session_out` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `session_tbl`
--

INSERT INTO `session_tbl` (`id`, `logged_id`, `user_type`, `admin_type`, `finance_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(1, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '::1', '2018-12-28 11:44:14', '2018-12-28 11:54:17'),
(2, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '::1', '2018-12-28 11:54:32', '2018-12-28 11:54:37'),
(3, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '::1', '2018-12-28 12:02:12', '2018-12-28 17:30:28'),
(4, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '::1', '2018-12-31 10:26:57', '0000-00-00 00:00:00'),
(5, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '127.0.0.1', '2019-01-04 12:35:28', '0000-00-00 00:00:00'),
(6, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '::1', '2019-01-08 10:08:05', '2019-01-10 10:58:21'),
(7, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '::1', '2019-01-10 11:10:53', '0000-00-00 00:00:00'),
(8, 1, 'admin', '', '', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.2; rv:65.0) Gecko/20100101 Firefox/65.0', '127.0.0.1', '2019-01-17 14:55:43', '2019-01-17 14:55:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category_tbl`
--
ALTER TABLE `blog_category_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_tbl`
--
ALTER TABLE `blog_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_tbl`
--
ALTER TABLE `contact_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events_tbl`
--
ALTER TABLE `events_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_tbl`
--
ALTER TABLE `session_tbl`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog_category_tbl`
--
ALTER TABLE `blog_category_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blog_tbl`
--
ALTER TABLE `blog_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact_tbl`
--
ALTER TABLE `contact_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `events_tbl`
--
ALTER TABLE `events_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `session_tbl`
--
ALTER TABLE `session_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
