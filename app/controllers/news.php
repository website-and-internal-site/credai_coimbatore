<?php

class News extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/news', 
			array(	
				'active_menu' 	=> 'news',
				'meta_title'  	=> 'CREDAI Coimbatore | News & Events | Real estate developers in India',
				'meta_desc'     => 'Get to know the latest News and Events of CREDAI Coimbatore. CREDAI comprises of all Real estate developers in India to safeguard their rights and regulations.',
				'keywoeds' 		=> 'builders association of india, builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders, construction project management,builders association, bc contractors association, commercial real estate development, property developers in coimbatore, top builders in coimbatore, real estate agent, Real estate Coimbatore, Real estate association Coimbatore, RERA, Apartments in Coimbatore, Villa in Coimbatore, Real estate regulations',
				'scripts'		=> 'news',
				'list' 			=> $user->manageNews(),
				'event' 		=> $user->manageEvents($type="all")
			));
	}


	public function details()
	{
		$user 		= $this->model('User');
		$this->view('home/newsdetails', 
			array(	
				'active_menu' 	=> 'news',
				'meta_title'  	=> '',
				'meta_desc'     => '',
				'keywoeds' 		=> '',
				'scripts'		=> 'news',
			));
	}


	public function error()

	
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>