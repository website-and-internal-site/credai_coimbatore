<?php

class Vendors extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/vendors', 
			array(	
				'active_menu' 	=> 'vendors',
				'meta_title'  	=> 'CREDAI Coimbatore | Real estate | Coimbatore Builders Association ',
				'meta_desc'     => 'CREDAI Coimbatore - one of the top Coimbatore Builders Associations is the apex body of all registered real estate builders and developers in Coimbatore ',
				'keywoeds' 		=> 'real estate development, association of real estate developer, Real Estate Developers, Private real estate developers in India, commercial real estate development, property developers in coimbatore top builders in coimbatore, real estate agent.',
				'scripts'		=> 'vendors',
				'list' 			=> $user->manageVendor()
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>