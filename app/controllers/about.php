<?php

class About extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/about', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> 'CREDAI Coimbatore | Real estate Association | Real Estate Regulations',
				'meta_desc'     => 'CREDAI Coimbatore is a real estate association that brings all the builders under one single roof to follow Real Estate Regulations.',
				'keywoeds' 		=> 'builders association of india ,builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders, construction project management',
				'scripts'		=> 'about',
			));
	}




	public function national()
	{
		$user 		= $this->model('User');
		$this->view('home/national', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> 'CREDAI National | Builders Association of India Coimbatore',
				'meta_desc'     => 'CREDAI National is the Builders Association of India that mobilizes to sustain CREDAI through industry associations and community power.',
				'keywoeds' 		=> 'Real estate developers Associations, Real estate commercial, real estate India, real estate developer in India, real estate companies, real estate in Coimbatore',
				'scripts'		=> 'national',
			));
	}

	public function membership()
	{
		$user 		= $this->model('User');
		$this->view('home/credai_membership', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> 'CREDAI Membership | Realtor Association | Constructions Companies',
				'meta_desc'     => 'Get to know about CREDAI and the CREDAI Membership. CREDAI is a realtor association that unifies the construction companies in Coimbatore.',
				'keywoeds' 		=> 'builders association, top builders in coimbatore, Real estate association Coimbatore, RERA, partments in Coimbatore, Real estate regulations',
				'scripts'		=> 'national',
			));
	}




	public function members_of_credai()
	{
		$user 		= $this->model('User');
		$this->view('home/members_of_credai', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> "CREDAI Members | Construction companies in Coimbatore | RERA",
				'meta_desc'     => "CREDAI Members who belong to top construction companies in Coimbatore have become a vital partner in growth of Coimbatore's RERA Certified real estate.",
				'keywoeds' 		=> 'builders association of india, real estate association, realtor association, coimbatore builders and contractors association, builders association, bc contractors association, Real estate association Coimbatore',
				'scripts'		=> 'national',
				'list' 			=>  $user->manageLogo($type="members_coimbatore")
			));
	}



	public function coimbatore()
	{
		$user 		= $this->model('User');
		$this->view('home/about_coimbatore', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> 'CREDAI Coimbatore | Real estate Association | Real Estate Regulations',
				'meta_desc'     => 'CREDAI Coimbatore is a real estate association that brings all the builders under one single roof to follow Real Estate Regulations.',
				'keywoeds' 		=> 'builders association of india ,builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders, construction project management ',
				'scripts'		=> 'national',
				'list' 			=>  $user->manageLogo($type="members_coimbatore")
			));
	}


	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>