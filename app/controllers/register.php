<?php

class Register extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		if(!isset($_SESSION['add_member_key'])){
			$_SESSION['add_member_key'] = $user->generateRandomString("40");
		}
		$this->view('home/registration', 
			array(	
				'active_menu' 	=> 'contact',
				'meta_title'  	=> 'CREDAI Coimbatore Registration | Property Developers in Coimbatore',
				'meta_desc'     => 'Property Developers in Coimbatore and Real estate builders can now register their membership at CREDAI Coimbatore and be a member of CREDAI Community.',
				'keywoeds' 		=> 'builders association of india, builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders',
				'scripts'		=> 'Register'
			));
	}


	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found'
			));
	}


}

?>