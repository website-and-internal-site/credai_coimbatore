<?php

class Career extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		if(!isset($_SESSION['add_job_key'])){
			$_SESSION['add_job_key'] = $user->generateRandomString("40");
		}
		$this->view('home/career', 
			array(	
				'active_menu' 	=> 'contact',
				'meta_title'  	=> 'CREDAI Career | CREDAI Coimbatore | Real Estate Association ',
				'meta_desc'     => 'CREDAI Career offers various career opportunities at the real estate association - CREDAI Coimbatore to be a part of the growing builders community',
				'keywoeds' 		=> 'builders association of india, builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders',
				'scripts'		=> 'career',
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>