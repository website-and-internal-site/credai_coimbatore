<?php

class Insights extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/about', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> '',
				'meta_desc'     => '',
				'keywoeds' 		=> '',
				'scripts'		=> 'about',

			));
	}

	
	public function go()
	{
		$user 		= $this->model('User');
		$this->view('home/go', 
			array(	
				'active_menu' 	=> 'insights',
				'meta_title'  	=> 'CREDAI Coimbatore | Government Orders | Real estate regulations | RERA',
				'meta_desc'     => 'CREDAI Coimbatore follows government orders and real estate regulations like RERA to safeguard the interests of real estate builders and buyers.',
				'keywoeds' 		=> 'RERA, Real estate regulations,  real estate association, realtor association, builders association of india coimbatore',
				'scripts'		=> 'G.O',
				'list' 			=> $user->manageGO()
			));
	}
}

?>