<?php

class Events extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/event', 
			array(	
				'active_menu' 	=> 'event',
				'meta_title'  	=> 'CREDAI Events | CREDAI Coimbatore | Real Estate Association ',
				'meta_desc'     => 'CREDAI Coimbatore is one of the leading Real Estate Developers Associations that includes the top private real estate developers under a single umbrella.',
				'keywoeds' 		=> 'builders association of india, builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders',
				'scripts'		=> 'event',
				'list' 			=> $user->manageEvents($type="all")
			));
	}


	public function details($token="")
	{
		$user = $this->model('User');
        $check = $user->check_query(EVENTS_TBL,"id"," token='$token' ");
        if($check){
            $info 	= $user->getDetails(EVENTS_TBL,"*"," token='$token' ");
            $this->view('home/eventdetails', 
              array(
                    'active_menu' 	=> 'news',
					'meta_title'  	=> $info['meta_title'],
					'meta_desc'     => $info['meta_description'],
					'keywoeds' 		=> $info['keyword'],
					'info'			=>	$info,
					'upcomming' 	=>  $user->manageUpcomingEvents(),
					'desc' 			=>  $user->publishContent($info['description']),
                ));
        }else{
        	$user = $this->model('User');
			$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
		}	
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>