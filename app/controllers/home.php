<?php

class Home extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/index', 
			array(	
				'active_menu' 	=> 'home',
				'meta_title'  	=> 'CREDAI Coimbatore | Real Estate Developers Association',
				'meta_desc'     => 'CREDAI Coimbatore is one of the leading Real Estate Developers Associations that includes the top private real estate developers under a single umbrella.',
				'keywoeds' 		=> 'Builders Association of India, Builders in Coimbatore, Construction companies in India, Real estate association, construction project management',
				'scripts'		=> 'Home',
				'news' 			=> $user->manageNews($type="home"),
				'list' 			=> $user->manageEvents($type="home"),
				'vendor' 		=> $user->manageVendor()
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>