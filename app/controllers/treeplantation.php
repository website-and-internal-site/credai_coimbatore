<?php

class Treeplantation extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/treeplantation', 
			array(	
				'active_menu' 	=> 'insights',
				'meta_title'  	=> 'CREDAI Coimbatore | Real estate Association | Real Estate Regulations',
				'meta_desc'     => 'CREDAI Coimbatore is a real estate association that brings all the builders under one single roof to follow Real Estate Regulations.',
				'keywoeds' 		=> 'builders association of india ,builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders, construction project management',

				'list'			=> $user->getTreeDetails(),

				'scripts'		=> 'about',
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>