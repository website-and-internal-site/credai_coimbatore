<?php

class Members extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/members', 
			array(	
				'active_menu' 	=> 'about',
				'meta_title'  	=> 'CREDAI Coimbatore | Property Developers Community | RERA',
				'meta_desc'     => 'CREDAI Coimbatore represents the property developers community that includes top builders in Coimbatore with RERA certified properties.',
				'keywoeds' 		=> 'builders association of india, real estate association, realtor association, coimbatore builders and contractors association, builders association, bc contractors association, Real estate association Coimbatore',
				'scripts'		=> 'members',
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>