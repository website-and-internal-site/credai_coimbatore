<?php

class Complaints extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		if(!isset($_SESSION['add_cgrf_key'])){
			$_SESSION['add_cgrf_key'] = $user->generateRandomString("40");
		}
		$this->view('home/complaints', 
			array(	
				'active_menu' 	=> 'contact',
				'meta_title'  	=> 'CREDAI Customer Grievance Redressal Forum (CGRF) | CREDAI Coimbatore',
				'meta_desc'     => 'CREDAI Coimbatore provides an open platform for complaints at Customer Grievances Redressal Forum(CGRF). Please register your grievances & complaints here.',
				'keywoeds' 		=> 'Property developers in coimbatore,  real estate association, realtor association, top builders in coimbatore, real estate agent, Real estate Coimbatore, Real estate association Coimbatore, RERA, Real estate regulations',
				'scripts'		=> 'complaints'
			));
	}


	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found'
			));
	}


}

?>