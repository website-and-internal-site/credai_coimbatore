<?php

class Csr extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/csr', 
			array(	
				'active_menu' 	=> 'insights',
				'meta_title'  	=> 'CREDAI Coimbatore | CSR activities | Builders in Coimbatore',
				'meta_desc'     => "CREDAI Coimbatore's CSR activities involves social and charitable projects like Kerala Flood Relief Fund by joining hands with builders in Coimbatore",
				'keywoeds' 		=> 'builders association of india, builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders, construction project management,builders association.',

				'list'			=> $user->getCsrDetails(),

				'scripts'		=> 'csr',
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>