<?php

class Mou extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		$this->view('home/mou', 
			array(	
				'active_menu' 	=> 'insights',
				'meta_title'  	=> 'CREDAI Coimbatore | Partners | Builders Association Coimbatore',
				'meta_desc'     => 'Major partners of CREDAI Coimbatore - Builders Association Coimbatore include top hotel chains, hospitals & colleges.',
				'keywoeds' 		=> 'builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, coimbatore builders and contractors association, construction companies in coimbatore, india builders, real estate agent, Real estate Coimbatore, Real estate association Coimbatore, RERA',
				'scripts'		=> 'mou',

				'logo'			=> $user->managePartnersLogo(),
				'list' 			=> $user->manageMOU()
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>