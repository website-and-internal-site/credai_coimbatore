<?php

class Contact extends Controller
{
	
	public function index()
	{
		$user 		= $this->model('User');
		if(!isset($_SESSION['add_contact_key'])){
			$_SESSION['add_contact_key'] = $user->generateRandomString("40");
		}
		$this->view('home/contact', 
			array(	
				'active_menu' 	=> 'contact',
				'meta_title'  	=> 'Contact CREDAI Coimbatore | Real Estate Association in India',
				'meta_desc'     => 'Contact CREDAI Coimbatore, the real estate association in Coimbatore. CREDAI is one of the leading association that comprises real estate builders.',
				'keywoeds' 		=> 'builders association of india, builders in coimbatore, construction companies in india, real estate association, realtor association, property developers in coimbatore, builders association of india coimbatore, construction companies in coimbatore, builders association, property developers in coimbatore, top builders in coimbatore, real estate agent, Real estate Coimbatore, Real estate association Coimbatore',
				'scripts'		=> 'contact',
			));
	}

	public function error()
	{
		$user = $this->model('User');
		$this->view('home/error', 
			array(
				'meta_title'  	=> '404 Error - Page Not Found',
				'page_title'  	=> '404 Error - Page Not Found',
			));
	}
	
}

?>