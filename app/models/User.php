<?php

require_once 'Model.php';

class User extends Model
{	

	/*--------------------------------------------- 
					User Info
	----------------------------------------------*/


	// General User info for all pages

	function userInfo($id)
	{
		$today = date("Y-m-d");
		$query = "SELECT id,(ad_name) as username, ad_status FROM ".ADMIN_TBL." WHERE id ='".$id."' ";
		$exe = mysql_query($query);
		$list = mysql_fetch_assoc($exe);
		return $list;
	}




	function manageEvents($type)
  	{
  		$layout = "";
  		$today = date("Y-m-d");
  		if ($type=="home") {
  			$where = "status='1' ORDER BY date DESC LIMIT 6";
  		}else{
			$where = "status='1' ORDER BY date DESC";
  		}
	    $q = "SELECT id,name,token,description,date,status,image FROM ".EVENTS_TBL."  WHERE $where";
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$upcoming = $list['date']>$today ? "<h6 class='upcoming'>Upcoming</h6>" : "";
	    		$layout .= "<div class='col-lg-4 col-md-6'>
							    <a href='".BASEPATH."events/details/".$list['token']."'>
							        <div class='js-blog-single-item has-color'>

							          <div class='js-blog-single-thumbnail'>
							          $upcoming
							            <img alt='blog thumbnail' src='".SRCIMG.$list['image']."'>
							          </div>
							          <div class='js-blog-single-content'>
							              <span class='js-post-date'>".date("M d, Y",strtotime($list['date']))."</span>
							              <h5><a href='".BASEPATH."events/details/".$list['token']."'>".$list['name']."</a></h5>
							          </div>
							        </div>
							    </a>
							</div>";
	    		$i++;
	    	}
	    }
	    return $layout;
	}

	function manageUpcomingEvents()
  	{
  		$layout = "";	
  		$today  = date("Y-m-d");
	    $q = "SELECT id,name,token,description,date,status,image FROM ".EVENTS_TBL."  WHERE date >='$today'	";
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    		<li>
				    <div class='js-widget-recent-post'>
				        <div class='js-widget-post-thumbnail'>
				            <a href='".BASEPATH."events/details/".$list['token']."''><img alt='Thumbnail' src='".SRCIMG.$list['image']."'></a>
				        </div>
				        <div class='js-widget-post-content'>
				            <p><a href='".BASEPATH."events/details/".$list['token']."'>".$list['name']."</a></p>
				            <span class='js-post-date'>".date("M d, Y",strtotime($list['date']))."</span>
				        </div>
				    </div>
				</li>";
	    		$i++;
	    	}
	    }
	    return $layout;
	}

	// Manage Logo

	function manageLogo($type="")
  	{
  		$layout = "";
	    $q = "SELECT id,title,link,status,image,type FROM ".LOGO_TBL."  WHERE type='$type' and status = '1' ORDER BY title ASC" ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    		<div class='col-md-3'>
		           <div class='members_of_credai'>
		               	<div class='image_wrap'>
		               		<a target='_blank' href=".$list['link']."><img src='".SRCIMG.$list['image']."'></a>
		               	</div>
		               	<div class='title'>
		                 	<h2><a target='_blank' href=".$list['link'].">".$list['title']."</a> </h2>
		               	</div>
		           </div>
		        </div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}




  	function manageVendor()
  	{
  		$layout = "";
	    $q = "SELECT id,title,link,status,image,type FROM ".LOGO_TBL."  WHERE type='vendors' ORDER BY id ASC";
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$link = $list['link']!="" ? $list['link'] : BASEPATH."contact";
	    		$layout .= "
	    		<div class='col-md-2'>
				    <div class='vendor_wrap'>
				      <a href=".$link."> <img src='".SRCIMG.$list['image']."'></a>
				    </div>
				</div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	function manageNews($type="")
  	{
  		$layout = "";
  		if ($type=="home") {
  			$where = "type='news' ORDER BY date DESC LIMIT 6";
  		}else{
			$where = "type='news' ORDER BY date DESC";
  		}
	    $q = "SELECT id,title,created_at,status,image FROM ".CONTENT_TBL." C WHERE $where " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    			     <div class='col-md-4'>
				            <div class='news_grid_wrap'>
				                <h2>".$list['title']."</h2>
				                <a target='blank' class='btn btn-primary btn-sm' href='".SRCIMG.$list['image']."'>Read More</a>
				            </div>
				        </div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	function manageGO()
  	{
  		$layout = "";
  	
	    $q = "SELECT id,title,created_at,status,image,link FROM ".CONTENT_TBL." C WHERE type='go' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    			    <div class='col-md-4'>
		    			    <div class='go_item'>
							    <h2>".$list['title']."</h2>
							    <a href='".$list['link']."'  target='_blank' class='btn btn-primary btn-sm'>View</a>
							</div>
						</div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	function manageMOU()
  	{
  		$layout = "";
  	
	    $q = "SELECT id,title,created_at,status,image,link FROM ".CONTENT_TBL." C WHERE type='mou' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    			   <div class='mou_item'>
						    <p>".$list['title']."</p>
						    <a target='blank' class='btn btn-primary btn-sm' href='".SRCIMG.$list['image']."'>Know more</a>
						</div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	function managePartnersLogo()
  	{
  		$layout = "";
	    $q = "SELECT id,title,partner_image,link FROM ".PARTNERS_TBL." WHERE 1 AND status='1' AND delete_status='0' " ;
	    $query = mysql_query($q);

	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$link = (($list['link'] == "") ? "javascript:void(0);" : $list['link']);
	    		$target = (($list['link'] == "") ? "" : "_blank");
	    		$layout .= "<div class='col-md-3'>
	    						<a href='".$link."' target='".$target."'>
	    							<img src='".SRCIMG.$list['partner_image']."' alt=''>
	    						</a>
        					</div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getCsrDetails()
  	{
  		$layout = "";
  		$q = "SELECT id,title,description FROM ".CSR_TBL." WHERE 1 AND status = 1 ";
  		$query = mysql_query($q);

  		if(mysql_num_rows($query) > 0){
  			while($list = mysql_fetch_array($query)) {
  				$description = (($list['description'] !="") ? "<div class='col-md-12'>".$list['description']."</div>" : " " );
  				$layout .= " <div class='col-md-12'>
								<div class='js-section-title mb-50'>
						      		<h4>".$this->unHyphenize($list['title'])."</h4>  
						      		<div class='title-shape2'></div>
						    	</div>
							</div>
							$description 
							<div class='container'>
								<div class='row'>" ;
				$imagecount = mysql_query("SELECT count(csr_id) as imagecount From " .CSR_IMAGE_TBL. " WHERE csr_id = '".$list['id']."' ");
				$mcount = mysql_fetch_assoc($imagecount);
				
				$videocount = mysql_query("SELECT count(csr_id) as videocount From " .CSR_VIDEO_TBL. " WHERE csr_id = '".$list['id']."' ");
				$vcount = mysql_fetch_assoc($videocount);
				
				if($mcount['imagecount'] > 0 ){
					$imageq = "SELECT csr_id,image FROM ".CSR_IMAGE_TBL." WHERE csr_id = '".$list['id']."' AND  status = 1 ";
					$imagequery = mysql_query($imageq);
					if(mysql_num_rows($imagequery) > 0 ){
						while($imagelist = mysql_fetch_array($imagequery)) {
							$layout .="<div class='col-md-4'>
								           	<img style='margin-bottom: 30px; height: 250px; width: 400px;' src='".SRCIMG.$imagelist['image']."' alt=''>
								        </div>"; 
						}
					}
				}
				if($vcount['videocount'] > 0){
					$videoq = "SELECT csr_id,video FROM ".CSR_VIDEO_TBL." WHERE csr_id = '".$list['id']."' AND  status = 1 ";
					$videoquery = mysql_query($videoq);
					if(mysql_num_rows($videoquery) > 0 ){
						while($videolist = mysql_fetch_array($videoquery)) {
							$layout .="<div class='col-md-4'>
							 <video width='350px' height='250px' controls>
							  <source src='".SRCIMG.$videolist['video']."' type='video/mp4'>
							</video></div>"; 
						}
					}
				}
				$layout .="</div></div>";
  			}
  		}else{
  			$layout .="<h1>No Data found</h1>";
  		}
  		return $layout;
  	}


  	function getTreeDetails()
  	{
  		$layout = "";
  		$q = "SELECT id,title,description FROM ".TREE_TBL." WHERE 1 AND status = 1 ";
  		$query = mysql_query($q);

  		if(mysql_num_rows($query) > 0){
  			while($list = mysql_fetch_array($query)) {
  				$description = (($list['description'] !="") ? "<div class='col-md-12'>".$list['description']."</div>" : " " );
  				$layout .= " <div class='col-md-12'>
								<div class='js-section-title mb-50'>
						      		<h4>".$this->unHyphenize($list['title'])."</h4>  
						      		<div class='title-shape2'></div>
						    	</div>
							</div>
							$description 
							<div class='container'>
								<div class='row'>" ;
				$imagecount = mysql_query("SELECT count(tree_id) as imagecount From " .TREE_IMAGE_TBL. " WHERE tree_id = '".$list['id']."' ");
				$mcount = mysql_fetch_assoc($imagecount);
				
				$videocount = mysql_query("SELECT count(tree_id) as videocount From " .TREE_VIDEO_TBL. " WHERE tree_id = '".$list['id']."' ");
				$vcount = mysql_fetch_assoc($videocount);
				
				if($mcount['imagecount'] > 0 ){
					$imageq = "SELECT tree_id,image FROM ".TREE_IMAGE_TBL." WHERE tree_id = '".$list['id']."' AND  status = 1 ";
					$imagequery = mysql_query($imageq);
					if(mysql_num_rows($imagequery) > 0 ){
						while($imagelist = mysql_fetch_array($imagequery)) {
							$layout .="<div class='col-md-4'>
								           	<img style='margin-bottom: 30px; height: 250px; width: 400px;' src='".SRCIMG.$imagelist['image']."' alt=''>
								        </div>"; 
						}
					}
				}
				if($vcount['videocount'] > 0){
					$videoq = "SELECT tree_id,video FROM ".TREE_VIDEO_TBL." WHERE tree_id = '".$list['id']."' AND  status = 1 ";
					$videoquery = mysql_query($videoq);
					if(mysql_num_rows($videoquery) > 0 ){
						while($videolist = mysql_fetch_array($videoquery)) {
							$layout .="<div class='col-md-4'>
							 <video width='350px' height='250px' controls style='object-fit: initial;'>
							  <source src='".SRCIMG.$videolist['video']."' type='video/mp4'>
							</video></div>"; 
						}
					}
				}
				$layout .="</div></div>";
  			}
  		}else{
  			$layout .="<h1>No Data found</h1>";
  		}
  		return $layout;
  	}



/*End Tag*/
}