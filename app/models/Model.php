<?php


class Model
{
	
	/*--------------------------------------------- 
					Base Methods
	----------------------------------------------*/


	function __construct()
	{
		date_default_timezone_set("Asia/Calcutta");
	}
	
	function encryptPassword($data){
		return $encrypted = sha1($data);		
	}
	function decryptPassword($data){
		return $decrypted = sha1($data);
	}
	function encryptData($data){
		return $encrypted = base64_encode(base64_encode($data));		
	}
	function decryptData($data){
		return $decrypted1 = base64_decode(base64_decode($data));
	}
	function check_query($table,$column,$where){
		$query = mysql_query("SELECT $column FROM $table WHERE $where");
		$no_rows = @mysql_num_rows($query);
		return $no_rows;
	}
	function getDetails($table,$column,$where){
		$query = mysql_query("SELECT $column FROM $table WHERE $where");
		$rows = mysql_fetch_array($query);
		return $rows;
	}
	function deleteRow($table,$where)
	{
		$q = "DELETE FROM $table WHERE $where ";
		$exe = mysql_query($q);
		if($exe){
			return 1;
		}else{
			return 0;
		}
	}

	function roundUpToAny($n,$x=10) {
        return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
    }
    

    function escapeString($data)
  	{
  		$escaped = mysql_real_escape_string($data);
		//$escaped = array_map('htmlentities', $escaped);
		return $escaped;
  	}


	function cleanString($data)
  	{
      //$string = str_replace("'", "\'", $data);
      //$string = str_replace('"', '\"', $string);
      $string = trim($data);
      $string = $this->escapeString($string);
      return $string;
  	}

  	function publishContent($data)
  	{
  		 $string = str_replace("\'", "'", $data);
  		 $string = str_replace('\"', '"', $string);
  		 return $string;
  	}

  

  	function hyphenize($string) {
   		return preg_replace(
            	array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
           		array('-', ''),
              urldecode(strtolower($string))
        );
	}

	function unHyphenize($string) {
   		return ucfirst(str_replace('-', " ", $string));
	}

  	// Random String

  	function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function errorMsg($value){
		$err = "<div class='alert alert-danger alert-dismissable fade in'>
					<button class='close' data-dismiss='alert'>&times;</button >
					".$value."
				</div>";
		return $err;
	}

	/*---------------------------------------------
				Session In and Out
	----------------------------------------------*/ 

	// Get Client IP Address - Old

	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	// Get User Agent

	function get_user_agent() {
		if ( !isset( $_SERVER['HTTP_USER_AGENT'] ) )
		return '-';		
		$ua = strip_tags( html_entity_decode( $_SERVER['HTTP_USER_AGENT'] ));
		$ua = preg_replace('![^0-9a-zA-Z\':., /{}\(\)\[\]\+@&\!\?;_\-=~\*\#]!', '', $ua );			
		return substr( $ua, 0, 254 );
	}

	// Get User IP Address

	function get_IP() {
		$ip = '';
		// Precedence: if set, X-Forwarded-For > HTTP_X_FORWARDED_FOR > HTTP_CLIENT_IP > HTTP_VIA > REMOTE_ADDR
		$headers = array( 'X-Forwarded-For', 'HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP', 'HTTP_VIA', 'REMOTE_ADDR' );
		foreach( $headers as $header ) {
			if ( !empty( $_SERVER[ $header ] ) ) {
				$ip = $_SERVER[ $header ];
				break;
			}
		}		
		// headers can contain multiple IPs (X-Forwarded-For = client, proxy1, proxy2). Take first one.
		if ( strpos( $ip, ',' ) !== false )
			$ip = substr( $ip, 0, strpos( $ip, ',' ) );		
		return $ip;
	}

	// Session In

	function sessionIn($user_type,$finance_type,$id,$referer="",$medium="")
	{		
		$auth_user_agent =	$this->get_user_agent();
		$auth_ip_address =	$this->get_IP();
		$curr 	= date("Y-m-d H:i:s");
		$q 		= "INSERT INTO ".SESSION_TBL." SET logged_id ='".$id."', user_type='$user_type', auth_referer='$referer', auth_medium='$medium', auth_user_agent='$auth_user_agent', auth_ip_address='$auth_ip_address', session_in='$curr'  ";

		$exe 	= mysql_query($q);
		if ($exe) {
			return 1;
		}else{
			return 0;
		}
	}

	
	// Session Out

	function sessionOut($user_type,$id)
	{
		$today 	= date("Y-m-d");
		$curr 	= date("Y-m-d H:i:s");
		$info 	= $this->getDetails(SESSION_TBL,"id"," logged_id='".$id."' AND user_type='$user_type'  ORDER BY id DESC LIMIT 1");
		$q 		= "UPDATE ".SESSION_TBL." SET session_out='$curr' WHERE logged_id='".$id."' AND id='".$info['id']."'  ";
		$exe 	= mysql_query($q);
		if($exe) {
			return 1;
		}else{
			return 0;
		}
	}


	
	// Change the Date format

	function changeDateFormat($date)
	{
		$array = explode("/", $date);
		$new_date = $array[2]."/".$array[1]."/".$array[0];
		$date 			= date_create($new_date);
		$final_date		= date_format($date,"Y-m-d");
		return $final_date;
	}




	/*--------------------------------------------- 
					Mail Functions
	----------------------------------------------*/


  	function send_mail($sender,$sender_mail,$receiver_mail,$subject,$message,$bcc=""){

			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Port = SMTP_PORT; 
			$mail->Host = MAIL_HOST;
			$mail->SMTPAuth = true;
			$mail->Username = MAIL_USERNAME;
			$mail->Password = MAIL_PASSWORD;
			//$mail->SMTPSecure = 'tls';
			$mail->From = $sender_mail;
			$mail->FromName = COMPANY_NAME;
			$mail->addAddress($receiver_mail, '');
			$mail->AddBCC($bcc, '');
			//$mail->addReplyTo('raj.amalw@gmail.com', 'Raj Amal W');
			$mail->WordWrap = 50;
			$mail->isHTML(true);
			$mail->Subject = $subject;
			$mail->Body    = $message;
			return $mail->send();
	}




	function contactTemplate($data="")
	{
		$msg= "<table width='500' border='0' align='center' cellpadding='10' cellspacing='10' style='font-family:Arial, Helvetica, sans-serif; font-size:10pt; border:1px solid #ccc;'> ";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Name</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['name']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Email Id </td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['email']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Phone No</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['phone']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Subject</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['subject']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Message</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['message']."</td>";
		$msg.= "</tr>";
		$msg.= "</table>";
		return $msg;
	}


}


?>