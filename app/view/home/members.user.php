<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-10 offset-lg-1">
                <div class="js-breatcam-content">
                    <h2>Members of CREDAI Coimbatore</h2>
                    <!-- <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>Members of CREDAI Coimbatore</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->
  <section class="js-agent-area pb150">
    <div class="container">
      <div class="row">
        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/1.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Surender Vittel</h4>   
            <h6>President </h6>
          </div> 
         
        </div><!-- js-single-agent-item -->  
        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/2.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Madan B Lund</h4>
            <h6>Vice President</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->  
        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/8.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Rajiv Ramaswamy</h4>
            <h6>Secretary </h6>
          </div> 
         
        </div><!-- js-single-agent-item --> 
        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/4.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>D. Abhishek</h4>
            <h6>Joint Secretary </h6>
          </div> 
         
        </div><!-- js-single-agent-item -->  
        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/5.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>U. Somasundaram</h4>
            <h6>Treasurer </h6>
          </div> 
         
        </div><!-- js-single-agent-item -->  
        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/6.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>SJ Ananthakrishnan</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->

        <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/7.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>SP Ramaswamy</h4>
            <h6>MC Member</h6>
          </div> 
        </div><!-- js-single-agent-item -->
         <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/6.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>S.Manoj Krishna Kumar</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->
         <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/9.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>R Ramesh</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->
        <!--  <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/10.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4> Kalpesh Bafna</h4>
            <h6>Elect President </h6>
          </div> 
         
        </div> --><!-- js-single-agent-item -->
         <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/11.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Rajesh B Lund</h4>
            <h6>Ex Officio Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->
         <div class="js-single-agent-item col-md-3">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/12.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Ananthram</h4>
            <h6>General Manager</h6>
          </div> 
        </div><!-- js-single-agent-item --> 
      </div><!-- js-agent-sliders -->
    </div><!-- container -->
  </section><!-- js-agent-area Close -->    
<?php require_once 'includes/bottom.html'; ?>