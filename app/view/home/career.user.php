<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-8 offset-lg-2">
                <div class="js-breatcam-content">
                    <h2>Career</h2>
                   <!--  <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>Career</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->
<section class="js-contact-area alt-bg">
    <div class="container">

        <div class="row">
            <div class="col-lg-5">
                <div class="js-section-title mb-50">
                    <h2>Career</h2>
                    <div class="title-shape2"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                 <?php if(isset($_GET['a'])) { ?>
                  <div class="alert alert-success">
                      <strong>Thanks, We will get back to you soon...</strong> 
                  </div>
            <?php } ?>
                <form action="#" method="POST" id="applyJob" accept-charset="utf-8">
                  <input type="hidden" value="<?php echo $_SESSION['add_job_key'] ?>" name="fkey" id="fkey">
                  <div class="js-contat-form">
                      <div class="row">
                          <div class="col-lg-6">
                              <input type="text" name="name" placeholder="Your Name">
                          </div>
                          <div class="col-lg-6">
                              <input type="email" name="email" placeholder="Your Email" required="">
                          </div>
                      </div>
                      <textarea placeholder="Your Message" name="message"></textarea>
                      <div class="form-group">
                           <input type="file" id="cimage" name="cimage" />
                            <input type="hidden" id="customerImage" name="image">
                      </div>
                      <div class="form-group pull-right">
                          <input type="submit" value="Submit" class="btn btn-primary">
                      </div>
                  </div>
                </form>
            </div>
            <div class="col-md-5">
                <img src="<?php echo IMGPATH ?>career.jpg" alt="credai career">
        </div>
            </div>
        </div>
</section>
<?php require_once 'includes/bottom.html'; ?>