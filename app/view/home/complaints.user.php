<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-12">
                <div class="js-breatcam-content">
                    <h2>Customer Grievance Redressal Forum (CGRF)</h2>
                   <!--  <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>Customer Grievance Redressal Forum (CGRF)</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->
<section class="js-contact-area alt-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                  <?php if(isset($_GET['a'])) { ?>
                  <div class="alert alert-success">
                      <strong>Form submitted successfully..</strong> 
                  </div>
            <?php } ?>
                <div class="js-contat-form">
                    <form name="addCGRF" method="post" action="#" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $_SESSION['add_cgrf_key'] ?>" name="fkey" id="fkey">
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="js-section-title mb-50">
                                <h4>Customer Grievance Redressal Forum (CGRF)</h4>  
                                <div class="title-shape2"></div>
                                <br>
                                    <p>A Grievance submission fee of Rs. 2000/- is mandatory for consideration of your grievance.</p>
                                    <p>Please note that the fee will be refunded to you if the developer against whom you have the grievance is not a member of CREDAI - Coimbatore, as your grievance can not be handled by the CGRF Coimbatore in such a case.</p>
                                    <p>If the complaint is against a member of CREDAI Coimbatore and if the complaint is found to be worthy and genuine, CGRF will ask the developer to reimburse the same to you.</p>
                                    <p>Kindly remit the fee to CREDAI Coimbatore and attach the screen shot of the successful transaction details below while you fill the Grievance Redressal Form for us to register the grievance.<a href="#!" id='bankdetailsbtn'> Click here</a> for Bank Details</p>
                              </div>
                            </div><!-- col-lg-3 -->
                        </div>
                        <div class="row none" id="bankdetails">
                            <div class="col-md-12">
                                <h4>For CGRF Fees: </h4>
                                <br>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Account Name </th>
                                            <td>CREDAI Coimbatore</td>
                                        </tr>
                                        <tr>
                                            <th>Bank name  </th>
                                            <td>Punjab National Bank</td>
                                        </tr>
                                        <tr>
                                            <th>Account No  </th>
                                            <td>4379000108814005</td>
                                        </tr>
                                        <tr>
                                            <th>Type </th>
                                            <td>Savings</td>
                                        </tr>
                                        <tr>
                                            <th>IFSC Code </th>
                                            <td>PUNB0437900</td>
                                        </tr>
                                        <tr>
                                            <th>Branch  </th>
                                            <td>Cross cut road, Gandhipuram</td>
                                        </tr>
                                        <tr>
                                            <th>City </th>
                                            <td>Coimbatore</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row r">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location"> Customer Name <span style="color:red;">*</span></label><br>
                                    <input type="text" name="name" id="name" class="input"  required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location"> Age <span style="color:red;">*</span></label><br>
                                    <input type="text" name="age" id="age" class="input" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Email<span style="color:red;">*</span></label><br>
                                    <input type="email" name="email" class="input" id="email" required="">
                                </div>
                            </div>
                             <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Phone<span style="color:red;">*</span></label><br>
                                    <input type="text" name="phone" id="phone" class="input"  required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Landline</label><br>
                                    <input type="text" name="landline" id="landline" class="input">
                                </div>
                            </div>
                        </div>

                        <div class="row r">
                            <div class="col-lg-12 col-md-12 col-sm-12 fl-left">
                                <div class="formBlock select">
                                    <label for="location">Address for communication<span style="color:red;">*</span></label><br>
                                    <textarea name="address_comm" style="height: 54px;" class="input" id="address_comm" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row r">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Name of the Property developer<span style="color:red;">*</span></label><br>
                                    <input type="text" name="name_develop" id="name_develop" class="input"  required="required">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Name of the Project<span style="color:red;">*</span></label><br>
                                    <input type="text" class="input" name="project_name" required="required">
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="formBlock select">
                                    <label for="location">Address of the Project<span style="color:red;">*</span></label><br>
                                    <textarea style="height: 80px;" type="text" name="project_address" id="project_address" class="input" value="" size="20" required="required"></textarea>
                                </div>
                            </div>
                           
                        </div>
                        <div class="row r">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="formBlock select">
                                    <label for="location">Grievance Details
                                        <span style="color:red;">*</span></label><br>
                                    <textarea type="text" style="height: 80px;" name="grievance" id="grievance" class="input" value="" size="20" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="formBlock select">
                                    <label for="location">Whether the details of this complaint in full or in part form the subject of any law suit before any court of law / Consumer court? (If yes please give details)<span style="color:red;">*</span></label><br>
                                    <textarea type="text" style="height: 80px;" name="complaint_any_court" id="complaint_any_court" class="input" value="" size="20" required="required"></textarea>
                                </div>
                            </div>

                             <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="formBlock select">
                                    <label for="location">If your answer to question above is “yes”, are you approaching the CGRF to try for a mediation and thereby an out of court settlement?<span style="color:red;">*</span></label><br>
                                    <textarea type="text" style="height: 80px;" name="out_of_court_settlement" id="out_of_court_settlement" class="input" value="" size="20" required="required"></textarea>
                                </div>
                            </div>
                             <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="formBlock select">
                                    <label for="location">Have you lodged any Complaint / Greivance with the CGRF at CREDAI- Coimbatore or any other chapter of CREDAI earlier on the same subject on the same developer? (If yes please give details including its current status)<span style="color:red;">*</span></label><br>
                                    <textarea type="text" style="height: 80px;" name="lodged_any_complaint" id="lodged_any_complaint" class="input" value="" size="20" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row r">
                            <div class="col-lg-12 col-md-12 col-sm-6">
                                <p>a) Documents like agreements with the developer, sale deed etc. ( or Receipt(s) for payment of advance if no agreement has been entered into)</p>
                                <p>b) All correspondence with the developer relating to this grievance or relevant to this complaint in your view.</p>
                                <p>c) Grievance submission fee - successful payment transaction details screen shot</p>
                                <p>d) Any other document relevant to this grievance in your opinion.</p>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <input type="file" multiple="multiple" data-jfiler-extensions="jpg, jpeg, png, gif,pdf,doc,docx" name="files[]" id="input2">
                               <!--  <div class="tips_img_up">
                                    <h3>Note:</h3>
                                    <ul>
                                        <li> Upload Files in a set of 10.</li>
                                        <li> Upload Files in the form of pdf,doc,docx.</li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Please read the <a target="_blank" href="<?php echo IMGPATH ?>docs/cgrf.pdf">"TERMS OF WORKING OF THE CONSUMER GRIEVANCE REDRESSAL FORUM (CGRF) OF CREDAI, COIMBATORE” </a> thoroughly before proceeding further. If you have a query on the same or need any explanation, please post the same here" accompanied by a field for posting queries.</p>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value=""  id="cgrfForm">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    I/We have read and understood the document “THE TERMS OF WORKING OF THE CONSUMER GRIEVANCE REDRESSAL FORUM (CGRF) OF CREDAI, COIMBATORE"
                                  </label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <input type="submit" value="Submit" id="cgrsubmit" class="btn btn-primary display_none">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once 'includes/bottom.html'; ?>