<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>Events Details</h2>
           <!--  <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li><a href="<?php echo BASEPATH ?>events">Events</a></li>
              <li>Events Details</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->
  <div class="main-single-area pt-100 pb-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <article class="js-classic-item">
            <div class="js-blog-article-thumbnail">
              <img src="<?php echo SRCIMG.$data['info']['image']  ?>" alt="Credai Events">
            </div>
            <div class="js-js-blog-article"> 
              <ul class="list-inline ja-post-date-comment">
                <li><a href="#"><?php echo date("M d, Y",strtotime($data['info']['date'])) ?></a></li>
              </ul>
              <h3><a href="#"><?php echo $data['info']['name'] ?></a></h3>
              <p></p>
              <?php echo $data['desc'] ?>
            </div> <!-- js-js-blog-article -->
          </article><!-- article -->  
        </div><!-- col-lg-8 -->
        <div class="col-lg-4">
          <aside class="alt-bg">
             <div class="js-sidebar-widget">
              <div class="js-widget-search">
              <div class="date_time">
                  <h4>Date and Time </h4>
                  <p><?php echo date("M d, Y",strtotime($data['info']['date'])) ?>  </p> 
                  
              </div>
              <div class="location">
                  <h4>Location </h4>
                  <p>
                      <?php echo $data['info']['venue'] ?>
                  </p> 
              </div>
             </div>
            </div><!-- js-sidebar-widget -->
       
            <div class="js-sidebar-widget"> 
              <h4>Upcoming Events</h4>
              <div class="title-shape2 mb-4"></div>
              <ul class="js-widget-recent-post">
              <?php echo $data['upcomming'] ?>
              </ul>
            </div><!-- js-sidebar-widget -->
          </aside><!-- aside -->
        </div><!-- col-lg-4 -->
      </div><!-- row -->
    </div><!-- container -->
  </div><!--  main-blog-area -->

  <?php require_once 'includes/bottom.html'; ?>