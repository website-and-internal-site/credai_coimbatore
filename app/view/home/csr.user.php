<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>CSR</h2>
           <!--  <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li>CSR</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->

<section class="js-work-area alt-bg news_wrap">

	<div class="container">
		<div class="row">
			<?php echo $data['list']; ?>
		</div>
	</div>
</section> 


  <?php require_once 'includes/bottom.html'; ?>