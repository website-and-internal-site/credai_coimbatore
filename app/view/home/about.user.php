<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>About US</h2>
           <!--  <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li>About Us</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->

  <section class="js-quality-services">
    <div class="container">
      <div class="row">
      
        <div class="col-lg-12  js-management-system-area">
          <h2>Welcome to CREDAI - Coimbatore</h2>
          <div class="about_wrap">
            <p>CREDAI - Coimbatore is a registered body that represents the organized property development sector of Coimbatore city. It comprises of member organizations whose primary business is property development, and is headquartered or has projects in Coimbatore. </p>
            <p> CREDAI - Coimbatore is affiliated to Confederation of Real Estate Developers Associations of India (CREDAI), Confederation of Indian Industry (CII) and The Indian Chamber of Commerce & Industry. </p>
            <p>All members of CREDAI are required to sign and adhere to CREDAI National Code of Conduct.</p>

            <h2>Objectives of CREDAI - Coimbatore</h2>
            <ul>
              <li>To promote integrity and transparency among the Coimbatore property developer community by encouraging them to adopt fair practices in all aspects of business</li>
              <li>To provide a platform for members to network and exchange information on best practices, competencies, and the latest trends in the property development industry</li>
              <li>To facilitate the improvement in construction quality standards in Coimbatore by promoting the use of technology, latest building techniques, and environmentally-friendly materials for construction</li>
              <li>To disseminate information to members and update them on all governmental policies, procedures, and regulations that concern the property development industry</li>
              <li>To represent the property development community to government authorities in order to create and sustain an environment conducive to the growth and well-being of the industry in Coimbatore</li>
              <li>To protect and promote the interests of employees and workers of members and provide them with an improved quality of life, both at work and outside</li>
              <li>To encourage members to be responsible corporate citizens of Coimbatore city</li>
              <li>To serve as a bridge between the property development industry and the public of Coimbatore city</li>             
            </ul>
            <h2>Membership</h2>
            <p> There are two classes of membership within CREDAI - Coimbatore.</p>
            <h3>1. Permanent Membership</h3>
            <ul>
              <li>Attend, speak and vote at any General Body/Annual General Body Meetings of the Association. Each member represented by a person shall have one vote to be cast in person</li>
              <li>Participate in the activities of the Association and to get their names registered in the Register of Members maintained by the Association for permanent member</li>
              <li>Receive free of cost or on payment of charges, as may be decided in General Body Meeting/Annual General Body Meeting/Managing Committee Meeting, all the publications of the Association</li>
              <li>Use the library facilities</li>
              <li>Propose or second or vote at any election held by the Association</li>
              <li>Stand for election as a member of the Managing Committee or any other sub-committee of the Association</li>
              <li>Convene a General Body Meeting subject to the Rules and Regulations</li>
              <li>Inspect the office of the Association, the Register of Members, Minutes, accounts of the Association</li>
              <li>Get a free copy of the Annual Accounts prior to the Annual General Meeting and a copy of the Memorandum of the Association and its Bye-Laws on payment of prescribed fee</li>
              <li>Use CREDAI - Coimbatore logo and CREDAI logo in all its advertising material</li>
            </ul>
            <h2> Who can apply for permanent membership?</h2>
            <p>An associate member of CREDAI - Coimbatore can:</p>
            <ul>
              <li>Attend and speak at the Annual General Body Meeting of the Association</li>
              <li>To get their names registered in the Register of Members maintained by the Association for Associate Members </li>
              <li>Receive free of cost or on payment of charges, as may be decided in General Body Meeting/Annual General Body Meeting/Managing Committee Meeting, all the publications of the Association</li>
              <li>Use the library facilities</li>
              <li>Get a copy of the Memorandum of the Association and its Bye-Laws on payment of prescribed fee</li>
              <li>Have any other privileges as decided by the Managing Committee</li>
            </ul>

            <h3> 2. Associate Membership</h3>
            <p>An associate member of CREDAI - Coimbatore can:</p>
            <ul>
              <li>Attend and speak at the Annual General Body Meeting of the Association</li>
              <li>To get their names registered in the Register of Members maintained by the Association for Associate Members </li>
              <li>Receive free of cost or on payment of charges, as may be decided in General Body Meeting/Annual General Body Meeting/Managing Committee Meeting, all the publications of the Association</li>
              <li>Use the library facilities</li>
              <li>Get a copy of the Memorandum of the Association and its Bye-Laws on payment of prescribed fee</li>
              <li>Have any other privileges as decided by the Managing Committee</li>
            </ul>

            <p>An Associate member cannot use the logos of CREDAI - Coimbatore and CREDAI in any form or place whatsoever.</p>
            <h2> Who can apply for associate membership?</h2>
            <p> Any person / firm / company that satisfy the following criteria can apply for associate membership in CREDAI - Coimbatore:</p>
            <ul>
              <li>Place of business in Coimbatore District, and engaged or involved in the property development / construction industry</li>
              <li>Registered under the sales tax law in Tamil Nadu</li>
              <li>Proposed by two permanent members</li>
            </ul>
            <p> An associate member can apply for the permanent membership of the Association when they complete minimum three years term as an associate member of the Association. They can also apply for permanent membership of the Association when they finish construction of 1,50,000 sq.ft. (or) construct 3 projects anywhere in India or upon completion of three consecutive years in the business. </p>
          </div>
        </div><!-- col-lg-6 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-quality-services close -->

  <?php require_once 'includes/bottom.html'; ?>