<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-8 offset-lg-2">
                <div class="js-breatcam-content">
                    <h2>CREDAI National</h2>
                    <!-- <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>CREDAI National</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->


  <section class="js-contact-area alt-bg">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
        
 <h2>Welcome to CREDAI National</h2>
          <div class="about_wrap">
            <p>CREDAI is a knowledge sharing network about the latest industry data, technology advancements and industry benchmarks for its members. CREDAI Conclave and CREDAI National Convention are both annual events held over two days each which showcases latest technological, managerial and financial innovations in the real estate sector presented by leading authorities. These events draw close to 1000 developers, policy influencers and media person and thus generate rich dialogues. </p>

            <h2>Objectives of CREDAI National include:</h2>
            <ul>
              <li>To work with the Government to increase the scope of private real estate developers in urban agenda.</li>
              <li>Maintaining a strong relationship with the Government, the media, industry associates, and other stakeholders through dialogues and interactions.</li>
              <li> Provide a balanced and integrated industry viewpoint and response on critical aspects of policy, regulation, and developments related to real estate sector.</li>
              <li> To serve as a platform for members to exchange views and arrive at common understanding through meetings, conferences and other events for a favorable policy climate for real estate development.</li>
              <li>Implement CREDAI CSR initiatives.</li>
              <li>Mobilize funds to sustain CREDAI through industry associations and community power.</li>
                      
            </ul>
          
          </div>

          <p>For more information <a target="_blank" href="https://credai.org/">click here</a></p>

        </div>
      </div>
    </div>
  </section>

<?php require_once 'includes/bottom.html'; ?>