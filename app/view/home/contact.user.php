<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-8 offset-lg-2">
                <div class="js-breatcam-content">
                    <h2>Contact Us</h2>
                   <!--  <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>Contact Us</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->


  <section class="js-contact-area alt-bg">
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="js-section-title mb-50">
            <h2>Get in touch with us</h2>
            <div class="title-shape2"></div>
          </div>
        </div>
        <div class="col-md-7">
            <div class="pull-right">
              <a  href="<?php echo BASEPATH ?>complaints" target="blank" class="btn btn-primary btn-sm">CGRF</a>
              <a  href="<?php echo BASEPATH ?>register" target="blank" class="btn btn-danger btn-sm">New Member Registration</a>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-7">
           <?php if(isset($_GET['a'])) { ?>
                <div class="alert alert-success">
                    <strong>Thanks for contact us, we will get back to you soon...</strong> 
                </div>
            <?php } ?>
            <div class="form-error">
              
            </div>
        <form action="#" method="POST" id="contactUs" accept-charset="utf-8">
        <input type="hidden" value="<?php echo $_SESSION['add_contact_key'] ?>" name="fkey" id="fkey">
          <div class="js-contat-form">
            <div class="row">
              <div class="col-lg-6">
                <input type="text" name="name" placeholder="Your Name">
              </div>
              <div class="col-lg-6">
                <input type="email" name="email" placeholder="Your Email" required="">
              </div>
              <div class="col-lg-6">
                <input type="text" name="phone" placeholder="Your Phone" required="">
              </div>
               <div class="col-lg-6">
               <input type="text" name="subject" placeholder="Subject">
              </div>
            </div>
            
            <textarea placeholder="Your Message"></textarea>
            <input type="submit" value="Send Message" name="message" class="btn btn-primary">
          </div>
         </form>
        </div>
        <div class="col-lg-4 offset-lg-1">
          <div class="js-contact-info">
            <div class="js-contact-info-item">
              <div class="info-icon">
                <i class="fa fa-map-marker"></i>
              </div>
              <div class="info-content">
                <h4>Address:</h4>
                <p>  Gowtham Arcade, 3rd floor <br>
                208, TV Samy road, East RS Puram <br> COIMBATORE – 641002</p>
              </div>
            </div><!-- js-contact-info-item -->
            <div class="js-contact-info-item">
              <div class="info-icon">
                <i class="fa fa-phone-square "></i>
              </div>
              <div class="info-content">
                <h4>Phone:</h4>
                <p>+91 94422 62202</p>
              </div>
            </div><!-- js-contact-info-item -->
            <div class="js-contact-info-item">
              <div class="info-icon">
                <i class="fa fa-envelope-open-o "></i>
              </div>
              <div class="info-content">
                <h4>Email:</h4>
                <p><a href="mailto:credaicbe@gmail.com">credaicbe@gmail.com</a> <br>  <a href="mailto:admin@credaicoimbatore.com">admin@credaicoimbatore.com</a></p>
              </div>
            </div><!-- js-contact-info-item -->
          </div>
          <h5>Social:</h5>
          <ul class="list-inline js-social-icon-bg mt-3">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
            <li><a href="#"><i class="fa fa-youtube"></i></a></li> 
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li> 
          </ul>
        </div>
      </div>
      <div class="row">
         <div class="col-md-12">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3916.3737563213595!2d76.94925361431255!3d11.01055894216291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba8597722663f97%3A0xf621d9e55b3be08!2sCREDAI+Coimbatore!5e0!3m2!1sen!2sin!4v1549877223427" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
         </div>
      </div>
    </div>
  </section>

<?php require_once 'includes/bottom.html'; ?>