<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-8 offset-lg-2">
                <div class="js-breatcam-content">
                    <h2>Preferred Vendors</h2>
                    <!-- <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>Vendors</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->
<div class="js-brand-area p100">
    <div class="container">
      <div class="row">
          <?php //echo $data['list'] ?>
          <div class="col-md-4  offset-lg-4">
            <div class="vendor_wrap">
              <a href="<?php echo BASEPATH ?>contact"> <h2>Contact us to get your logo here.</h2></a>
            </div>
        </div>
      </div>
    </div><!-- container -->
  </div><!-- js-brand-area -->
<?php require_once 'includes/bottom.html'; ?>