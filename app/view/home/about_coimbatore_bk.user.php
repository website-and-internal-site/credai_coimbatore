<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>About Coimbatore</h2>
           <!--  <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li>About Coimbatore</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->

  <section class="js-quality-services">
    <div class="container">
      <div class="row">
      
        <div class="col-lg-12  js-management-system-area">
          <h2>Coimbatore</h2>
          <div class="about_wrap">
            <div class="row">
               <div class="col-md-4">
                  <p><img src="<?php echo IMGPATH ?>about/coimbatore-citymap.jpg" alt="coimbatore citymap"></p>
               </div>
              <div class="col-md-8">
                 <p> Coimbatore, situated in the extreme west of the state of Tamil Nadu bordering the state of Kerala, is among the fastest developing Tier-II cities of India and the second largest city of the state after Chennai. Recognized for its pleasant climate, peaceful atmosphere, cosmopolitan outlook and private enterprise, Coimbatore serves as the nerve-centre for north western districts of Tamil Nadu and northern districts of Kerala.</p>
           
           
              </div>
            </div>
            <p> Coimbatore has been rated as the fourth best city in India to do business amongst thirty six others in the country in a study done for the Confederation of Indian Industry by Indicus Analytics.</p>
             <div class="row" >
                 <div class="col-md-6">
                     <p><img src="<?php echo IMGPATH ?>about/about-coimbatore.jpg" alt="about coimbatore"></p>
                 </div>
                 <div class="col-md-6">
                     <p><img src="<?php echo IMGPATH ?>about/about-cbe.jpg" alt="about cbe"></p>
                 </div>
             </div>
            <h2>Manufacturing</h2>
            <p> Coimbatore is a veritable beehive of industries with over fifty thousand large, medium, and small-scale industries housed in the city. Once known as the "Manchester of South India", it has, even today, the highest textile activity per square kilometer in the world and has evolved into a manufacturing hub for auto components, pumps, motors, and other light engineering goods. </p>
            <ul>
              <li>Coimbatore accounts for 84% of the textile machinery and spares manufactured in the country</li>
              <li>15% of the medium and large scale textile mills are based out of Coimbatore</li>
              <li>One of every two pumps produced in India is from Coimbatore</li>
              <li>Coimbatore is among the six major foundry centres of India</li>
              <li>Coimbatore region contributes over Rs. 15,000 crores of exports each year, with Tirupur, the knitwear capital of India and located 50 kms away, itself contributing Rs. 8,000 crores</li>
            </ul>
            <h2>IT & ITES</h2>
            <p>Coimbatore is also an emerging centre for Information Technology and IT Enabled Services with several large players having a significant presence in the city. The Tamil Nadu Government is in the process of setting up an IT SEZ on 56 acres and major companies like Wipro, Tata Consultancy Services, and HCL Technologies are expected to have a presence there. CHIL IT Park, promoted by KGiSL, is another IT SEZ on 150 acres housing companies like Cognizant Technology Services, Perot Systems, and Robert Bosch GmBH.</p>
            <h2>Healthcare & Education</h2>
            <p> Coimbatore has also become one of the most trusted destinations for healthcare in the country. From comprehensive health care hospitals to specialty centres, the city provides people with the benefits of latest technology and competent professional care. Coimbatore is home to a large number of multi-specialty, super-specialty, and general hospitals delivering highly affordable services of international standards.</p>
            <p> Coimbatore is also being recognized as a centre for educational excellence with 7 universities, 32 engineering colleges, 47 arts & science colleges, 22 medical and paramedical institutions, and many more colleges of management, education, catering, etc. In fact, Coimbatore has the highest density of educational institutions in the country, and produces over 40,000 graduates of various disciplines each year. The city has the highest literacy among Indian cities having a population of one million or more.</p>
             <div class="row" >
                 <div class="col-md-6">
                     <p><img src="<?php echo IMGPATH ?>about/about-health.jpg" alt="health"></p>
                 </div>
                 <div class="col-md-6">
                     <p><img src="<?php echo IMGPATH ?>about/about-education.jpg" alt="education"></p>
                 </div>
             </div>
            <h2>Connectivity</h2>
            <p>Coimbatore is strategically located at the junction of three southern states - Tamil Nadu, Kerala and Karnataka. The city is well-connected with the rest of the country by road, rail, and air. Three National Highways pass through the city and Coimbatore is just a few hours by road from Chennai, Bangalore, and Kochi. Coimbatore also serves as the gateway by train to central and southern Kerala, and hence, the Coimbatore Junction and Podanur Junction on the outskirts of the city are very well-connected to the rest of India. Coimbatore also has a modern international airport with operations by all major domestic airlines. In addition, Coimbatore also has international flights to the Middle East, Singapore, and Colombo.</p>
            <h2>Residential Haven</h2>
            <p>Coimbatore enjoys a salubrious and pleasant climate throughout the year with the temperature ranging between 35 degrees Celsius and 15 degrees Celsius throughout the year. The city receives rainfall from both the south-west and north-east monsoons, and hence has ample supply of drinking water. </p>
             <div class="row" >
                 <div class="col-md-6">
                     <p><img src="<?php echo IMGPATH ?>about/about-residential.jpg" alt="residential"></p>
                 </div>
                 <div class="col-md-6">
                     <p><img src="<?php echo IMGPATH ?>about/about-coimbatore1.jpg" alt="coimbatore"></p>
                 </div>
             </div>
            <p> Coimbatore is also a noted centre for several religious movements and places of worship, and people of all denominations and cultures call Coimbatore their home. The city also offers its citizens a rich variety of entertainment, retail, and cultural options to choose from. All these, combined with a good working environment and excellent educational and healthcare facilities make Coimbatore among the best cities to live in the country, and is a preferred destination for post-retirement living.</p>
          </div>
        </div><!-- col-lg-6 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-quality-services close -->

  <?php require_once 'includes/bottom.html'; ?>