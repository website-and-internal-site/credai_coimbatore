<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>News Details</h2>
            <!-- <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li><a href="<?php echo BASEPATH ?>news">News</a></li>
              <li>News Details</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->
  <div class="main-single-area pt-100 pb-0">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <article class="js-classic-item">
            <div class="js-blog-article-thumbnail">
              <img src="<?php echo IMGPATH  ?>blog/blog-single-1thumbnail.jpg" alt="Credai News">
            </div>
            <div class="js-js-blog-article"> 
              <ul class="list-inline ja-post-date-comment">
                <li><a href="#">18 September, 2019</a></li>
              </ul>
              <h3><a href="#">A Look Inside the Protein Bar in our store</a></h3>
              <p>Why I say old chap that is, spiffing haggle pardon you lavatory easy peasy blag jolly good, zonked on your bike mate only a quid up the kyver bonnet. Off his nut spiffing good time blimey bog it's all gone to pot a load of old tosh nancy boy chinwag you mug crikey horse play sloshed lavatory grub blag, porkies burke daft squiffy hotpot bleeding excuse my French bugger all mate golly gosh bonnet A bit of how's your father codswallop. Nice one Jeffrey blimey naff starkers at public school cheers, it's your round bamboozled crikey burke cockup boot posh, up the duff hanky panky fantastic nancy boy chinwag. Happy days a load of old tosh he lost his bottle codswallop porkies cor blimey guvnor William David, gutted mate buggered knackered starkers such a fibber a blinding shot it's all gone to pot cack, cras James Bond the BBC quaint ruddy mufty. Pear shaped amongst bog I mufty cracking goal is do one what a plonker, say arse.</p>
              <p>Hanky panky twit bamboozled lost the plot it's your round bodge barney, chimney pot squiffy ruddy spiffing good time my lady codswallop cheeky bugger, naff blimey chip shop arse over tit a load of old tosh. Chimney pot haggle chip shop what a load of rubbish Why Queen's English happy days only a quid argy-bargy spend a penny pear shaped lost the plot wellies gosh, boot David blatant it's all gone.!</p> 
          
              <p>Don't get shirty with me bugger all mate golly gosh he nicked it arse over tit bodge faff about up the duff chinwag, bevvy butty daft off his nut cras pardon you no biggie I don't want no agro some dodgy chav, mush are you taking the piss Harry plastered lavatory happy days Oxford. Easy peasy blower car boot absolutely bladdered well bubble and squeak owt to do with me arse bamboozled, is cheeky bugger super bodge morish bog-standard wellies blow off jolly good, chimney pot chinwag old get stuffed mate what a plonker in my flat tomfoolery. Show off show off pick your nose and blow off blimey bonnet super Eaton faff about well that lost the plot, ummm I'm telling Richard up the duff he nicked it sloshed cheeky bender boot, mush a load of old tosh plastered cup of char golly gosh argy-bargy cras. Blimey have it that blag blow off down the pub what a plonker, Queen's English a blinding shot bum bag victoria sponge chimney pot, excuse my French spend a penny squiffy tickety-boo matie boy. </p>
              <p>Squiffy David blatant some dodgy chav William he legged it fantastic well, quaint twit Eaton off his nut brilliant at public school bender James Bond, the full monty Why is bevvy bleeding arse. Bodge in my flat morish so I said vagabond.!</p>  
            </div> <!-- js-js-blog-article -->
          </article><!-- article -->  
        </div><!-- col-lg-8 -->
        <div class="col-lg-4">
          <aside class="alt-bg">
            <div class="js-sidebar-widget">
              <div class="js-widget-search">
                <h4>Search</h4>
                <div class="title-shape2 mb-4"></div>
               <form>
                 <input type="text" required="" placeholder="Search...">
                 <i><img alt="search icon" src="<?php echo IMGPATH  ?>color-angle.png"></i>
               </form>
             </div>
            </div><!-- js-sidebar-widget -->
            <div class="js-sidebar-widget">
               <div class="js-widget-catagories">
                  <h4>Categories</h4>
                  <div class="title-shape2 mb-4"></div>
                  <ul>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Business</a></li>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Concept</a></li>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Inspiration</a></li>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Service</a></li>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Creative</a></li>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Fashion</a></li>
                      <li><a href="#"><i class="pe-7s-angle-right"></i>Bike Book</a></li> 
                  </ul>
               </div>
            </div><!-- js-sidebar-widget -->
            <div class="js-sidebar-widget"> 
              <h4>Recent Posts</h4>
              <div class="title-shape2 mb-4"></div>
              <ul class="js-widget-recent-post">
                <li> 
                  <div class="js-widget-recent-post">
                    <div class="js-widget-post-thumbnail">
                      <a href="#"><img alt="Bridge" src="<?php echo IMGPATH  ?>blog/sidebar-widget-thumbnail.jpg"></a>
                    </div>
                    <div class="js-widget-post-content">
                      <p><a href="#">Get Bridge and Set Up Your Website Today</a></p>
                      <span class="js-post-date">19 November, 2019</span>
                    </div> 
                  </div>
                </li>
                <li> 
                  <div class="js-widget-recent-post">
                    <div class="js-widget-post-thumbnail">
                      <a href="#"><img alt="Credai" src="<?php echo IMGPATH  ?>blog/sidebar-widget-thumbnail2.jpg"></a>
                    </div>
                    <div class="js-widget-post-content">
                      <p><a href="#">Design is really the creative invention</a></p>
                      <span class="js-post-date">06 October, 2019</span>
                    </div>
                  </div> 
                </li>
                <li> 
                  <div class="js-widget-recent-post ">
                    <div class="js-widget-post-thumbnail">
                      <a href="#"><img alt="Designers" src="<?php echo IMGPATH  ?>blog/sidebar-widget-thumbnail3.jpg"></a>
                    </div>
                    <div class="js-widget-post-content">
                      <p><a href="#">We’re Hiring Designers, Find Out Now How</a></p>
                      <span class="js-post-date">04 November, 2019</span>
                    </div>
                  </div> 
                </li>
              </ul>
            </div><!-- js-sidebar-widget -->
          </aside><!-- aside -->
        </div><!-- col-lg-4 -->
      </div><!-- row -->
    </div><!-- container -->
  </div><!--  main-blog-area -->

  <?php require_once 'includes/bottom.html'; ?>