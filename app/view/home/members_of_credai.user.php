<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>Members Of CREDAI Coimbatore</h2>
           <!--  <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li>Members Of CREDAI Coimbatore</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->
 <section class="js-blog-post-area js-blog-style2">
    <div class="container">
       <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Members Of CREDAI Coimbatore</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
      <div class="row">
        <?php echo $data['list'] ?>
      </div><!-- row --> 

     
    </div><!-- container -->
  </section><!-- js-blog-post-area -->

  <?php require_once 'includes/bottom.html'; ?>