<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>News & Events</h2>
            <!-- <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li>News</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->
 <section class="js-work-area alt-bg news_wrap" style="background-image: url(<?php echo IMGPATH ?>news_bg.jpg); background-size:cover;width:100%;">
   <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Recent News</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
    <div class="row">
        <?php echo $data['list'] ?>
    </div><!-- row -->
   </div><!-- container -->
 </section><!-- js-work-area -->

 <section class="js-blog-post-area js-blog-style2  white-bg">
    <div class="container">
       <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Our Events</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
      <div class="row">
        <?php echo $data['event'] ?>
      </div><!-- row --> 

     
    </div><!-- container -->
  </section><!-- js-blog-post-area -->
  <?php require_once 'includes/bottom.html'; ?>