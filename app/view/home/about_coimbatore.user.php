<?php require_once 'includes/top.html'; ?>

    <section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
      <div class="row d-flex text-center align-items-center">
        <div class="col-lg-8 offset-lg-2">
          <div class="js-breatcam-content">
            <h2>About Coimbatore</h2>
           <!--  <ul class="js-breatcam-menu list-inline">
              <li><a href="<?php echo BASEPATH ?>">Home</a></li>
              <li>About Coimbatore</li>
            </ul> -->
          </div>
        </div><!-- col-lg-8 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-breatcam-area -->

  <section class="js-quality-services">
    <div class="container">
      <div class="row">
      
        <div class="col-lg-12  js-management-system-area">
          <div class="about_wrap">
            <div class="row">
              <div class="col-md-12">
                <h2>Economy</h2>
                 <p> A major hub for manufacturing, education and healthcare in Tamil Nadu, Coimbatore is among the fastest growing tier-II cities in India. It houses more than 25,000 small, medium and large industries with the primary industries being engineering and textiles. Coimbatore is called the "Manchester of South India" due to its extensive textile industry, fed by the surrounding cotton fields.  TIDEL Park Coimbatore in ELCOT SEZ was the first special economic zone (SEZ) set up in 2006.  In 2010, Coimbatore ranked 15th in the list of most competitive (by business environment) Indian cities. Coimbatore also has a 160,000 square feet (15,000 m2) trade fair ground, built in 1999 and is owned by CODISSIA.</p>

                 <p>Coimbatore region experienced a textile boom in the 1920s and 1930s. Though, Robert Stanes had established Coimbatore's first textile mills as early as the late 19th century, it was during this period that Coimbatore emerged as a prominent industrial centre. In 2009 Coimbatore was home to around 15% of the cotton spinning capacity in India.  Coimbatore has trade associations such as CODISSIA, COINDIA and COJEWEL representing the industries in the city. Coimbatore houses a number of textile mills and is the base of textile research institutes like the Sardar Vallabhbhai Patel International School of Textiles & Management, Central Institute for Cotton Research(CICR) and the South India Textile Research Institute (SITRA). Kovai Cora Cotton saree is a recognised Geographical Indication. </p>

                 <p>Coimbatore is the second largest producer of software in the state, next to capital Chennai. TIDEL Park Coimbatore and other Information technology parks in the city has aided in the growth of IT and Business process outsourcing industries in the city. It is ranked at 17th among the top global outsourcing cities by Tholons. Software exports stood at ₹7.1 billion (US$99 million) for the financial year 2009–10 up 90% from the previous year. Coimbatore has a large and diversified manufacturing sector and a number of engineering colleges producing about 50,000 engineers annually.</p>

                 <p>Coimbatore is a major centre for the manufacture of automotive components in India with car manufacturers Maruti Udyog and Tata Motors sourcing up to 30%, of their automotive components from the city. G.D. Naidu developed India's first indigenous motor in 1937. India's first indigenously developed diesel engine for cars was manufactured in the city in 1972. The city is also a major centre for small auto component makers catering to the automobile industry, from personal to commercial and farm vehicles. The city contributes to about 75% of the 1 lakh total monthly output of wet grinders in India. The industry employs 70,000 people and had a yearly turnover of ₹2,800 crore (US$390 million) in 2015. The term "Coimbatore Wet Grinder" has been given a Geographical indication. </p>

                 <p>Coimbatore is also referred to as "the Pump City" as it supplies nearly 50% of India's requirements of motors and pumps. The city is one of the largest exporters of jewellery renowned for diamond cutting, cast and machine made jewellery. There are about 3,000 jewellery manufacturers employing over 40,000 goldsmiths. Coimbatore also has a booming real estate industry, there are several private builders like Ramani Realtors who took advantage of this and established their operations in Coimbatore.</p>

                 <p>Coimbatore has a large number of poultry farms and is a major producer of chicken eggs. The city contributes to nearly 95% of processed chicken meat exports. Coimbatore has some of the country's oldest flour mills and these mills which cater to all the southern states, have a combined grinding capacity of more than 50,000 MT per month. The hospitality industry has seen a growth in the 21st century with new upscale hotels being set up. Coimbatore is the largest non-metro city for e-commerce in South India.</p>
           
                  <div class="row">
                    <div class="col-md-6">
                        <img src="<?php echo IMGPATH ?>about/1.jpg" alt="about">
                    </div> 
                    <div class="col-md-6">
                       <img src="<?php echo IMGPATH ?>about/2.jpg" alt="about">
                    </div> 
                  </div>
                  <h2>History</h2>
                  <p>Coimbatore, also known as Kovai and coyamuthur, is a major city in the Indian state of Tamil Nadu. It is located on the banks of the Noyyal River and surrounded by the Western Ghats. Coimbatore is the second largest city (by area and population) in the state (after Chennai) and the 16th largest urban agglomeration in India. It is administered by the Coimbatore Municipal Corporation and is the administrative capital of Coimbatore district.</p>
                  <p>The city is one of the largest exporters of jewellery, wet grinders, poultry and auto components; the "Coimbatore Wet Grinder" and the "Kovai Cora Cotton" are recognised as Geographical Indications by the Government of India.</p>
                  <p>In 1804, Coimbatore was established as the capital of the newly formed Coimbatore district and in 1866 it was accorded municipality status with Robert Stanesas its chairman. November 24 is being observed as Coimbatore Day, say those familiar with the history of Coimbatore.[10] The city experienced a textile boom in the early 19th century due to the decline of the cotton industry in Mumbai. Post independence, Coimbatore has seen rapid growth due to industrialisation. Coimbatore was ranked the best emerging city in India by India Today in the 2014 annual Indian city survey. The city was ranked fourth among Indian cities in investment climate by Confederation of Indian Industry and 17th among the top global outsourcing cities by Tholons. Coimbatore has been selected as one of the hundred Indian cities to be developed as a smart city under Prime Minister Narendra Modi's flagship Smart Cities Mission. Coimbatore was rated as the safest city in India for women according to National Crime Records Bureau report in 2015. </p>
                  <h2>Geograpy</h2>
                  <p>Coimbatore lies at 11°1′6″N 76°58′21″E in south India at 411 metres (1349 ft) above sea level on the banks of the Noyyal River, in southwestern Tamil Nadu. It covers an area of 642.12 km2 (247.92 sq mi). It is surrounded by the Western Ghats mountain range to the West and the North, with reserve forests of the Nilgiri Biosphere Reserve on the northern side.[32] The Noyyal River forms the southern boundary of the city, which has an extensive tank system fed by the river and rainwater.[33][34] The eight major tanks and wetland areas of Coimbatore are namely, Singanallur, Valankulam, UkkadamPeriyakulam, Selvampathy, Narasampathi, Krishnampathi, Selvachinthamani, and Kumaraswami.[35] Multiple streams drain the waste water from the city.</p>
                  <h2>Climate</h2>
                  <p>Under the Köppen climate classification, the city has a tropical wet and dry climate, with a wet season lasting from September to November due to the northeast monsoon. The mean maximum temperature ranges from 35.9 °C (97 °F) to 29.2 °C (85 °F) and the mean minimum temperature ranges from 24.5 °C (76 °F) to 19.8 °C (68 °F). The highest temperature ever recorded is 40.4 °C (105 °F) on May 5, 1983 while the lowest is 9.7 °C (49 °F) on January 8, 1912. </p>
                  <h2>Demographics</h2>
                  <p>Coimbatore has a population of 1,601,438. As per the 2011 census based on pre-expansion city limits, Coimbatore had a population of 1,050,721 with a sex ratio of 997 females for every 1,000 males, much above the national average of 929.] It is the second largest city in the state after capital Chennai and the sixteenth largest urban agglomeration in India. A total of 102,069 were under the age of six, comprising 52,275 males and 49,794 females. The average literacy of the city was 82.43%, compared to the national average of 72.99%.[ There were a total of 425,115 workers, comprising 1,539 cultivators, 2,908 main agricultural labourers, 11,789 in house hold industries, 385,802 other workers, 23,077 marginal workers, 531 marginal cultivators, 500 marginal agricultural labourers, 1,169 marginal workers in household industries and 20,877 other marginal workers.</p>
                  <p> As per the 2001 census, Coimbatore had a population of 930,882 within the municipal corporation limits.  The population of the urban agglomeration as per 2011 census is 2,136,916 with males constituting 50.08% of the population and females 49.92%. Coimbatore has an average literacy rate of 89.23%, higher than the national average of 74.04%. Male literacy is 93.17% and female literacy is 85.3% with 8.9% of the population under six years of age. The sex ratio was 964 females per 1000 males. In 2005, the crime rate in the city was 265.9 per 100,000 people, accounting for 1.2% of all crimes reported in major cities in India. It ranked 21st among 35 major cities in India in the incidence of crimes. In 2011, the population density in the city was 10,052 per km2 (26,035 per mi2).  Around 8% of the city's population lives in slums.</p>
                     <div class="row">
                    <div class="col-md-6">
                        <img src="<?php echo IMGPATH ?>about/3.jpg" alt="credai">
                    </div> 
                    <div class="col-md-6">
                       <img src="<?php echo IMGPATH ?>about/4.jpg" alt="about credai">
                    </div> 
                  </div>
                  <h2>Administration</h2>
                  <p>Coimbatore is a Municipal corporation administered by the Coimbatore Municipal Corporation and is the administrative headquarters of Coimbatore district] Coimbatore was established as the capital of Coimbatore district in 1804 and in 1866 it was accorded municipality status.  In 1981, Coimbatore was elevated as a municipal corporation. The city is divided into five administrative zones – East, West, North, South and Central, each further subdivided into 20 wards.  Each ward is represented by a councillor who is elected by direct election and the Mayor of Coimbatore is elected by Councillors. The executive wing of the corporation is headed by a Corporation Commissioner and maintains basic services like water supply, sewage and roads. The district itself is administered by the District collector and the district court in Coimbatore is the highest court of appeal in the district. The Coimbatore City Police is headed by a Commissioner and there are 18 police stations in the city. </p>
                  <p>A large part of the Coimbatore urban agglomeration falls outside the Municipal corporation limits.  These suburbs are governed by local bodies called Village Panchayats and Town Panchayats.  Besides the Coimbatore Municipal Corporation, the Coimbatore UA comprises the town panchayats of Vellalur, Irugur, Pallapalayam, Kannampalayam, Veerapandi, Periyanaickenpalayam, Narasimhanaickenpalayam, Idikarai, Vedapatti, Perur, Madukkarai, Ettimadai, Thondamuthur, Uliyampalayam, Thirumalayampalayam, Othakalmandapam, Alanthurai, Pooluvapatti, Thenkarai, Karumathampatti, Sarcarsamakulam, Mopperipalayam and Gudalur, census towns of Ashokapuram, Kurudampalayam, Malumichampatti, Selvapuram, Chettipalayam, Sulur, Chinniampalayam, Somayampalayam, MuthugoundanPudur, Arasur, Kaniyur, Neelambur and municipalities of Kuniyamuthur, Kurichi and Goundampalayam.[64] These local bodies are in turn split into wards each electing a councillor through direct election. The head of the local body known as president[65] is elected by the councillors from among their number. </p>
                  <p>Coimbatore elects ten members to the Tamil Nadu Legislative Assembly and one member to the Indian Parliament. The five legislative assembly constituencies in the city are Coimbatore North, Coimbatore South, Kaundampalayam, Singanallur and Sulur which form a part of the Coimbatore Parliamentary Constituency. Part of the urban agglomeration comes under the Nilgiris and Pollachi constituencies. In the Indian general election held in 2014, AIADMK candidate A. P. Nagarajan defeated C. P. Radhakrishnan of the BJP in the Lok Sabha constituency.[66] In the last legislative assembly election held in 2011, the AIADMK led front won in all five assembly constituencies</p>
              </div>
            </div>
          

          </div>
        </div><!-- col-lg-6 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-quality-services close -->

  <?php require_once 'includes/bottom.html'; ?>