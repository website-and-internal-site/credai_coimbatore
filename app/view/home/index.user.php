<?php require_once 'includes/top.html'; ?>

  <section class="js-slider-area has-color p-0" id="js-slider-area">
  <!--<div class="js-slider-item1 slide-item p150">
      <div class="container">
        <div class="row d-flex text-center align-items-center">
          <div class="col-lg-8 offset-lg-2">
            <div class="js-slider-content home_sldier_wrap">
              <p class="lead">Welcome to CREDAI Coimbatore – A unified body of Real Estate Developers</p>
              <a href="<?php echo BASEPATH ?>about" class="btn btn-primary">Know More</a>
            </div>
          </div>
        </div>
      </div>
    </div>-->
    <div class="js-slider-item slide-item p150">
      <div class="container">
        <div class="row d-flex text-center align-items-center">
          <div class="col-lg-8 offset-lg-2">
            <div class="js-slider-content home_sldier_wrap">
             <!--  <h2>CREDAI Coimbatore – A unified body of Real Estate Developers</h2> -->
              <p class="lead">Welcome to CREDAI Coimbatore – A unified body of Real Estate Developers</p>
              <a href="<?php echo BASEPATH ?>about" class="btn btn-primary">Know More</a>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- js-slider-item -->
    <div class="js-slider-item2 slide-item p150">
      <div class="container">
        <div class="row d-flex text-center align-items-center">
          <div class="col-lg-10 offset-lg-1">
            <div class="js-slider-content home_sldier_wrap">
              <!-- <h2>Educational Scholarship Fund</h2> -->
              <p class="lead"> The city’s chapter of the Confederation of Real Estate Developers’ Associations of India (CREDAI), Coimbatore brings together more than 11,500 Real Estate Developers from 156 city chapters across 23 states of India.</p>
              <a href="<?php echo BASEPATH ?>about" class="btn btn-primary">Know More</a>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- js-slider-item --> 
  </section><!-- js-slider-area --> 

  <section class="js-quality-services">
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="js-management-thumbnail">
            <img src="<?php echo IMGPATH  ?>about/about.jpg" alt="Thumbnail">
          </div>
        </div><!-- col-lg-6 -->
        <div class="col-lg-7  js-management-system-area">
          <h2>About CREDAI</h2>
          <ul class="list-inline nav nav-tabs mb-100"  id="myTab" role="tablist">
            <li>
              <a class="active" id="insights-tab" data-toggle="tab" href="#insights" role="tab" aria-controls="insights" aria-selected="true"> CREDAI Coimbatore</a>
            </li>
            <li>
              <a id="planning-tab" data-toggle="tab" href="#planning" role="tab" aria-controls="planning" aria-selected="false">CREDAI National</a>
            </li>
            <li>
              <a id="strategies-tab" data-toggle="tab" href="#strategies" role="tab" aria-controls="strategies" aria-selected="false"> WHY CREDAI</a>
            </li>
          </ul> 
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="insights" role="tabpanel" aria-labelledby="insights-tab">
                <div class="js-quality-services-tab-content">
                  <p>With over 70 members, CREDAI Coimbatore has succeeded in bringing the majority of private real estate developers in Coimbatore under a single umbrella. Over the years, it has become a vital partner in Coimbatore’s growth and promotion of real estate. It has been lauded for its spontaneity in disseminating information to members and updating them on all governmental policies, procedures, and regulations that concern the property development industry.</p>
                  <div class="col-lg-12">
          <a class="btn btn-primary" href="#">Read More</a>
        </div>
                </div><!-- js-quality-services-tab-content -->
            </div><!-- tab-pane -->
            <div class="tab-pane fade" id="planning" role="tabpanel" aria-labelledby="planning-tab">
              <div class="js-quality-services-tab-content">
                <p>Confederation of Real Estate Developers’ Associations of India (CREDAI) was established in 1999 with a mandate to pursue the cause of housing and habit providers. It has grown its membership base since then and has more than 12500 members today spread across 23 state and 205 city chapters. CREDAI targets to reach at least 300 cities by the end of March 2019.</p>
                <p>On the advocacy side, CREDAI is the preferred platform for National discourse on Housing and Habitat.</p>
              </div><!-- js-quality-services-tab-content -->
            </div><!-- tab-pane -->
            <div class="tab-pane fade" id="strategies" role="tabpanel" aria-labelledby="strategies-tab">
              <div class="js-quality-services-tab-content">
                <p>All members of CREDAI are required to sign and adhere to CREDAI National Code of Conduct. Some of the main points are,</p>
                <p>To promote integrity and transparency among the property developer community by encouraging them to adopt fair practices in all aspects of the business.</p>
                <p>To provide a platform for members to network and exchange information on best practices, competencies, and the latest trends in the property development industry.</p>
                <p>To organize conventions and exhibitions every year for the housing and real estate sector, to foster a relationship between builders, government and the people.</p>
              </div><!-- js-quality-services-tab-content -->
            </div><!-- tab-pane -->
          </div><!-- col-lg-12 --> 
        </div><!-- col-lg-6 -->
      </div><!-- row -->
    </div><!-- container -->
  </section><!-- js-quality-services close -->

 <section class="js-blog-post-area js-blog-style2  white-bg">
    <div class="container">
       <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Our Events</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
      <div class="row">
         <?php echo $data['list'] ?>
        <div class="col-lg-12 text-center mt-50">
          <a class="btn btn-primary" href="<?php echo BASEPATH ?>news">View All</a>
        </div>
      </div><!-- row --> 
    </div><!-- container -->
  </section><!-- js-blog-post-area -->

<section class="js-work-area alt-bg">
   <div class="container">
   <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Recent News</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
    <div class="row">
       <?php echo $data['news'] ?>
      <div class="col-lg-12 text-center mt-50">
          <a class="btn btn-primary" href="<?php echo BASEPATH ?>news">View All</a>
      </div>
    </div><!-- row -->
   </div><!-- container -->
 </section><!-- js-work-area -->
   <section class="js-agent-area pb150">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Committee Members of CREDAI Coimbatore</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
      <div class="js-agent-sliders">
         <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/1.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Surender Vittel</h4>   
            <h6>President </h6>
          </div> 
         
        </div><!-- js-single-agent-item -->   
       
        <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/2.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Madan B Lund</h4>
            <h6>Vice President</h6>
          </div> 
         
        </div>
        <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/8.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Rajiv Ramaswamy</h4>
            <h6>Secretary </h6>
          </div> 
         
        </div><!-- js-single-agent-item --> 
        <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/4.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>D. Abhishek</h4>
            <h6>Joint Secretary </h6>
          </div> 
         
        </div><!-- js-single-agent-item -->  
        <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/5.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>U. Somasundaram</h4>
            <h6>Treasurer </h6>
          </div> 
         
        </div><!-- js-single-agent-item -->  
        <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/6.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>SJ Ananthakrishnan</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->

         <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/7.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>SP Ramaswamy</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->
         <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/6.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>S.Manoj Krishna Kumar</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->
         <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/9.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>R Ramesh</h4>
            <h6>MC Member</h6>
          </div> 
         
        </div><!-- js-single-agent-item -->
        <!--  <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/10.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4> Kalpesh Bafna</h4>
            <h6>Elect President </h6>
          </div> 
        </div> --><!-- js-single-agent-item -->
         <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/11.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Rajesh B Lund</h4>
            <h6>Ex Officio Member</h6>
          </div> 
        </div><!-- js-single-agent-item -->
         <div class="js-single-agent-item">
          <div class="js-agent-thumbnail">
            <img src="<?php echo IMGPATH  ?>agent/12.jpg" alt="agent thumbnail">
          </div>
          <div class="js-agent-content text-center">
            <h4>Ananthram</h4>
            <h6>General Manager</h6>
          </div>
        </div><!-- js-single-agent-item --> 
      </div><!-- js-agent-sliders -->

    </div><!-- container -->
  </section><!-- js-agent-area Close -->
    <div class="js-brand-area p100">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="js-section-title mb-50">
            <h4>Vendors</h4>  
            <div class="title-shape2"></div>
          </div>
        </div><!-- col-lg-3 -->
      </div><!-- row -->
      <div class="row">
         <?php //echo $data['vendor'] ?>
           <div class="col-md-4  offset-lg-4">
            <div class="vendor_wrap">
              <a href="<?php echo BASEPATH ?>contact"> <h2>Contact us to get your logo here.</h2></a>
            </div>
        </div>
      </div>
     <!--  <div class="row text-center js-brand-list mb-50"> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/1.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/2.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/3.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/4.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/5.png" alt="client"> </a>
          </div> 
      </div> --><!-- row -->
     <!--  <div class="row text-center js-brand-list"> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/6.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/7.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/8.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/9.png" alt="client"> </a>
          </div> 
          <div class="col js-brand-item">
            <a href="#" target="_blank"><img src="<?php echo IMGPATH  ?>brand/10.png" alt="client"> </a>
          </div> 
      </div> --><!-- row -->
    </div><!-- container -->
  </div><!-- js-brand-area -->
  <?php require_once 'includes/bottom.html'; ?> 