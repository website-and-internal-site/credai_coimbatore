<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-8 offset-lg-2">
                <div class="js-breatcam-content">
                    <h2>WHY CREDAI</h2>
                    <!-- <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>WHY CREDAI</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->


  <section class="js-contact-area alt-bg">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
    <h2>WHY CREDAI</h2>
          <div class="about_wrap">
          

            <h2>CREDAI Membership Benefits :</h2>
            <ul>
              <li>Part of India’s largest national forum for real estates.</li>
              <li>Collective representation and unified voice of the industry.</li>
              <li> Dialogue with policymakers on important issues.</li>
              <li>Part of the social change through CREDAI CSR initiatives.</li>
              <li>Enhanced status of real estate business in the City / State / National / International level.</li>
              <li>Expansion of business network in India and abroad.</li>
              <li>Opportunity to take up leadership roles at CREDAI and drive the industry agenda forward.</li>             
            </ul>
            <h2>CREDAI Members enjoy exclusive branding benefits through membership:</h2>
            <ul>
              <li>Members get to prominently display CREDAI logo signifying their CREDAI membership on the website, marketing collaterals, advertisements etc.</li>
              <li>CREDAI membership gives potential buyer confidence and trust in the company they are investing in</li>
         
            </ul>
            <h2> MEMBERSHIP AND TYPES</h2>
            <h3>THERE SHALL BE FOLLOWING CATEGORIES OF MEMBERSHIP:</h3>
            <ul>
              <li>Member Association</li>
              <li>Associate Member </li>
              <li>Overseas Associate</li>
              <li>Invitee Member</li>
            </ul>

            <h3>(I)  MEMBER ASSOCIATION SHALL BE OF TWO TYPES:</h3>
            <p>Any State level Federation / Organization of Promoters and Builders and Real Estate Developers representing a State in India having at least two City / Town level Associations as its members, except MCHI (Maharashtra Chamber of Housing Industry), being the founder member, shall continue to enjoy the status of State Federation. The existing State-level Federation(s) not fulfilling the above criterion, in order to be considered as such, shall have to fulfill the above requirement within a period of six months, failing which the said Federation(s) shall automatically become a city/town level association.</p>
            <p>Any City / Town level Association / Organization of Promoters & Builders and Real-Estate Developers in any place where there is no State level Federation in existence. Upon formation of the State level Federation, such a State level Federation will become the Member and the concerned city level association will cease to remain a member of CREDAI National upon which it will be a member of the new State-level Federation.</p>
             <h3> (II) ASSOCIATE MEMBER:</h3>

            <p>Housing Finance Companies, Commercial Banks and Financial Institutions having Activities in the field of Real Estate, Consultants, Association or Council or Organization having interest in real estate development activities, Professional Institutes / Organizations / Boards dealing with collection and dissemination of Information, Research and Development shall be eligible to be invited to become Associate Member of the Confederation.</p>

            <h3>(III) OVERSEAS ASSOCIATE:</h3>

            <p>Foreign Association or undertaking engaged in real estate activities may apply to become an Overseas Associate provided that the Governing Council Board may, at its discretion with the concurrence of not less than three-fourths of its total strength, relax or vary any of the qualifications for membership specified herein. A circular or letter duly signed by a member of the Governing Council Board expressing his approval or disapproval of the relaxation or Variation, as the case may be, shall be entered in the Register of Members.</p>
            <h3>(IV) INVITEE MEMBERS:</h3>
            <p>All members of City-level associations shall be invitee members without having the voting right.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php require_once 'includes/bottom.html'; ?>