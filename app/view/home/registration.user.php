<?php require_once 'includes/top.html'; ?>
<section class="js-breatcam-area js-about-bg has-color p-0">
    <div class="container">
        <div class="row d-flex text-center align-items-center">
            <div class="col-lg-8 offset-lg-2">
                <div class="js-breatcam-content">
                    <h2>New Member Registration</h2>
                    <!-- <ul class="js-breatcam-menu list-inline">
                        <li><a href="<?php echo BASEPATH ?>">Home</a></li>
                        <li>New Member Registration</li>
                    </ul> -->
                </div>
            </div><!-- col-lg-8 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- js-breatcam-area -->
<section class="js-contact-area alt-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="js-section-title mb-50">
                    <h4>New Member Registration</h4>
                    <div class="title-shape2"></div>
                </div>
            </div><!-- col-lg-3 -->
        </div>
           <?php if(isset($_GET['a'])) { ?>
                  <div class="alert alert-success">
                      <strong>Registration made successfully..</strong> 
                  </div>
            <?php } ?>
        <form method="post" id="applyMembership" action="#">
            <div class="row">
                <div class="col-lg-12">
                    <p>New Membership Application Form <a target="_blank" href="<?php echo IMGPATH ?>docs/register.pdf">Download</a> </p>
                    <div class="js-contat-form">
                        <input type="hidden" value="<?php echo $_SESSION['add_member_key'] ?>" name="fkey" id="fkey">
                        <div class="row r">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="formBlock select">
                                    <label for="propertyType">Company Name<span style="color:red;">*</span></label><br>
                                    <input type="text" name="company_name" id="company_name" class="input">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Website<span style="color:red;">*</span></label><br>
                                    <input type="text" name="website" required="" id="website" class="input">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Phone<span style="color:red;">*</span></label><br>
                                    <input type="text" name="phone" id="phone" required="" class="input">
                                </div>
                            </div>
                        </div>
                        <div class="row r">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Company Logo</label><br>
                                    <div class="form-group">
                                    <input type="file" id="cimage" name="cimage" />
                                    <input type="hidden" id="customerImage" name="image">
                                  </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Address<span style="color:red;">*</span></label><br>
                                    <textarea class="input" name="address" required="" id="address"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Pincode<span style="color:red;">*</span></label><br>
                                    <input type="text" name="pincode" id="pincode" required="" class="input">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Company Start Up Date<span style="color:red;">*</span></label><br>
                                    <input type="text" name="start_date" id="start_date" required="" class="input hasDatepicker">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6">
                                <div class="formBlock select">
                                    <label for="location">Company Profile<span style="color:red;">*</span></label><br>
                                    <textarea class="input" name="company_profile" required="" id="company_profile"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row r reg_contact_wrap">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h3>Contact Person Details</h3>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="propertyType">Name<span style="color:red;">*</span></label><br>
                        <input type="text" name="contact_name" id="contact_name" required="" class="input">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Mobile No<span style="color:red;">*</span></label><br>
                        <input type="text" name="contact_mobile" id="contact_mobile" required="" class="input">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Email<span style="color:red;">*</span></label><br>
                        <input type="text" name="contact_email" id="contact_email" required="" class="input">
                    </div>
                </div>
            </div>
            <div class="row r reg_contact_wrap">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h3>MD Details</h3>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="propertyType">Name<span style="color:red;">*</span></label><br>
                        <input type="text" name="md_name" id="md_person" required="" class="input">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Mobile No<span style="color:red;">*</span></label><br>
                        <input type="text" name="md_mobile" id="md_mobile" required="" class="input">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Email<span style="color:red;">*</span></label><br>
                        <input type="text" name="md_email" id="md_email" required="" class="input">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Date of Birth
                            <input type="text" name="dob" id="dob" class="input">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="propertyType">Date of Anniversary</label><br>
                        <input type="text" name="doa" id="doa" class="input">
                    </div>
                </div>
            </div>
            <div class="row r">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Username<span style="color:red;">*</span></label><br>
                        <input type="text" name="user_name" id="user_name" required="" class="input" value="" size="20">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Password<span style="color:red;">*</span></label><br>
                        <input type="password" name="password" id="password" required="" class="input" value="" size="20">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Confirm Password<span style="color:red;">*</span></label><br>
                        <input type="password" name="confirm_password" id="confirm_password" required="" class="input" value="" size="20">
                    </div>
                </div>
            </div>
            <div class="row r">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Facebook</label><br>
                        <input type="text" name="facebook" id="facebook" class="input" placeholder="https://twitter.com/your_link">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Twitter</label><br>
                        <input type="text" name="twitter" id="twitter" class="input" placeholder="https://twitter.com/your_link">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">LinkedIn</label><br>
                        <input type="text" name="linkedin" id="linkedin" class="input" placeholder="https://www.linkedin.com/in/your_link">
                    </div>
                </div>
                <div class="col-md-4 col-md-4 col-sm-6">
                    <div class="formBlock select">
                        <label for="location">Youtube</label><br>
                        <input type="text" name="youtube" id="youtube" class="input" placeholder="https://www.youtube.com/your_link">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    
                </div>
                <div class="col-md-2">
                    <div class="text-right">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php require_once 'includes/bottom.html'; ?>