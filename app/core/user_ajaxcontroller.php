<?php
	
require_once './../global/global-config.php';
require_once '../config/config.php';
require_once '../app/models/Model.php';
require_once 'classes/PHPMailerAutoload.php';

class Ajaxcontroller extends Model
{		

	

	/*==============================================
					Contact Us
	=================================================*/


	function contactUs($data)
	{
		if(isset($_SESSION['add_contact_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_contact_key']){
				$curr  		= date("Y-m-d H:i:s");
					$query = "INSERT INTO ".CONTACT_US." SET 
							name 			= '".$this->cleanString($data['name'])."',
							mobile 			= '".$this->cleanString($data['phone'])."',
							email 			= '".$this->cleanString($data['email'])."',
							subject			= '".$this->cleanString($data['subject'])."',
							message			= '".$this->cleanString($data['message'])."',
							status			= '1',
							created_at 		= '$curr',
							updated_at 		= '$curr' ";
					$exe 	= mysql_query($query);
					if($exe){
							$sender         = COMPANY_NAME;
					       	$sender_mail    = NO_REPLY;
					        $subject        = COMPANY_NAME." - Contact";
					       	$receiver       = "selvap@venpep.net";
					        $email_temp     = $this->contactTemplate($data);
					       	$sendemail      = $this->send_mail($sender,$sender_mail,$receiver,$subject,$email_temp);
					       	if ($sendemail) {
					       		unset($_SESSION['add_contact_key']);
								return 1;
							}
						
					}else{
						return "Sorry!! Unexpected Error Occurred. Please try again.";
					}
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}		





	/*==============================================
						Career
	=================================================*/


	function applyJob($data="")
	{
		if(isset($_SESSION['add_job_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_job_key']){
				$curr  		= date("Y-m-d H:i:s");
					 $query = "INSERT INTO ".CAREER_TBL." SET 
							name 			= '".$this->cleanString($data['name'])."',
							email 			= '".$this->cleanString($data['email'])."',
							message			= '".$this->cleanString($data['message'])."',
							file 			= '".$this->cleanString($data['image'])."',
							status			= '1',
							created_at 		= '$curr',
							updated_at 		= '$curr' ";
					$exe 	= mysql_query($query);
					if($exe){
						unset($_SESSION['add_job_key']);
						return 1;
					}else{
						return "Sorry!! Unexpected Error Occurred. Please try again.";
					}
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}




	/*==============================================
						Membership
	=================================================*/


	function applyMembership($data="")
	{
		if(isset($_SESSION['add_member_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_member_key']){
				$curr  		= date("Y-m-d H:i:s");
				$query = "INSERT INTO ".MEMBER_REGISTRATION." SET 
						company 			= '".$this->cleanString($data['company_name'])."',
						website 			= '".$this->cleanString($data['website'])."',
						phone				= '".$this->cleanString($data['phone'])."',
						logo 				= '".$this->cleanString($data['image'])."',
						address 			= '".$this->cleanString($data['address'])."',
						pincode  			= '".$this->cleanString($data['pincode'])."',
						start_date 			= '".$this->cleanString($data['start_date'])."',
						company_profile 	= '".$this->cleanString($data['company_profile'])."',
						contact_name 		= '".$this->cleanString($data['contact_name'])."',
						contact_mobile 		= '".$this->cleanString($data['contact_mobile'])."',
						contact_email 		= '".$this->cleanString($data['contact_email'])."',
						md_name 			= '".$this->cleanString($data['md_name'])."',
						md_mobile 			= '".$this->cleanString($data['md_mobile'])."',
						dob 				= '".$this->cleanString($data['dob'])."',
						doa 				= '".$this->cleanString($data['doa'])."',
						md_email 			= '".$this->cleanString($data['md_email'])."',
						user_name 			= '".$this->cleanString($data['user_name'])."',
						password 			= '".$this->cleanString($data['password'])."',
						facebook 			= '".$this->cleanString($data['facebook'])."',
						twitter 			= '".$this->cleanString($data['twitter'])."',
						linkedin 			= '".$this->cleanString($data['linkedin'])."',
						youtube	 			= '".$this->cleanString($data['youtube'])."',
						status				= '1',
						created_at 			= '$curr',
						updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_member_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}



	function addCGRF($data,$images)
	{
		if(isset($_SESSION['add_cgrf_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_cgrf_key']){
				$curr 			= date("Y-m-d H:i:s");
				$query = "INSERT INTO ".CGRF_TBL." SET 
							name 						= '".$this->cleanString($data['name'])."',
							age 						= '".$this->cleanString($data['age'])."',
							email 						= '".$this->cleanString($data['email'])."',
							phone 						= '".$this->cleanString($data['phone'])."',
							landline 					= '".$this->cleanString($data['landline'])."',
							address 					= '".$this->cleanString($data['address_comm'])."',
							name_of_property_developer  = '".$this->cleanString($data['name_develop'])."',
							project_name 				= '".$this->cleanString($data['project_name'])."',
							project_address 			= '".$this->cleanString($data['project_address'])."',
							grievance   				= '".$this->cleanString($data['grievance'])."',
							complaint_any_court 		= '".$this->cleanString($data['complaint_any_court'])."',
							out_of_court_settlement 	= '".$this->cleanString($data['out_of_court_settlement'])."',	
							lodged_any_complaint 		= '".$this->cleanString($data['lodged_any_complaint'])."',
							status						= '1',
							created_at 					= '$curr',
							updated_at 					= '$curr' ";
				$exe 	= mysql_query($query);
				$last_id = mysql_insert_id();
				if($exe){
					if (count($images)>0) {
						$multifiles = $this->saveMultiFiles($images,$last_id);
					}	
					unset($_SESSION['add_cgrf_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}
				
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}


		// File Multi insert

	function saveMultiFiles($files,$reference_id)
	{
		if (count($files)>0) {
			$i = 0;
			foreach ($files as $value) {
	           	$output = str_replace("uploads/srcimg/", "", $value);
	           	$image_info = explode(".", $output); 
				$image_type = end($image_info);
	           	$qu = "INSERT INTO ".FILE_TBL. " SET reference_id ='$reference_id',file_type='$image_type',files='$output'";
	           	$exee = mysql_query($qu);
	           	$i++;
	        }
		}
		return 1;
	}

	
}

?>