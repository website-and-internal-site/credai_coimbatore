<?php

class Constants
{

	public function controllernames()
	{

		/*-----------------------------------------------
		 	Define the Custom Controllers with Hyphen
		-------------------------------------------------*/

		$custom_routes = array();
		$custom_routes['case-study'] 			= "case_study";
		return $custom_routes;

	}

	public function methodnames($value='')

	{ 

		/*-----------------------------------------------
			 Define the Custom Methods with Hyphen
		-------------------------------------------------*/

		$custom_methods = array(); 
		
		$custom_methods['members-of-credai'] 				= "members_of_credai";
		
		return $custom_methods;

	}
}


?>