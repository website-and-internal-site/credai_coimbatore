<?php
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
$route 		= new Ajaxcontroller();

// Add New Product productimage

$errors = array();
$msg = array();

if(!empty($_FILES["cimage"]["type"]))
{
 
		if($_FILES["cimage"]["size"] < 900000000)
		{
			$rands = $route -> generateRandomString("10");
			$date = date("dmYhis");

	        if ($_FILES["cimage"]["error"] > 0)
			{
	           $errors[] = "Return Code: " . $_FILES["cimage"]["error"] . "<br/>";
	        } 
			else 
			{ 
				if (file_exists("uploads/srcimg/".$_FILES["cimage"]["name"])) {
	            	$err =  $_FILES["cimage"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
	            	$errors[] = $err;
				} 
				else 
				{					
					$sourcePath = $_FILES["cimage"]['tmp_name'];
					$post_image_name = $rands.$date.$route->hyphenize($_FILES["cimage"]['name']);
					$targetPath = "uploads/srcimg/".$post_image_name;
					move_uploaded_file($sourcePath,$targetPath);
					
					$msg[] = $post_image_name;
				}				       
	        }
		}
		else{
			$errmsg =  "File Size Exceed 800 KB. Please upload proper file with proper file size.<br/>";
        	$errors[] = $errmsg;
		}
    
}else{
	$msg[] = "";
}


if(count($errors)==0){
	$success = "";
	foreach ($msg as $key =>  $value) {
		$success .= "`".$value;
	}
	echo "1".$success;
}else{
	$op = "";
	foreach ($errors as $value) {
		$op .= $value;
	}
	echo "0`".$op;
}



?>