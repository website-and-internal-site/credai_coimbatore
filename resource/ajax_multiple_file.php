<?php
    session_start();
    //require_once '../config/config.php';
    require_once '../app/core/user_ajaxcontroller.php';
    require_once '../app/core/class.uploader.php';
    $route      = new Ajaxcontroller();
    $uploader   = new Uploader();     

    $content = $_POST;  
    //print_r($content);


    if (isset($_FILES['files'])) {
        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 40, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => 'uploads/srcimg/', //Upload directory {String}
            'title' => array('auto', 30), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => 'onFilesRemoveCallback' //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if($data['isComplete']){
            $files  = $data['data'];
            $images = $data['data']['files'];
           //echo $route->addApprovalQuantity($content,$images);
        }

        if($data['hasErrors']){
            $errors = $data['errors'];
            $error  = $errors[0];
            $layout = "Upload Failed Due to Following reasons";
            foreach ($error as $key => $value) {
                $layout .=  "<p> - ".$value."</p>";
            }
            echo $layout;
        }
        
        function onFilesRemoveCallback($removed_files){
            foreach($removed_files as $key=>$value){
                $file = '../uploads/' . $value;
                if(file_exists($file)){
                    unlink($file);
                }
            }
            return $removed_files;
        }
    }else{
        $images = array();
    }
    
    //print_r($content);   

    $page = @$_REQUEST["page"];
    switch($page){

        case 'addCGRF':
            echo $route->addCGRF($content,$images);
        break;
    }
    
?>
