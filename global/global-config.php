<?php
	require_once 'environments.php';
	require_once 'database-tables.php'; 

		
	//--------------------- WEBSITE SETTINGS --------------------//

	// Website Base Path
	define('COMPANY_NAME', 'CREDAI - Coimbatore');
	// Domain Name
	define('DOMAIN', 'credaicoimbatore.com');
	// Domain Full Name
	define('DOMAIN_FULL', 'www.credaicoimbatore.com');
	// Domain Link
	define('DOMAIN_LINK', 'http://credaicoimbatore.com/');
	// Admin Folder
	define('ADMIN_PATH', '/admin');
	// Assets Folder
	define('ASSETS_PATH', HOST_NAME.'/assets/');
	// Payment Gateway
	define('LIVE_PAYMENT_KEY', '#');


	//--------------------- EMAIL SETTINGS --------------------//

	// Admin Mail Address
	define('ADMIN_EMAIL', 'info@credaicoimbatore.com');
	// No Reply Mail Address
	define('NO_REPLY', 'no-reply@credaicoimbatore.com');
	// Reply to Mail Address
	define('REPLY_TO', 'info@credaicoimbatore.com');
	// Email Footer Address
	define('EMAIL_FOOTER', 'CREDAI - Coimbatore , Email: info@credaicoimbatore.com');
	


	//--------------------- APP LINKS --------------------//

	// Google Play Store
	define('PLAY', '#');
	// App Store
	define('IOS', '#');
	

	//--------------------- SOCIAL PAGES --------------------//

	// Facebook Page
	define('FACEBOOK_PAGE', '');
	// Facebook API Key
	define('FB_APP_ID', '');		
	// Facebook Share Image
	define('FB_SHARE_IMG', '');
	

    require_once 'site-contents.php';
	require_once 'meta-contents.php';


?>