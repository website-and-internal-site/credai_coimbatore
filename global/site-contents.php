<?php

	//--------------------- FRONT WEBSITE SETTINGS --------------------//

	// Footer Content
	define('FOOTER_CONTENT', 'This is official website of Association of Global Nicmarians'); 
	// Footer Address
	define('FOOTER_ADDRESS', 'Association of Global Nicmarians');
	// Footer Phone
	define('FOOTER_PHONE', ''); 

	//--------------------- SOCIAL LINKS --------------------//

	// Facebook Link
	define('FACEBOOK_LINK', '');
	// Twitter Link
	define('TWITTER_LINK', '');
	// Google Plus Link
	define('GOOGLEPLUS_LINK', '');
	// Linkedin Link
	define('LINKEDIN_LINK', '');
	// Youtube Link
	define('YOUTUBE_LINK', '');
	// Instagram Link
	define('INSTAGRAM_LINK', '');
	
	



?>