<?php 

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/


session_start();
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
$route = new Ajaxcontroller();


$page = @$_REQUEST["page"];
$data = @$_REQUEST["element"];
$term = @$_REQUEST["term"];
$post = @$_POST["result"];

// Get Client IP Address

switch($page){

	// User Login 

	case 'userLogin':
		$escapedPost = array_map('mysql_real_escape_string', $_POST);
		$escapedPost = array_map('htmlentities', $escapedPost);
		echo $route -> userLogin($escapedPost);
	break;

	// Edit Profile

	case 'editUserProfile':
		$escapedPost = array_map('mysql_real_escape_string', $_POST);
		$escapedPost = array_map('htmlentities', $escapedPost);
		echo $route -> editUserProfile($escapedPost);
	break;	


	// Change Password

	case 'changeAdminPassword':
		$escapedPost = array_map('mysql_real_escape_string', $_POST);
		$escapedPost = array_map('htmlentities', $escapedPost);
		echo $route -> changeAdminPassword($escapedPost);
	break;	


	case 'deleteContactUs':
		echo $route -> deleteContactUs($post);
	break;	


	case 'addEvent':
		echo $route -> addEvent($_POST);
	break;	


	case 'removeEventImage':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(EVENTS_TBL,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removeEventImage($id);
	break;


	case 'editEvents':
		echo $route -> editEvents($_POST);
	break;	


	case 'eventActive':
		echo $route -> eventActive($post);
	break;	





	case 'addContent':
		echo $route -> addContent($_POST);
	break;	

	case 'editContent':
		echo $route -> editContent($_POST);
	break;	


	case 'contentActive':
		echo $route -> contentActive($post);
	break;	

	case 'removeContentImage':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(CONTENT_TBL,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removeContentImage($id);
	break;


	case 'addLogo':
		echo $route -> addLogo($_POST);
	break;	

	case 'editLogo':
		echo $route -> editLogo($_POST);
	break;	


	case 'logoActive':
		echo $route -> logoActive($post);
	break;	

	case 'removeLogoImage':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(LOGO_TBL,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removeLogoImage($id);
	break;



	/* Partner Management*/

	case 'addPartner':
		echo $route -> addPartner($_POST);
	break;

	case 'editPartner':
		echo $route -> editPartner($_POST);
	break;	

	case 'removePartnerImage':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(PARTNERS_TBL,"partner_image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['partner_image']);
		}
		echo $route -> removePartnerImage($id);
	break;

	case 'partnerActive':
		echo $route -> partnerActive($post);
	break;

	case 'partnerDelete':
		echo $route -> partnerDelete($post);
	break;

	/*CSR management*/

	case 'addCsr' :
		echo $route -> addCsr($_POST);
	break;
	case 'editCsr' :
		echo $route -> editCsr($_POST);
	break;

	case 'csrDelete':	
		$result	= $post;
		$id 	= $route->decryptData($post);	
		$info 	= $route->getDetails(CSR_IMAGE_TBL,"image", " csr_id = '$id' ");
		echo $route->csrDelete($post);
	break;

	case 'csrVideoDelete':		
		echo $route->csrvideoDelete($_POST);
	break;

	case 'removeCsrImage':
		$result	= $post;
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(CSR_IMAGE_TBL,"image", " id= '$id' ");
		if ($info['image']!="") {
			//unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removeCsrImage($id);
	break;

	case 'addCsrVideo' :
		echo $route -> addCsrVideo($_POST);
	break;

	/*TREE management*/

	case 'addTree' :
		echo $route -> addTree($_POST);
	break;
	case 'editTree' :
		echo $route -> editTree($_POST);
	break;

	case 'treeDelete':	
		$result	= $post;
		$id 	= $route->decryptData($post);	
		$info 	= $route->getDetails(TREE_IMAGE_TBL,"image", " tree_id = '$id' ");
		echo $route->treeDelete($post);
	break;

	case 'treeVideoDelete':
		echo $route->treevideoDelete($_POST);
	break;

	case 'removeTreeImage':
		$result	= $post;
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(TREE_IMAGE_TBL,"image", " id= '$id' ");
		if ($info['image']!="") {
			//unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removeTreeImage($id);
	break;

	case 'addTreeVideo' :
		echo $route -> addTreeVideo($_POST);
	break;

}
?>