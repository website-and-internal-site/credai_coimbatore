<?php
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
$route 		= new Ajaxcontroller();


// Add New Product productimage

$errors = array();
$msg = array();

if(!empty($_FILES["cimage"]["type"]))
{
    $validextensions = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG", "pdf", "docx", "doc", "rar", "zip");
    $temporary = explode(".", $_FILES["cimage"]["name"]); 
    $file_extension = end($temporary);

       if(($_FILES["cimage"]["type"] == "image/png") || ($_FILES["cimage"]["type"] == "image/jpg") || ($_FILES["cimage"]["type"] == "image/jpeg" || $_FILES["cimage"]["type"] == "application/pdf" || $_FILES["cimage"]["type"] == "application/docx" || $_FILES["cimage"]["type"] == "application/doc" | $_FILES["cimage"]["type"] == "application/rar" | $_FILES["cimage"]["type"] == "application/zip")
            )
	{
		if(($_FILES["cimage"]["size"] < 900000000) && in_array($file_extension, $validextensions))
		{
			$rands = $route -> generateRandomString("10");
			$date = date("dmYhis");

	        if ($_FILES["cimage"]["error"] > 0)
			{
	           $errors[] = "Return Code: " . $_FILES["cimage"]["error"] . "<br/>";
	        } 
			else 
			{ 
				if (file_exists("uploads/srcimg/".$_FILES["cimage"]["name"])) {
	            	$err =  $_FILES["cimage"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
	            	$errors[] = $err;
				} 
				else 
				{					
					$sourcePath = $_FILES["cimage"]['tmp_name'];
					$post_image_name = $rands.$date.$route->hyphenize($_FILES["cimage"]['name']);
					$targetPath = "uploads/srcimg/".$post_image_name;
					move_uploaded_file($sourcePath,$targetPath) ;
					//$source_url = $targetPath;
					//$destination_url = $targetPath;
					//$process_img = $route->compress_image($source_url, $destination_url, 60);
					//$output = str_replace("uploads/customers/", "", $process_img);
					$msg[] = $post_image_name;
				}				       
	        }
		}
		else{
			$errmsg =  "Image Size Exceed 800 KB. Please upload proper Image with proper file size.<br/>";
        	$errors[] = $errmsg;
		}
    }   
	else 
	{
        $errmsg =  "Invalid Image type. Please upload images only in the JPG, JPEG, PNG or GIF Formats.<br/>";
        $errors[] = $errmsg;
    }
}else{
	$msg[] = "";
}



if(!empty($_FILES["avideo"]["type"]))
{
    $validextensions = array("MP4", "AVI", "MKV", "MOV", "GIF");
    $temporary = explode(".", $_FILES["avideo"]["name"]); 
    $file_extension = end($temporary);

    if(($_FILES["avideo"]["type"] == "video/MP4") || ($_FILES["avideo"]["type"] == "video/AVI") || ($_FILES["avideo"]["type"] == "video/MKV") || ($_FILES["avideo"]["type"] == "video/GIF")
            )
	{
		if(($_FILES["avideo"]["size"] < 900000000) && in_array($file_extension, $validextensions))
		{
			$rands = $route -> generateRandomString("10");
			$date = date("dmYhis");

	        if ($_FILES["avideo"]["error"] > 0)
			{
	           $errors[] = "Return Code: " . $_FILES["avideo"]["error"] . "<br/>";
	        } 
			else 
			{ 
				if (file_exists("uploads/srcimg/".$_FILES["avideo"]["name"])) {
	            	$err =  $_FILES["avideo"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
	            	$errors[] = $err;
				} 
				else 
				{					
					$sourcePath = $_FILES["avideo"]['tmp_name'];
					$post_image_name = $rands.$date.$route->hyphenize($_FILES["avideo"]['name']);
					$targetPath = "uploads/srcimg/".$post_image_name;
					move_uploaded_file($sourcePath,$targetPath) ;
					//$source_url = $targetPath;
					//$destination_url = $targetPath;
					//$process_img = $route->compress_image($source_url, $destination_url, 60);
					//$output = str_replace("uploads/customers/", "", $process_img);
					$msg[] = $post_image_name;
				}				       
	        }
		}
		else{
			$errmsg =  "Floor Video Size Exceed 800 KB. Please upload proper Image with proper file size.<br/>";
        	$errors[] = $errmsg;
		}
    }   
	else 
	{
        $errmsg =  "Invalid Floor Image type. Please upload images only in the JPG, JPEG, PNG or GIF Formats.<br/>";
        $errors[] = $errmsg;
    }
}else{
	$msg[] = "";
}




if(count($errors)==0){
	$success = "";
	foreach ($msg as $key =>  $value) {
		$success .= "`".$value;
	}
	echo "1".$success;
}else{
	$op = "";
	foreach ($errors as $value) {
		$op .= $value;
	}
	echo "0`".$op;
}



?>