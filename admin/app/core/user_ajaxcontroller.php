<?php
	
require_once './../../global/global-config.php';
require_once '../config/config.php';
require_once '../app/models/Model.php';
require_once 'classes/PHPMailerAutoload.php';

class Ajaxcontroller extends Model
{		

	/*--------------------------------------------- 
				Login Authentication
	----------------------------------------------*/ 

	// User login

	function userLogin($data){
		$email 		= $data['email'];
		$passw 		= $data['password'];
    	$password 	= $this->encryptPassword($passw);
		$check = $this -> check_query(ADMIN_TBL,"id"," ad_email='$email' AND ad_password='$password' AND ad_status='1' ");
		if($check == 1){
        	$userinfo = $this -> getDetails(ADMIN_TBL,"id,ad_name"," ad_email='$email' AND ad_password='$password' ");	
         	$_SESSION["admin_session_id"] 			= $userinfo["id"];  
         	$_SESSION['username']  					= $userinfo["ad_name"];  	
         	if($this->sessionIn($user_type="admin",$finance_type="credai_coimbatore",$_SESSION["admin_session_id"],"web","browser")){
         		return 1;
         	}          	
        }else{
        	return $this->errorMsg("Sorry your account details are wrong.");
        }
	}

	// Edit User Profile

	function editUserProfile($data)
	{
		$layout = "";
		if(isset($_SESSION['edit_profile_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_profile_key']){
				$id 	= $_SESSION['admin_session_id'];
				$curr 	= date("Y-m-d H:i:s");
				$query = "UPDATE ".ADMIN_TBL." SET 
							ad_name 		= '".$this->cleanString($data['a_ad_name'])."',
							ad_mobile 		= '".$this->cleanString($data['a_ad_mobile'])."',
							ad_email 		= '".$this->cleanString($data['a_ad_email'])."',
							updated_at 		= '$curr' WHERE id='$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_profile_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}	

 	// Change User Password

	function changeAdminPassword($data)
	{
		$layout = "";
		if(isset($_SESSION['change_password_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['change_password_key']){

				$curr 			= date("Y-m-d H:i:s");
				$newpassword =  $this->encryptPassword($this->cleanString($data['new_password']));
   		 		$confirmpassword =  $this->encryptPassword($this->cleanString($data['re_password']));
   		 		
   		 		 $quer = "SELECT id FROM ".ADMIN_TBL." WHERE         
			          id = '".$_SESSION['admin_session_id']."' AND
			          ad_password= '".$this->encryptPassword($this->cleanString($data['password']))."' ";
			    $result =mysql_query($quer);
			    $count= mysql_num_rows($result);

			    if($count==1){
			        if($newpassword==$confirmpassword){
			           $query= "UPDATE ".ADMIN_TBL." SET
			                ad_password  	= '". $this->encryptPassword($this->cleanString($data['re_password']))."',
			                updated_at 		= '$curr' 
			                WHERE id 		= '".$_SESSION['admin_session_id']."' "; 
			                $exe 	= mysql_query($query);

			                if($exe){
					unset($_SESSION['change_password_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			        }else{
			           echo 'Your password is Mismatch';
			        }
			     }else{
			           echo "Invalid Password";
			     }      
				
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	
	// Delete contact us
	function deleteContactUs($data)
	{
		$layout = "";
		$id = $this->decryptData($data);
		$curr   = date("Y-m-d H:i:s");
		$query  = "DELETE FROM ".CONTACT_US." WHERE id='$id' "; 
		$exe 	= mysql_query($query);
		if($exe){
			return 1;
		}else{
			return "Sorry!! Unexpected Error Occurred. Please try again.";
		}	
	}


	  /*-----------------------------------------------
    		   add case study Category
    --------------------------------------------------*/


	function addEvent($data)
	{
		
		if(isset($_SESSION['add_case_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_case_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$date  	        = $this->changeDateFormat($data['date']);
				$query = "INSERT INTO ".EVENTS_TBL." SET 
							token 				= '".$this->hyphenize($data['name'])."',
							name 				= '".$this->cleanString($data['name'])."',
							date 				= '".$date."',
							venue 				= '".$this->cleanString($data['venue'])."',
							meta_title 			= '".$this->cleanString($data['mtitle'])."',
							meta_description 	= '".$this->cleanString($data['mdescription'])."',
							keyword 			= '".$this->cleanString($data['keywords'])."',
							short_description 	= '".$this->cleanString($data['short_description'])."',
							description 		= '".$this->cleanString($data['description'])."',
							image 				= '".$this->cleanString($data['image'])."',
							status				= '1',
							added_by 			= '$user_id',	
							created_at 			= '$curr',
							updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_case_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	



	function editEvents($data)
	{
		
		if(isset($_SESSION['edit_case_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_case_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$id  			= $this->decryptData($data['token']);
				$date  	        = $this->changeDateFormat($data['date']);
				 $query = "UPDATE ".EVENTS_TBL." SET 
							token 				= '".$this->hyphenize($data['name'])."',
							name 				= '".$this->cleanString($data['name'])."',
							date 				= '".$date."',
							venue 				= '".$this->cleanString($data['venue'])."',
							meta_title 			= '".$this->cleanString($data['mtitle'])."',
							meta_description 	= '".$this->cleanString($data['mdescription'])."',
							keyword 			= '".$this->cleanString($data['keywords'])."',
							short_description 	= '".$this->cleanString($data['short_description'])."',
							description 		= '".$this->cleanString($data['description'])."',
							image 				= '".$this->cleanString($data['image'])."',
							status				= '1',
							added_by 			= '$user_id',	
							updated_at 			= '$curr'
							WHERE id 			= '$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_case_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	

	function removeEventImage($id)
	{
		$q = "UPDATE ".EVENTS_TBL." SET image = '' WHERE id='$id' ";
		$exe = mysql_query($q);
		return 1;
	}


	// Avtive & Inactive Status For Product

    function eventActive($data)
    {
        $id = $this->decryptData($data);
        $info = $this->getDetails(EVENTS_TBL,"status"," id ='$id' ");
        if($info['status'] ==1){
            $query = "UPDATE ".EVENTS_TBL." SET status='0' WHERE id='$id' ";
        }else{
            $query = "UPDATE ".EVENTS_TBL." SET status='1' WHERE id='$id' ";
        }
        $up_exe = mysql_query($query);
        if($up_exe){
            return 1;
        }
    }


  /*-----------------------------------------------
    		  Content
    --------------------------------------------------*/


	function addContent($data)
	{
		
		if(isset($_SESSION['add_content_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_content_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$output 		= str_replace("uploads/srcimg/", "", $data['image']);
	           	$image_info 	= explode(".", $output); 
				$image_type 	= end($image_info);
				$date  	        = $this->changeDateFormat($data['date']);
				$query = "INSERT INTO ".CONTENT_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							date  				= '".$date."',
							sub_title  			= '".$this->cleanString($data['sub_title'])."',
							meta_title 			= '".$this->cleanString($data['mtitle'])."',
							meta_description 	= '".$this->cleanString($data['mdescription'])."',
							link 				= '".$this->cleanString($data['link'])."',
							keyword 			= '".$this->cleanString($data['keywords'])."',
							type 				= '".$this->cleanString($data['type'])."',
							short_description 	= '".$this->cleanString($data['short_description'])."',
							description 		= '".$this->cleanString($data['description'])."',
							image 				= '".$this->cleanString($data['image'])."',
							image_alt 			= '".$this->cleanString($data['image_alt'])."',
							image_type 			= '$image_type',
							status				= '1',
							added_by 			= '$user_id',	
							created_at 			= '$curr',
							updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_content_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	



	function editContent($data)
	{
		
		if(isset($_SESSION['edit_content_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_content_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$id  			= $this->decryptData($data['token']);
				$date  	        = $this->changeDateFormat($data['date']);
				 $query = "UPDATE ".CONTENT_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							date  				= '".$date."',
							title 				= '".$this->cleanString($data['title'])."',
							sub_title  			= '".$this->cleanString($data['sub_title'])."',
							meta_title 			= '".$this->cleanString($data['mtitle'])."',
							link 				= '".$this->cleanString($data['link'])."',
							meta_description 	= '".$this->cleanString($data['mdescription'])."',
							keyword 			= '".$this->cleanString($data['keywords'])."',
							type 				= '".$this->cleanString($data['type'])."',
							short_description 	= '".$this->cleanString($data['short_description'])."',
							description 		= '".$this->cleanString($data['description'])."',
							image 				= '".$this->cleanString($data['image'])."',
							image_alt 			= '".$this->cleanString($data['image_alt'])."',
							added_by 			= '$user_id',	
							updated_at 			= '$curr'
							WHERE id 			= '$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_content_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	

	function removeContentImage($id)
	{
		$q = "UPDATE ".CONTENT_TBL." SET image = '' WHERE id='$id' ";
		$exe = mysql_query($q);
		return 1;
	}


	// Avtive & Inactive Status For Product

    function contentActive($data)
    {
        $id = $this->decryptData($data);
        $info = $this->getDetails(CONTENT_TBL,"status"," id ='$id' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CONTENT_TBL." SET status='0' WHERE id='$id' ";
        }else{
            $query = "UPDATE ".CONTENT_TBL." SET status='1' WHERE id='$id' ";
        }
        $up_exe = mysql_query($query);
        if($up_exe){
            return 1;
        }
    }




  /*-----------------------------------------------
    		  Logo
    --------------------------------------------------*/


	function addLogo($data)
	{
		
		if(isset($_SESSION['add_logo_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_logo_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$query = "INSERT INTO ".LOGO_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							type 				= '".$this->cleanString($data['type'])."',
							short_description 	= '".$this->cleanString($data['short_description'])."',
							image 				= '".$this->cleanString($data['image'])."',
							image_alt 			= '".$this->cleanString($data['image_alt'])."',
							link 				= '".$this->cleanString($data['link'])."',
							status				= '1',
							added_by 			= '$user_id',	
							created_at 			= '$curr',
							updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_logo_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	



	function editLogo($data)
	{
		
		if(isset($_SESSION['edit_logo_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_logo_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$id  			= $this->decryptData($data['token']);
				 $query = "UPDATE ".LOGO_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							type 				= '".$this->cleanString($data['type'])."',
							short_description 	= '".$this->cleanString($data['short_description'])."',
							image 				= '".$this->cleanString($data['image'])."',
							image_alt 			= '".$this->cleanString($data['image_alt'])."',
							link 				= '".$this->cleanString($data['link'])."',
							added_by 			= '$user_id',	
							updated_at 			= '$curr'
							WHERE id 			= '$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_logo_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	

	function removeLogoImage($id)
	{
		$q = "UPDATE ".LOGO_TBL." SET image = '' WHERE id='$id' ";
		$exe = mysql_query($q);
		return 1;
	}


	// Avtive & Inactive Status For Product

    function logoActive($data)
    {
        $id = $this->decryptData($data);
        $info = $this->getDetails(LOGO_TBL,"status"," id ='$id' ");
        if($info['status'] ==1){
            $query = "UPDATE ".LOGO_TBL." SET status='0' WHERE id='$id' ";
        }else{
            $query = "UPDATE ".LOGO_TBL." SET status='1' WHERE id='$id' ";
        }
        $up_exe = mysql_query($query);
        if($up_exe){
            return 1;
        }
    }



    /*-----------------------------------------------
    		 Partners Management
    --------------------------------------------------*/


	function addPartner($data)
	{
		if(isset($_SESSION['add_partner_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_partner_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$query = "INSERT INTO ".PARTNERS_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							partner_image 		= '".$this->cleanString($data['image'])."',
							link 				= '".$this->cleanString($data['link'])."',
							sort_order			= '".$this->cleanString($data['order'])."',
							status				= '1',
							delete_status		= '0',
							added_by 			= '$user_id',	
							created_at 			= '$curr',
							updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_partner_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	function editPartner($data)
	{
		if(isset($_SESSION['edit_partner_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_partner_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$id  			= $this->decryptData($data['token']);
				 $query = "UPDATE ".PARTNERS_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							partner_image 		= '".$this->cleanString($data['image'])."',
							link 				= '".$this->cleanString($data['link'])."',
							sort_order			= '".$this->cleanString($data['order'])."',
							added_by 			= '$user_id',	
							updated_at 			= '$curr'
							WHERE id 			= '$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_partner_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}
	

	function removePartnerImage($id)
	{
		$q = "UPDATE ".PARTNERS_TBL." SET partner_image = '' WHERE id='$id' ";
		echo $q;
		$exe = mysql_query($q);
		return 1;
	}

	// Partner Avtive & Inactive Status

    function partnerActive($data)
    {
        $id = $this->decryptData($data);
        $info = $this->getDetails(PARTNERS_TBL,"status"," id ='$id' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PARTNERS_TBL." SET status='0' WHERE id='$id' ";
        }else{
            $query = "UPDATE ".PARTNERS_TBL." SET status='1' WHERE id='$id' ";
        }
        $up_exe = mysql_query($query);
        if($up_exe){
            return 1;
        }
    }

    // Delete Partner

    function partnerDelete($data)
    {
        $id = $this->decryptData($data);
        $query = " DELETE FROM ".PARTNERS_TBL." WHERE id='$id' ";
        $up_exe = mysql_query($query);
        if($up_exe){
            return 1;
        }
    }

    /*------------------------------
    		CSR Management
   	-------------------------------*/

   	function addCsr($data)
	{
		if(isset($_SESSION['add_csr_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_csr_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$query = "INSERT INTO ".CSR_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							description 		= '".$this->cleanString($data['description'])."',
							sort_order			= '".$this->cleanString($data['order'])."',
							status				= '1',	
							created_at 			= '$curr',
							updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_csr_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	function editCsr($data)
	{
		if(isset($_SESSION['edit_csr_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_csr_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$id 			= $this->decryptData($data['token']);
				$curr 			= date("Y-m-d H:i:s");
				$query = "UPDATE ".CSR_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							description 		= '".$this->cleanString($data['description'])."',
							sort_order			= '".$this->cleanString($data['order'])."',
							status				= '1',	
							updated_at 			= '$curr' 
							WHERE id 			= '$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_csr_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	function csrDelete($data)
	{
		$data = $this->decryptData($data);	
		$query = "DELETE FROM ".CSR_TBL." WHERE id='$data' ";
		$up_exe = mysql_query($query);
		if($up_exe){
			$query_result = "DELETE FROM ".CSR_IMAGE_TBL." WHERE csr_id='$data' ";
			$query_result1 = "DELETE FROM ".CSR_VIDEO_TBL." WHERE csr_id='$data' ";
			$up_result = mysql_query($query_result);
			$up_result1 = mysql_query($query_result1);
			return 1;
		}else{
			return  "Unexpected Error Occurred";
		}
	}

	function csrVideoDelete($data)
	{
		$id = $this->decryptData($data['result']);
		$item = $this->decryptData($data['result2']); 	
		$query = "DELETE FROM ".CSR_VIDEO_TBL." WHERE csr_id = '$id' AND id='$item' ";
		$up_exe = mysql_query($query);
		if($up_exe){
			return 1;
		}else{
			return  "Unexpected Error Occurred";
		}
	}

	// Add Csr Image Uploads

	function addCsrImages($images=[],$token)
	{
		$id 		= $this->decryptData($token);
		$curr 		= date("Y-m-d H:i:s");
		$q="";
		if(count($images)>0){
			foreach ($images as $value) {
	           	$output = str_replace("uploads/srcimg/", "", $value);
	           	$q = "INSERT INTO ".CSR_IMAGE_TBL. " SET 
	           	csr_id			= '$id', 
	           	image 			= '$output',
	           	status			= '1',
				created_at		= '$curr',
				updated_at 		= '$curr' ";
	           	$exe = mysql_query($q);
	        }
	        return 1;
		}else{
			return $this->errorMsg("Please select images to upload.");
		}
	}

	// Add Csr Image Uploads

	function addCsrVideo($data)
	{
		$id 		= $data['token'];
		$curr 		= date("Y-m-d H:i:s");
	    $q = "INSERT INTO ".CSR_VIDEO_TBL. " SET 
	           	csr_id			= '$id', 
	           	video  			= '".$this->cleanString($data['newVideo'])."',
	           	status			= '1',
				created_at		= '$curr',
				updated_at 		= '$curr' ";
	        $exe = mysql_query($q);
	       if($exe){
	       		unset($_SESSION['add_csr_video_key']);
	       		return 1;
	       }
	       else{
	       		return "Unexpected Error Occurred";
	       }
	}

	// Remove CSR Image

	function removeCsrImage($id)
	{
		return $this->deleteRow(CSR_IMAGE_TBL," id='$id' ");
	}



	/*------------------------------
    		TREE Management
   	-------------------------------*/

   	function addTree($data)
	{
		if(isset($_SESSION['add_tree_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['add_tree_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$curr 			= date("Y-m-d H:i:s");
				$query = "INSERT INTO ".TREE_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							description 		= '".$this->cleanString($data['description'])."',
							sort_order			= '".$this->cleanString($data['order'])."',
							status				= '1',	
							created_at 			= '$curr',
							updated_at 			= '$curr' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['add_tree_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	function editTree($data)
	{
		if(isset($_SESSION['edit_tree_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_tree_key']){
				$user_id 		= $_SESSION["admin_session_id"];
				$id 			= $this->decryptData($data['token']);
				$curr 			= date("Y-m-d H:i:s");
				$query = "UPDATE ".TREE_TBL." SET 
							token 				= '".$this->hyphenize($data['title'])."',
							title 				= '".$this->cleanString($data['title'])."',
							description 		= '".$this->cleanString($data['description'])."',
							sort_order			= '".$this->cleanString($data['order'])."',
							status				= '1',	
							updated_at 			= '$curr' 
							WHERE id 			= '$id' ";
				$exe 	= mysql_query($query);
				if($exe){
					unset($_SESSION['edit_tree_key']);
					return 1;
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}	
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	function treeDelete($data)
	{
		$data = $this->decryptData($data);	
		$query = "DELETE FROM ".TREE_TBL." WHERE id='$data' ";
		$up_exe = mysql_query($query);
		if($up_exe){
			$query_result = "DELETE FROM ".TREE_IMAGE_TBL." WHERE TREE_id='$data' ";
			$query_result1 = "DELETE FROM ".TREE_VIDEO_TBL." WHERE TREE_id='$data' ";
			$up_result = mysql_query($query_result);
			$up_result1 = mysql_query($query_result1);
			return 1;
		}else{
			return  "Unexpected Error Occurred";
		}
	}

	function treeVideoDelete($data)
	{
		$id = $this->decryptData($data['result']);
		$item = $this->decryptData($data['result2']); 	
		$query = "DELETE FROM ".TREE_VIDEO_TBL." WHERE tree_id = '$id' AND id='$item' ";
		$up_exe = mysql_query($query);
		if($up_exe){
			return 1;
		}else{
			return  "Unexpected Error Occurred";
		}
	}

	// Add Tree Image Uploads

	function addTreeImages($images=[],$token)
	{
		$id 		= $this->decryptData($token);
		$curr 		= date("Y-m-d H:i:s");
		$q="";
		if(count($images)>0){
			foreach ($images as $value) {
	           	$output = str_replace("uploads/srcimg/", "", $value);
	           	$q = "INSERT INTO ".TREE_IMAGE_TBL. " SET 
	           	tree_id			= '$id', 
	           	image 			= '$output',
	           	status			= '1',
				created_at		= '$curr',
				updated_at 		= '$curr' ";
	           	$exe = mysql_query($q);
	        }
	        return 1;
		}else{
			return $this->errorMsg("Please select images to upload.");
		}
	}

	// Add Tree Image Uploads

	function addTreeVideo($data)
	{
		$id 		= $data['token'];
		$curr 		= date("Y-m-d H:i:s");
	    $q = "INSERT INTO ".TREE_VIDEO_TBL. " SET 
	           	tree_id			= '$id', 
	           	video  			= '".$this->cleanString($data['newVideo'])."',
	           	status			= '1',
				created_at		= '$curr',
				updated_at 		= '$curr' ";
	        $exe = mysql_query($q);
	       if($exe){
	       		unset($_SESSION['add_tree_video_key']);
	       		return 1;
	       }
	       else{
	       		return "Unexpected Error Occurred";
	       }
	}

	// Remove TREE Image

	function removeTreeImage($id)
	{
		return $this->deleteRow(TREE_IMAGE_TBL," id='$id' ");
	}


}

?>