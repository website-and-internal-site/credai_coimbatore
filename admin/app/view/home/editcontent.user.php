<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo COREPATH ?>content">Manage Content</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="form-error"></div>
        <form id="editContent" method="POST" action="#">
            <input type="hidden" value="<?php echo $_SESSION['edit_content_key'] ?>" name="fkey" id="fkey">
            <input type="hidden" value="<?php echo $data['token'] ?>" name="token" id="token">
            <div class="row">
                 <div class="col-md-12">
                    <div class="panel panel-color panel-custom">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-file"></i> Edit Content</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label> Title
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Enter Title" value="<?php echo $data['info']['title'] ?>" class="form-control" name="title" type="text">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>  Date
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="dd/mm/yyyy" value="<?php echo date("d/m/Y",strtotime($data['info']['date'])) ?>" class="form-control datepicker" name="date" type="text">
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sub Title
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Enter Sub Title" value="<?php echo $data['info']['sub_title'] ?>" class="form-control" name="sub_title" type="text">
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label> Link
                                            <en>*</en>
                                        </label>
                                        <input value="<?php echo $data['info']['link'] ?>" placeholder="Link" class="form-control" name="link" type="text">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label> Meta Title
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Meta Title" value="<?php echo $data['info']['meta_title'] ?>" class="form-control" name="mtitle" type="text">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label> Meta Keywords
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Meta Keywords" class="form-control" value="<?php echo $data['info']['keyword'] ?>" name="keywords" type="text">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Meta Description
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Meta Description" value="<?php echo $data['info']['meta_description'] ?>" class="form-control" name="mdescription" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type of content <en>*</en></label>
                                    <span class="clearfix"></span>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="news" value="news" name="type" <?php echo $data['info']['type']=="news" ? "checked" : ""  ?>>
                                        <label for="news"> News </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="mou" value="mou" name="type" <?php echo $data['info']['type']=="mou" ? "checked" : ""  ?>>
                                        <label for="mou"> MOU </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="go" value="go" name="type" <?php echo $data['info']['type']=="go" ? "checked" : ""  ?>>
                                        <label for="go"> GO </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="awards" value="awards" name="type" <?php echo $data['info']['type']=="awards" ? "checked" : ""  ?>>
                                        <label for="awards"> Awards by Members </label>
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Short Description
                                        </label>
                                        <textarea type="text" placeholder="Short Description"  class="form-control" name="short_description"><?php echo $data['short_desc'] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description
                                        </label>
                                        <textarea type="text" placeholder="Description" class="summernote" name="description"><?php echo $data['desc'] ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                  <?php if ($data['info']['image']=="") { ?>
                                    <div class="form-group">
                                        <label> Image
                                            <en>*</en>
                                        </label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-preview fileupload-large thumbnail"><img src="<?php echo IMGPATH ?>file_upload_icon.jpg"></div>
                                            <div>
                                              <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="cimage" name="cimage" />
                                                <input type="hidden" id="customerImage" name="image">
                                              </span>
                                              <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="form-group">
                                        <label> Image
                                            <en>*</en>
                                        </label>
                                        <input type="hidden" id="cimage" name="cimage" value="" />
                                        <input type="hidden" id="customerImage" name="image" value="<?php echo $data['info']['image'] ?>">
                                        <div class="customer_pic_edit">
                                           <?php if ($data['info']['image_type']=="pdf"){ ?>
                                                <a target="blank" href="<?php echo SRCIMG.$data['info']['image'] ?>"><i class="fa fa-file-o" style="font-size: 100px;"></i></a>
                                               
                                            <?php }else{ ?>
                                                 <img src="<?php echo SRCIMG.$data['info']['image'] ?>" width='250' class="img-thumbnail">
                                            <?php } ?>
                                            <button type="button" class="btn btn-danger removeContentImage" data-type="image" data-item="<?php echo $data['token']  ?>" ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label> Alt Image Title
                                            <en>*</en>
                                        </label>
                                        <input placeholder="Alt Image Title" value="<?php echo $data['info']['image_alt'] ?>" class="form-control" name="image_alt" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
                <div class="form_submit_footer">
                    <div class="form_footer_contents">
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                <i class="fa fa-check"></i> Update 
                            </button>
                            <a href="<?php echo COREPATH ?>content" class="btn btn-danger waves-effect waves-light m-l-5">
                                <i class="fa fa-close"></i> Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End content -->
    <?php require_once 'includes/bottom.html'; ?>
