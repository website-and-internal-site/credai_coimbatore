<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo COREPATH ?>content">Manage Content</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="form-error"></div>
        <form id="editLogo" method="POST" action="#">
            <input type="hidden" value="<?php echo $_SESSION['edit_logo_key'] ?>" name="fkey" id="fkey">
            <input type="hidden" value="<?php echo $data['token'] ?>" name="token" id="token">
            <div class="row">
                 <div class="col-md-12">
                    <div class="panel panel-color panel-custom">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-file"></i> Edit Content</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label> Title
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Enter Title" value="<?php echo $data['info']['title'] ?>" class="form-control" name="title" type="text">
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type of content <en>*</en></label>
                                    <span class="clearfix"></span>

                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="news" value="members_coimbatore" name="type" <?php echo $data['info']['type']=="members_coimbatore" ? "checked" : ""  ?>>
                                        <label for="news"> Members of CREDAI Coimbatore </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="vendors" value="vendors" name="type" <?php echo $data['info']['type']=="vendors" ? "checked" : ""  ?>>
                                        <label for="vendors"> Vendors </label>
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Short Description
                                        </label>
                                        <textarea type="text" placeholder="Short Description"  class="form-control" name="short_description"><?php echo $data['short_desc'] ?></textarea>
                                    </div>
                                </div>
                               
                                <div class="col-md-4">
                                  <?php if ($data['info']['image']=="") { ?>
                                    <div class="form-group">
                                        <label> Image
                                            <en>*</en>
                                        </label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-preview fileupload-large thumbnail"><img src="<?php echo IMGPATH ?>file_upload_icon.jpg"></div>
                                            <div>
                                              <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="cimage" name="cimage" />
                                                <input type="hidden" id="customerImage" name="image">
                                              </span>
                                              <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="form-group">
                                        <label> Image
                                            <en>*</en>
                                        </label>
                                        <input type="hidden" id="cimage" name="cimage" value="" />
                                        <input type="hidden" id="customerImage" name="image" value="<?php echo $data['info']['image'] ?>">
                                        <div class="customer_pic_edit">
                                            <img src="<?php echo SRCIMG.$data['info']['image'] ?>" width='250' class="img-thumbnail">
                                            <button type="button" class="btn btn-danger removeLogoImage" data-type="image" data-item="<?php echo $data['token']  ?>" ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label> Alt Image Title
                                            <en>*</en>
                                        </label>
                                        <input required="" placeholder="Alt Image Title" value="<?php echo $data['info']['image_alt'] ?>" class="form-control" name="image_alt" type="text">
                                    </div>
                                </div>
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <label> Link
                                            <en>*</en>
                                        </label>
                                        <input required="" value="<?php echo $data['info']['link'] ?>" placeholder="Enter Title" class="form-control" name="link" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
                <div class="form_submit_footer">
                    <div class="form_footer_contents">
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                <i class="fa fa-check"></i> Update 
                            </button>
                            <a href="<?php echo COREPATH ?>logo" class="btn btn-danger waves-effect waves-light m-l-5">
                                <i class="fa fa-close"></i> Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End content -->
    <?php require_once 'includes/bottom.html'; ?>
