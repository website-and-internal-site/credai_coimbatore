<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo BASEPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo BASEPATH ?>loans"> Manage Loans</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row minheight">
            <div class="col-md-6">
                <div class="card-box">
                    <h4 class="form-box-title"><i class="fa fa-file"></i> Company Information</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Company Name</td>
                                        <td>
                                            <?php echo $data['info']['company']  ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Website</td>
                                        <td><?php echo $data['info']['website']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><?php echo $data['info']['phone']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Company Logo</td>
                                        <td><img src="<?php echo ATTACHMENTS.$data['info']['logo'] ?>" alt=""> </td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td><?php echo $data['info']['address']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pincode</td>
                                        <td><?php echo $data['info']['pincode']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Company Start Up Date</td>
                                        <td><?php echo $data['info']['start_date']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Company Profile</td>
                                        <td><?php echo $data['info']['company_profile']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td><?php echo $data['info']['contact_name']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mobile No</td>
                                        <td><?php echo $data['info']['contact_mobile']  ?></td>
                                    </tr>
                                     <tr>
                                        <td>Email</td>
                                        <td><?php echo $data['info']['contact_email']  ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="card-box">
                    <h4 class="form-box-title"><i class="fa fa-file"></i> Md Information</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td> Name</td>
                                        <td>
                                            <?php echo $data['info']['md_name']  ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile No</td>
                                        <td><?php echo $data['info']['md_mobile']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $data['info']['md_email']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth</td>
                                        <td><?php echo $data['info']['dob']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Date of Anniversary</td>
                                        <td><?php echo $data['info']['doa']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Username</td>
                                        <td><?php echo $data['info']['user_name']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td><?php echo $data['info']['password']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Facebook</td>
                                        <td><?php echo $data['info']['facebook']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Twitter</td>
                                        <td><?php echo $data['info']['twitter']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>LinkedIn</td>
                                        <td><?php echo $data['info']['linkedin']  ?></td>
                                    </tr>
                                     <tr>
                                        <td>Youtube</td>
                                        <td><?php echo $data['info']['youtube']  ?></td>
                                    </tr>
                                   
                                   
                                </table>
                              
                            </div>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End content -->
<?php require_once 'includes/bottom.html'; ?>
