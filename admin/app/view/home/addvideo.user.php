<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo COREPATH ?>csr">Manage CSR</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?> For <?php echo $data['title'];?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="form-error"></div>
        <form id="addCsrVideo" method="POST" action="#">
            <input type="hidden" value="<?php echo $_SESSION['add_csr_video_key'] ?>" name="fkey" id="fkey">
            <input type="hidden" value="<?php echo $data['token']; ?>" name="token" id="token" >
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-color panel-custom">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-info-circle"></i> Add CSR Video</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Upload Video
                                            <en>*</en>
                                        </label>
                                        <video style="object-fit: initial;" class="video" width="430" height="160" controls></video>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-new video-new">Select Video</span>
                                            <span class="fileupload-exists video-exists" style="display:none;">Change</span>
                                            <input type="file" id="advideo" name="advideo" />
                                            <input type="hidden" name="newVideo" id="newVideo"/>
                                        </span>
                                        <span  class="btn btn-danger video-remove" style="display:none;">Remove</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
                <div class="form_submit_footer">
                    <div class="form_footer_contents">
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                <i class="fa fa-check"></i> Add 
                            </button>
                            <a href="<?php echo COREPATH ?>csr/video" class="btn btn-danger waves-effect waves-light m-l-5">
                                <i class="fa fa-close"></i> Cancel
                            </a>
                        </div>
                    </div>
                </div>
        </form>
        </div>
    </div>
    <!-- End content -->
    <?php require_once 'includes/bottom.html'; ?>
