<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Credai Coimbatore">
    <meta name="author" content="Credai Coimbatore">
    <link rel="shortcut icon" href="<?php echo IMGPATH?>favicon.ico">
    <title>
        <?php echo $data['meta_title'] ?>
    </title>
    <link href="<?php echo CSSPATH ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSSPATH ?>core.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSSPATH ?>components.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSSPATH ?>icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSSPATH ?>pages.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSSPATH ?>responsive.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo CSSPATH ?>user.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo JSPATH ?>modernizr.min.js"></script>
    <script type="text/javascript">
         var core_path = "<?php echo COREPATH ?>";
    </script>
</head>

<body>
    <div class="page_loading">
        <div class="loading_image">
            <p class="load_img"><img  src="<?php echo IMGPATH ?>loader.gif" width=""></p>
            <p class="load_text">Loading Please Wait...</p>
        </div>
    </div>
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <?php if(isset($_GET['r'])) { ?>
            <div class="alert alert-block alert-success fade in block-inner">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p><strong><i class="fa fa-check"></i> Password Reset Successfully!!</strong></p>
            </div>
        <?php } ?>
        <div class="card-box login_wrap">
            <div class="panel-body">
                <p><img src="<?php echo ASSETS_PATH ?>logo.png"></p>
                <div class="form-error"></div>
                <form class="form-horizontal m-t-20" id="login_auth">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="email" name="email"  required="" placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" required="" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                    <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="<?php echo COREPATH."login/forgotpassword" ?>" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
    <script>
    var resizefunc = [];
    </script>
    <!-- jQuery  -->
    <script src="<?php echo JSPATH ?>jquery.min.js"></script>
    <script src="<?php echo JSPATH ?>bootstrap.min.js"></script>
    <script src="<?php echo JSPATH ?>jquery.core.js"></script>   
    <script src="<?php echo JSPATH ?>validate.min.js"></script>
    <script src="<?php echo JSPATH ?>user.js"></script>
    
</body>

</html>
