    <?php require_once 'includes/top.html'; ?>
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-2">
                         <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                            </li>
                            <li class="active">
                                <?php echo  $data['page_title']; ?> For <?php echo $data['title'];?>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
      
           <?php if(isset($_GET['a'])) { ?>
                <div class="alert alert-block alert-success fade in block-inner">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <p><strong><i class="fa fa-check"></i> TREE Added successfully !!</strong></p>
                </div>
            <?php } ?>
            
            <?php if(isset($_GET['e'])) { ?>
                <div class="alert alert-block alert-success fade in block-inner">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <p><strong><i class="fa fa-check"></i> TREE Details Updated Successfully !!</strong></p>
                </div>
            <?php } ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="text-right m-b-5">
                        <a href="<?php echo COREPATH.'tree/addvideo/'.$data['videotoken'] ?>" class="btn btn-primary">Add Video</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <span class="clearfix"></span>
                <div class="col-md-12">
                    <div class="card-box table-responsive">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $data['list'] ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End content -->
<?php require_once 'includes/bottom.html'; ?>