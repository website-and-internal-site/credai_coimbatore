

<!-- edit Profile-->

<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo COREPATH ?>Home">Home</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <form id="editUserProfile" method="POST" action="#">
            <input type="hidden" value="<?php echo $_SESSION['edit_profile_key'] ?>" name="fkey" id="fkey">
            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-error"></div>
                </div>
                <div class="col-md-4">
                    <div class="card-box">
                        <h4 class="form-box-title"><i class=" md-business"></i> Basic Information</h4>

                     
                        <div class="form-group">
                            <label>Name <en>*</en></label>
                            <input required="" placeholder="Enter First name" value="<?php echo  $data['info']['ad_name'] ?>" class="form-control" id="a_ad_name" name="a_ad_name" type="text">
                        </div>
                         <div class="form-group">
                            <label>Mobile Number <en>*</en></label>
                            <input required="" placeholder="Mobile Number " value="<?php echo  $data['info']['ad_mobile'] ?>" class="form-control" name="a_ad_mobile" type="text">
                        </div>
                        <div class="form-group">
                            <label>Email Address <en>*</en></label>
                            <input required="" placeholder="Email Address" value="<?php echo  $data['info']['ad_email'] ?>" class="form-control" name="a_ad_email" type="email">
                        </div>  
                    </div>
                </div>
            </div>
            <div class="form_submit_footer">
                <div class="form_footer_contents">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            <i class="fa fa-check"></i>
                            Update Profile
                        </button>
                        <a href="<?php echo COREPATH ?>home" class="btn btn-danger waves-effect waves-light m-l-5">
                            <i class="fa fa-close"></i>
                            Cancel
                        </a>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
</div>
<!-- End content -->
<?php require_once 'includes/bottom.html'; ?>
