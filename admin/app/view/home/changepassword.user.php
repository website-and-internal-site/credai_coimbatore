<!-- change Password-->

<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo COREPATH ?>employee"> Manage Employees</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <form id="changeAdminPassword" method="POST" action="#">
            <input type="hidden" value="<?php echo $_SESSION['change_password_key'] ?>" name="fkey" id="fkey">
            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-error"></div>
                </div>
                <div class="col-md-4">
                    <div class="card-box">
                        <h4 class="form-box-title"><i class=" md-business"></i>Change Password</h4>

                     
                        <div class="form-group">
                            <label>Current Password <en>*</en></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Current Password">
                            <input type="hidden" id="password_id" name="password_id" >
                        </div>
                         <div class="form-group">
                            <label>New Password <en>*</en></label>
                            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password <en>*</en></label>
                            <input type="password" class="form-control" id="re_password" name="re_password" placeholder="Retype Password">
                        </div>  
                    </div>
                </div>
            </div>
            <div class="form_submit_footer">
                <div class="form_footer_contents">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            <i class="fa fa-check"></i>
                            Update Password
                        </button>
                        <a href="<?php echo COREPATH ?>home" class="btn btn-danger waves-effect waves-light m-l-5">
                            <i class="fa fa-close"></i>
                            Cancel
                        </a>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
</div>
<!-- End content -->
<?php require_once 'includes/bottom.html'; ?>