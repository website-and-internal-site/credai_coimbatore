<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo BASEPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo BASEPATH ?>loans"> Manage Loans</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['page_title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row minheight">
            <div class="col-md-8">
                <div class="card-box">
                    <h4 class="form-box-title"><i class="fa fa-file"></i> General Information</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Name</td>
                                        <td>
                                            <?php echo $data['info']['name']  ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Age</td>
                                        <td><?php echo $data['info']['age']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $data['info']['email']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><?php echo $data['info']['phone']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Landline</td>
                                        <td><?php echo $data['info']['landline']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address for communication</td>
                                        <td><?php echo $data['info']['address']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Name of the Property developer</td>
                                        <td><?php echo $data['info']['name_of_property_developer']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Name of the Project</td>
                                        <td><?php echo $data['info']['project_name']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address of the Project</td>
                                        <td><?php echo $data['info']['project_address']  ?></td>
                                    </tr>
                                    <tr>
                                        <td>Grievance Details</td>
                                        <td><?php echo $data['info']['lodged_any_complaint']  ?></td>
                                    </tr>
                                   
                                   
                                </table>
                              
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card-box">
                                <h4>Whether the details of this complaint in full or in part form the subject of any law suit before any court of law / Consumer court? </h4>
                                <p><?php echo $data['info']['complaint_any_court'] ?> </p>

                                <h4>If your answer to question above is “yes”, are you approaching the CGRF to try for a mediation and thereby an out of court settlement?* </h4>
                                <p><?php echo $data['info']['out_of_court_settlement'] ?> </p>

                                <h4>Have you lodged any Complaint / Greivance with the CGRF at CREDAI- Coimbatore or any other chapter of CREDAI earlier on the same subject on the same developer?  </h4>
                                <p><?php echo $data['info']['complaint_any_court'] ?> </p>
                            </div>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                </div>
               
            </div>
            <div class="col-md-4">
                <div class="card-box">
                    <h4 class="form-box-title"> <i class="fa fa-image"></i> Attachments </h4>
                    <div class="row">
                        <?php echo $data['files'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End content -->
<?php require_once 'includes/bottom.html'; ?>
