    <?php require_once 'includes/top.html'; ?>
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-2">
                         <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                            </li>
                            <li class="active">
                                <?php echo  $data['page_title']; ?>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
      
          
            <div class="row">
                <span class="clearfix"></span>
                <div class="col-md-12">
                    <div class="card-box table-responsive">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Subject </th>
                                    <th>Message </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $data['list'] ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End content -->
<?php require_once 'includes/bottom.html'; ?>