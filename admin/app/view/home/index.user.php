<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Dashboard</h4>
                <p class="text-muted page-title-alt">Welcome <?php echo $_SESSION['username'] ?>
                   
            </div>
        </div>
        <div class="col-md-12">
            <?php if(isset($_GET['u'])) { ?>
            <div class="alert alert-block alert-success fade in block-inner">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p><strong><i class="fa fa-check"></i> New Password Updated successfully !!</strong></p>
            </div>
            <?php } ?>
            <?php if(isset($_GET['e'])) { ?>
            <div class="alert alert-block alert-success fade in block-inner">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p><strong><i class="fa fa-check"></i> Profile Updated Successfully !!</strong></p>
            </div>
            <?php } ?>
        </div>

    </div>
</div>
</div>
</div>
<!-- container -->
</div>
<?php require_once 'includes/bottom.html'; ?>
