<?php require_once 'includes/top.html'; ?>
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title"><?php echo  $data['page_title']; ?></h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo COREPATH ?>"><i class="fa fa-home"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo COREPATH ?>csr">Manage CSR</a>
                        </li>
                        <li class="active">
                            <?php echo  $data['info']['title']; ?>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="form-error"></div>
        <form id="addCsrImage" method="POST" action="#" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $_SESSION['csr_image_key'] ?>" name="fkey" id="fkey">
            <input type="hidden" value="<?php echo $data['token']?>" name="token">
            <div class="row">
                 <div class="col-md-12">
                    <div class="panel panel-color panel-custom">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-info-circle"></i> Add CSR <?php echo $data['info']['title']; ?> Images</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="image_list" id="masonry">
                                        <?php echo $data['images'] ?> 
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <input type="file" multiple="multiple" data-jfiler-extensions="jpg, jpeg, png, gif" name="files[]" id="input2">
                                    <div class="tips_img_up">
                                        <h3>Note:</h3>
                                        <ul>
                                            <li> Upload Images in a set of 10.</li>
                                            <li>Upload Images in Width * Height(900 * 600).</li>
                                            <li> Upload Images in the form of JPEG, PNG or GIF Formats alone with a max Size of 3.5 MB.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
                <div class="form_submit_footer">
                    <div class="form_footer_contents">
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                <i class="fa fa-check"></i> Add 
                            </button>
                            <a href="<?php echo COREPATH ?>csr" class="btn btn-danger waves-effect waves-light m-l-5">
                                <i class="fa fa-close"></i> Cancel
                            </a>
                        </div>
                    </div>
                </div>
        </form>
        </div>
    </div>
    <!-- End content -->
    <?php require_once 'includes/bottom.html'; ?>
