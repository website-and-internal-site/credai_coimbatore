<?php

require_once 'Model.php';

class User extends Model
{	

	/*--------------------------------------------- 
					User Info
	----------------------------------------------*/


	// General User info for all pages

	function userInfo($id)
	{
		$today = date("Y-m-d");
		$query = "SELECT id,(ad_name) as username, ad_status FROM ".ADMIN_TBL." WHERE id ='".$id."' ";
		$exe = mysql_query($query);
		$list = mysql_fetch_assoc($exe);
		return $list;
	}
  


  	/*-----------------------------------------------------------
						Contact Us management
	-------------------------------------------------------------*/

	// Manage Contact Us

	function manageContactUs()
  	{
  		$layout = "";
	    $q = "SELECT *  FROM ".CONTACT_US." WHERE delete_status='0' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    		<tr id='cotact_row_$i'>
	    			<td>$i</td>
	    			<td>".date("d/m/Y",strtotime($list['created_at']))."</td>
	    			<td>".$this->unHyphenize($list['name'])."</td>
	    			<td>".$this->unHyphenize($list['mobile'])."</td>
	    			<td>".$this->unHyphenize($list['email'])."</td>
	    			<td>".$list['subject']."</td>
	    			<td>".$list['message']."</td>
	    			<td>
	    				<a href='javascript:void();' data-id='".$this->encryptData($list['id'])."' data-option='".$i."' class='btn btn-danger btn-sm deleteContactUs'><span class='fa fa-trash'></span></a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

    

	/*----------------------------------------------------------------
  							Event Management
  	-------------------------------------------------------------------*/

  	

	function manageEvents()
  	{
  		$layout = "";
	    $q = "SELECT C.id,C.name,C.description,C.status,C.image,C.date FROM ".EVENTS_TBL." C  WHERE 1 " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img width='100' src='".SRCIMG.$list['image']."' alt=''></td>
	    			<td>".date("d/m/Y",strtotime($list['date']))."</td>
	    			<td>".$list['name']."</td>
	    			
	    			<td>
	    				<div class='checkbox checkbox-custom eventActive' data-option='".$this->encryptData($list['id'])."'>
                        	<input id='check_$i' type='checkbox'  $status_c> 
                            <label for='check_$i' class='$status_class'>
                                $status
                            </label>
                        </div>
	    			</td>
	    			<td>
	    				<a href='".COREPATH."events/edit/".$list['id']."' class='btn btn-danger btn-sm' ><span class='fa fa-pencil'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	/*----------------------------------------------------------------
  								Content
  	-------------------------------------------------------------------*/


	

	function manageContent()
  	{
  		$layout = "";
	    $q = "SELECT C.id,C.title,C.created_at,C.status,C.image,C.date FROM ".CONTENT_TBL." C WHERE 1 ORDER BY id DESC " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td>".$list['title']."</td>
	    			<td>".date("d/m/Y",strtotime($list['date']))."</td>
	    			<td>
	    				<div class='checkbox checkbox-custom contentActive' data-option='".$this->encryptData($list['id'])."'>
                        	<input id='check_$i' type='checkbox'  $status_c> 
                            <label for='check_$i' class='$status_class'>
                                $status
                            </label>
                        </div>
	    			</td>
	    			<td>
	    				<a href='".COREPATH."content/edit/".$list['id']."' class='btn btn-danger btn-sm' ><span class='fa fa-pencil'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



	function manageLogo()
  	{
  		$layout = "";
	    $q = "SELECT C.id,C.title,C.link,C.status,C.image FROM ".LOGO_TBL." C WHERE 1 ORDER BY id DESC" ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img width='100' src='".SRCIMG.$list['image']."' alt=''></td>
	    			<td>".$list['title']."</td>
	    			<td>".$list['link']."</td>
	    			<td>
	    				<div class='checkbox checkbox-custom logoActive' data-option='".$this->encryptData($list['id'])."'>
                        	<input id='check_$i' type='checkbox'  $status_c> 
                            <label for='check_$i' class='$status_class'>
                                $status
                            </label>
                        </div>
	    			</td>
	    			<td>
	    				<a href='".COREPATH."logo/edit/".$list['id']."' class='btn btn-danger btn-sm' ><span class='fa fa-pencil'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*----------------------------------------------------------
  						Partners Management
  	-----------------------------------------------------------*/

  	function managePartners()
  	{
  		$layout = "";
	    $q = "SELECT id,title,link,status,partner_image FROM ".PARTNERS_TBL." WHERE 1 ORDER BY id DESC" ; 
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img width='100' src='".SRCIMG.$list['partner_image']."' alt=''></td>
	    			<td>".$list['title']."</td>
	    			<td>".$list['link']."</td>
	    			<td>
	    				<div class='checkbox checkbox-custom partnerActive' data-option='".$this->encryptData($list['id'])."'>
                        	<input id='check_$i' type='checkbox'  $status_c> 
                            <label for='check_$i' class='$status_class'>
                                $status
                            </label>
                        </div>
	    			</td>
	    			<td>
	    				<a href='".COREPATH."partners/edit/".$list['id']."' class='btn btn-primary btn-sm' ><span class='fa fa-pencil'></span> </a>
	    				<a href='javascript:void(0);' class='btn btn-danger partnerDelete btn-sm' data-option='".$this->encryptData($list['id'])."'><span class='fa fa-trash'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	/*-----------------------------------------------------------
						Career management
	-------------------------------------------------------------*/


	function manageCareer()
  	{
  		$layout = "";
	    $q = "SELECT *  FROM ".CAREER_TBL." WHERE delete_status='0' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    		<tr id='cotact_row_$i'>
	    			<td>$i</td>
	    			<td>".date("d/m/Y",strtotime($list['created_at']))."</td>
	    			<td>".$this->unHyphenize($list['name'])."</td>
	    			<td>".$this->unHyphenize($list['email'])."</td>
	    			<td>".$list['message']."</td>
	    			<td><a href='".ATTACHMENTS.$list['file']."' target='_blank' class='btn btn-success btn-sm'>View</a></td>
	    			
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}




  	/*-----------------------------------------------------------
						Career CGRF
	-------------------------------------------------------------*/


	function manageCGRF()
  	{
  		$layout = "";
	    $q = "SELECT *  FROM ".CGRF_TBL." WHERE delete_status='0' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    		<tr id='cotact_row_$i'>
	    			<td>$i</td>
	    			<td>".date("d/m/Y",strtotime($list['created_at']))."</td>
	    			<td>".($list['name'])."</td>
	    			<td>".($list['age'])."</td>
	    			<td>".$list['phone']."</td>
	    			<td>".($list['email'])."</td>
	    			
	    			<td><a href='".COREPATH."contact/viewcgrf/".$list['id']."' target='_blank' class='btn btn-success btn-sm'>View</a></td>
	    			
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageCGRFAttachments($id="")
  	{
  		$layout = "";
	    $q = "SELECT *  FROM ".FILE_TBL." WHERE reference_id='$id' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "<div class='col-md-6' style='margin-bottom:20px'>
						    	<a target='_blank' href='".ATTACHMENTS.$list['files']."'><i class='fa fa-file' style='font-size: 100px;'></i></a>
							</div>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}



  	function manageRegistration()
  	{
  		$layout = "";
	    $q = "SELECT *  FROM ".MEMBER_REGISTRATION." WHERE delete_status='0' " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    		<tr id='cotact_row_$i'>
	    			<td>$i</td>
	    			<td>".$list['start_date']."</td>
	    			<td>".($list['company'])."</td>
	    			<td>".($list['phone'])."</td>
	    			<td>".$list['website']."</td>
	    			<td>".($list['contact_name'])."</td>
	    			
	    			<td><a href='".COREPATH."contact/viewregistration/".$list['id']."' target='_blank' class='btn btn-success btn-sm'>View</a></td>
	    			
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	/*---------------------------------
			Manage CSR
	-----------------------------------*/


  	function manageCsr()
  	{
  		$layout = "";
	   	$q = "SELECT M.id,M.title,M.status,M.sort_order,(SELECT image FROM ".CSR_IMAGE_TBL." WHERE csr_id = M.id LIMIT 1 ) as image,
	   	(SELECT COUNT(id) FROM ".CSR_IMAGE_TBL." WHERE csr_id = M.id LIMIT 1 ) as image_count FROM ".CSR_TBL." M WHERE 1 ORDER BY sort_order ASC " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
				$status_c = (($list['status']==1) ? "checked" : " ");
				$status_class = (($list['status']==1) ? "text-success" : "text-danger");	

				$image = (($list['image']=='') ? 'no_image.png' : $list['image'] );

	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img src=".SRCIMG."".$image." width='60' class='img-thumbnail' ></td>
	    			<td>".$this->unHyphenize($list['title'])."</td>
	    			<td>".($list['image_count'])."</td>
	    			<td>".($list['sort_order'])."</td>
	    			<td>
	    				<div class='checkbox checkbox-custom csrStatus' data-option='".$this->encryptData($list['id'])."'>
	                        <input id='check_$i' type='checkbox'  $status_c>
	                        <label for='check_$i' class='$status_class'>
	                            $status
	                        </label>
	                    </div>
	    			</td>
	    			<td>
	    				<a href='".COREPATH."csr/edit/".$list['id']."' class='btn btn-success btn-sm' ><span class='fa fa-pencil'></span> </a>
	    				<a href='".COREPATH."csr/images/".$list['id']."' class='btn btn-warning btn-sm' ><span class='fa fa-picture-o'></span> </a>
	    				<a href='javascript:void(0);' data-id='".$this->encryptData($list['id'])."' class='btn btn-danger btn-sm csrDelete' ><span class='fa fa-trash'></span> </a>
	    				<a href='".COREPATH."csr/video/".$list['id']."' class='btn btn-primary btn-sm' ><span class='fa fa-video-camera'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageCsrVideo($videotoken)
  	{
  		$layout = "";
	   	$q = "SELECT M.id,M.title,M.status,M.sort_order,V.id as videoid,(SELECT image FROM ".CSR_IMAGE_TBL." WHERE csr_id = M.id LIMIT 1 ) as image FROM ".CSR_TBL." M LEFT JOIN ".CSR_VIDEO_TBL." V ON (M.id = V.csr_id) WHERE V.csr_id = '$videotoken' ORDER BY sort_order ASC " ;

	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
				$status_c = (($list['status']==1) ? "checked" : " ");
				$status_class = (($list['status']==1) ? "text-success" : "text-danger");	

				$image = (($list['image']=='') ? 'no_image.png' : $list['image'] );

	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img src=".SRCIMG."".$image." width='60' class='img-thumbnail' ></td>
	    			<td>".$this->unHyphenize($list['title'])."</td>
	    			<td>
	    				<a href='javascript:void(0);' data-id='".$this->encryptData($list['id'])."' data-item='".$this->encryptData($list['videoid'])."' class='btn btn-danger btn-sm csrVideoDelete' ><span class='fa fa-trash'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Manage Awards Images

  	function getCsrImages($csr_id)
  	{
  		$layout = "";
  		$q = "SELECT id,image FROM ".CSR_IMAGE_TBL." WHERE csr_id='$csr_id' ORDER BY id ASC" ;
  		$query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    			<li id='pro_$i' class='col-masonry'>
	    				<div class='product_image_item'>
	    					<img src='".SRCIMG.$list['image']."' class='img-responsive' />
	    					<p><a href='javascript:void(0);' class='removeCsrImage' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='fa fa-trash-o'></i></a> </p>
	    					<span class='clearfix'></span>
	    				</div>
	    			</li>";
	    			$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*---------------------------------
			Manage TREE
	-----------------------------------*/

  	function manageTree()
  	{
  		$layout = "";
	   	$q = "SELECT M.id,M.title,M.status,M.sort_order,(SELECT image FROM ".TREE_IMAGE_TBL." WHERE tree_id = M.id LIMIT 1 ) as image,
	   	(SELECT COUNT(id) FROM ".TREE_IMAGE_TBL." WHERE tree_id = M.id LIMIT 1 ) as image_count FROM ".TREE_TBL." M WHERE 1 ORDER BY sort_order ASC " ;
	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
				$status_c = (($list['status']==1) ? "checked" : " ");
				$status_class = (($list['status']==1) ? "text-success" : "text-danger");	

				$image = (($list['image']=='') ? 'no_image.png' : $list['image'] );

	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img src=".SRCIMG."".$image." width='60' class='img-thumbnail' ></td>
	    			<td>".$this->unHyphenize($list['title'])."</td>
	    			<td>".($list['image_count'])."</td>
	    			<td>".($list['sort_order'])."</td>
	    			<td>
	    				<div class='checkbox checkbox-custom treeStatus' data-option='".$this->encryptData($list['id'])."'>
	                        <input id='check_$i' type='checkbox'  $status_c>
	                        <label for='check_$i' class='$status_class'>
	                            $status
	                        </label>
	                    </div>
	    			</td>
	    			<td>
	    				<a href='".COREPATH."tree/edit/".$list['id']."' class='btn btn-success btn-sm' ><span class='fa fa-pencil'></span> </a>
	    				<a href='".COREPATH."tree/images/".$list['id']."' class='btn btn-warning btn-sm' ><span class='fa fa-picture-o'></span> </a>
	    				<a href='javascript:void(0);' data-id='".$this->encryptData($list['id'])."' class='btn btn-danger btn-sm treeDelete' ><span class='fa fa-trash'></span> </a>
	    				<a href='".COREPATH."tree/video/".$list['id']."' class='btn btn-primary btn-sm' ><span class='fa fa-video-camera'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageTreeVideo($videotoken)
  	{
  		$layout = "";
	   	$q = "SELECT M.id,M.title,M.status,M.sort_order,V.tree_id,V.id as videoid, (SELECT image FROM ".TREE_IMAGE_TBL." WHERE tree_id = M.id LIMIT 1 ) as image FROM ".TREE_TBL." M LEFT JOIN ".TREE_VIDEO_TBL." V ON (M.id = V.tree_id) WHERE V.tree_id = '$videotoken' ORDER BY sort_order ASC " ;

	    $query = mysql_query($q);
	    if(mysql_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$status = (($list['status']==1) ? "Active" : "Inactive"); 
				$status_c = (($list['status']==1) ? "checked" : " ");
				$status_class = (($list['status']==1) ? "text-success" : "text-danger");	

				$image = (($list['image']=='') ? 'no_image.png' : $list['image'] );

	    		$layout .= "
	    		<tr>
	    			<td>$i</td>
	    			<td><img src=".SRCIMG."".$image." width='60' class='img-thumbnail' ></td>
	    			<td>".$this->unHyphenize($list['title'])."</td>
	    			<td>
	    				<a href='javascript:void(0);' data-id='".$this->encryptData($list['id'])."' data-item='".$this->encryptData($list['videoid'])."' class='btn btn-danger btn-sm treeVideoDelete' ><span class='fa fa-trash'></span> </a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Manage Awards Images

  	function getTreeImages($tree_id)
  	{
  		$layout = "";
  		$q = "SELECT id,image FROM ".TREE_IMAGE_TBL." WHERE tree_id='$tree_id' ORDER BY id ASC" ;
  		$query = mysql_query($q);
	    if(mysql_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysql_fetch_array($query)){
	    		$layout .= "
	    			<li id='pro_$i' class='col-masonry'>
	    				<div class='product_image_item'>
	    					<img src='".SRCIMG.$list['image']."' class='img-responsive' />
	    					<p><a href='javascript:void(0);' class='removeTreeImage' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='fa fa-trash-o'></i></a> </p>
	    					<span class='clearfix'></span>
	    				</div>
	    			</li>";
	    			$i++;
	    	}
	    }
	    return $layout;
  	}

/*End Tag*/

}