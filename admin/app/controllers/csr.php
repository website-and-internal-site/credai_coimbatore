<?php

class Csr extends Controller
{

	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/managecsr', 
				array(	
					'active_menu' 	=> 'csr',
					'meta_title'  	=> 'Manage CSR - '.COMPANY_NAME,
					'page_title'  	=> 'Manage CSR',
					'scripts'		=> 'crs',
					'list' 			=>  $user->manageCsr(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}
	

	public function add()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_csr_key'])){
				$_SESSION['add_csr_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addcsr', 
				array(	
					'active_menu' 	=> 'add_csr',
					'meta_title'	=> 'Add CSR - '.COMPANY_NAME,
					'page_title'  	=> 'Add CSR ',
					'scripts'		=> 'add_csr',	
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}


	public function edit($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_csr_key'])){
				$_SESSION['edit_csr_key'] = $user->generateRandomString("40");
			}
	        $check = $user->check_query(CSR_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(CSR_TBL,"*"," id='$token' ");
	            $this->view('home/editcsr', 
	              array(
	                    'active_menu' 	=> 'csr',
						'meta_title'	=> 'Edit CSR - '.COMPANY_NAME,
						'page_title'  	=> 'Edit CSR ',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($token),
						'scripts'		=> 'csr',
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}

	public function images($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['csr_images_key'])){
				$_SESSION['csr_images_key'] = $user->generateRandomString("40");
			}
			$check = $user->check_query(CSR_TBL,"id"," id='$token' ");
	        if($check){
				$info = $user->getDetails(CSR_TBL,"*"," id='$token'  ");
				$this->view('home/addcsrimages', 
					[	
						'active_menu' 	  	=> 'csr',
						'meta_title'	  	=> 'Add CSR Images - '.COMPANY_NAME,
						'page_title'  	  	=> 'Add CSR Images',
						'redirect'		  	=> '',
						'images'			=>	$user->getCsrImages($token),
						'token'			  	=>  $user->encryptData($token),
						'info'			  	=>  $info,
						'scripts'		  	=> 'csr',
						'user'   		  	=>  $user->userInfo($_SESSION["admin_session_id"])
					]);
			}else{
				$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'active_menu' 	=> 'awards',
						'user'   		=>  $user->userInfo($_SESSION["smaavins_admin_id"])
					]);	
			}	
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);				
	    }
	}

	public function video($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$title		= $user->getDetails(CSR_TBL, "title", "id ='$token'");
			$this->view('home/managevideo', 
				array(	
					'active_menu' 	=> 'csr',
					'meta_title'  	=> 'Manage CSR - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Video',
					'scripts'		=> 'crs',
					'videotoken'	=>  $token,
					'title'			=>  $title['title'],
					'list' 			=>  $user->manageCsrvideo($token),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

	public function addvideo($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_csr_video_key'])){
				$_SESSION['add_csr_video_key'] = $user->generateRandomString("40");
			}
			$title		= $user->getDetails(CSR_TBL, "title", "id ='$token'");
			$this->view('home/addvideo', 
				array(	
					'active_menu' 	=> 'add_csr',
					'meta_title'	=> 'Add CSR  - '.COMPANY_NAME,
					'page_title'  	=> 'Add CSR Video ',
					'scripts'		=> 'add_csr',
					'title'			=>  $title['title'],
					'token'			=>  $token,
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function error()
	{	
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>