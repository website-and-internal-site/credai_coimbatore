<?php

class Logo extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$check 		= $user->check_query(LOGO_TBL,"id"," 1 ");
			$this->view('home/managelogo', 
				array(	
					'active_menu' 	=> 'logo',
					'meta_title'  	=> 'Manage Logo - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Logo',
					'scripts'		=> 'logo',
					'check' 		=>  $check,
					'list' 			=>  $user->manageLogo(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}



	public function add()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_logo_key'])){
				$_SESSION['add_logo_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addlogo', 
				array(	
					'active_menu' 	=> 'add_logo',
					'meta_title'	=> 'Add Logo - '.COMPANY_NAME,
					'page_title'  	=> 'Add Logo ',
					'scripts'		=> 'add_logo',	
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}


	public function edit($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_logo_key'])){
				$_SESSION['edit_logo_key'] = $user->generateRandomString("40");
			}
	        $check = $user->check_query(LOGO_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(LOGO_TBL,"*"," id='$token' ");
	            $this->view('home/editlogo', 
	              array(
	                    'active_menu' 	=> 'logo',
						'meta_title'	=> 'Edit Logo - '.COMPANY_NAME,
						'page_title'  	=> 'Edit Logo ',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($token),
						'scripts'		=> 'blog',
						'short_desc' 	=>  $user->publishContent($info['short_description']),
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}


	public function error()
	{	
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>