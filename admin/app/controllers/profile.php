<?php

class Profile extends Controller
{
	public function index()
	{
	    if(isset($_SESSION['admin_session_id'])){
	      $user = $this->model('User');
	      if(!isset($_SESSION['change_password_key'])){
					$_SESSION['change_password_key'] = $user->generateRandomString("40");
				}
	      $this->view('home/changepassword', 
	     array(  
	        'active_menu'   => 'changepassword',
	        'meta_title'    => 'Change Password',
	        'page_title'    => 'Change Password',
	        'token'			=>	$user->encryptData($_SESSION["admin_session_id"]),
	        'user'   		=>  $user->userInfo($_SESSION["admin_session_id"]) 

	      ));
	    }else{
	      $this->view('home/login', 
	       array(
	          'meta_title'  => 'Admin Login | '.COMPANY_NAME,
	          'page_title'  => 'Admin Login'
	        ));
	    }
	}


	 public function edit()
	 {
	  	if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_profile_key'])){
					$_SESSION['edit_profile_key'] = $user->generateRandomString("40");
			}
		            $info = $user->getDetails(ADMIN_TBL,"*"," id='".$_SESSION["admin_session_id"]."'  ");	
	   		 		$this->view('home/editprofile', 
				      array( 
				        'active_menu'   => 'editprofile',      
				        'meta_title'    => 'Update Profile',
				        'page_title'    => 'Update Profile',
				        'info'			=>	$info,
				        'token'			=>	$user->encryptData($info['id']),
						'user'   		=>  $user->userInfo($_SESSION["admin_session_id"]),
				        
				      ));
				}else{ 
				$this->view('home/login',
					array(
						'meta_title'=> 'Admin Login - '.COMPANY_NAME
					));

	   		}
	 }



	public function error()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"]),
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}



}


?>	


