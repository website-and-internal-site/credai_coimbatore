<?php

class Events extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/manageevents', 
				array(	
					'active_menu' 	=> 'events',
					'meta_title'  	=> 'Manage Events - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Events',
					'scripts'		=> 'events',
					'list' 			=>  $user->manageEvents(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}


	public function add()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_case_key'])){
				$_SESSION['add_case_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addevent', 
				array(	
					'active_menu' 	=> 'add_events',
					'meta_title'	=> 'Add Events - '.COMPANY_NAME,
					'page_title'  	=> 'Add Events ',
					'scripts'		=> 'events',	
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}



	public function edit($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_case_key'])){
				$_SESSION['edit_case_key'] = $user->generateRandomString("40");
			}
	        $check = $user->check_query(EVENTS_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(EVENTS_TBL,"*"," id='$token' ");
	            $this->view('home/editevent', 
	              array(
	                    'active_menu' 	=> 'events',
						'meta_title'	=> 'Edit Events - '.COMPANY_NAME,
						'page_title'  	=> 'Edit Events ',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($token),
						'scripts'		=> 'events',
						'desc' 			=>  $user->publishContent($info['description']),
						'short_desc' 	=>  $user->publishContent($info['short_description']),
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}


	public function error()
	{	
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>