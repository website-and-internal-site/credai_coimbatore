<?php

class Contact extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/managecontact', 
				array(	
					'active_menu' 	=> 'contact',
					'meta_title'  	=> 'Manage Contact - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Contact',
					'scripts'		=> 'contact',
					'list' 			=>  $user->manageContactUs(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}



	public function career()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/managecareer', 
				array(	
					'active_menu' 	=> 'Career',
					'meta_title'  	=> 'Manage Career - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Career',
					'scripts'		=> 'Career',
					'list' 			=>  $user->manageCareer(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

	public function cgrf()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/managecgrf', 
				array(	
					'active_menu' 	=> 'cgrf',
					'meta_title'  	=> 'Manage CGRF - '.COMPANY_NAME,
					'page_title'  	=> 'Manage CGRF',
					'scripts'		=> 'cgrf',
					'list' 			=>  $user->manageCGRF(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}


	public function viewcgrf($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
	        $check = $user->check_query(CGRF_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(CGRF_TBL,"*"," id='$token' ");
	            $this->view('home/viewcgrf', 
	              array(
	                    'active_menu' 	=> 'content',
						'meta_title'	=> 'View CGRF - '.COMPANY_NAME,
						'page_title'  	=> 'View CGRF ',
						'info'			=>	$info,
						'scripts'		=> 'content',
						'files' 			=> $user->manageCGRFAttachments($token),
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}

	public function registration()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/manageregistration', 
				array(	
					'active_menu' 	=> 'registration',
					'meta_title'  	=> 'Manage Registration - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Registration',
					'scripts'		=> 'registration',
					'list' 			=>  $user->manageRegistration(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}


	public function viewregistration($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
	        $check = $user->check_query(MEMBER_REGISTRATION,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(MEMBER_REGISTRATION,"*"," id='$token' ");
	            $this->view('home/viewregistration', 
	              array(
	                    'active_menu' 	=> 'content',
						'meta_title'	=> 'View Registration - '.COMPANY_NAME,
						'page_title'  	=> 'View Registration ',
						'info'			=>	$info,
						'scripts'		=> 'content',
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}


	public function error()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>