<?php

class Home extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/index', 
				array(	
					'active_menu' 	=> 'home',
					'meta_title'  	=> 'Dashboard - '.COMPANY_NAME,
					'page_title'  	=> 'Dashboard',
					'scripts'		=> 'dashboard',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

	public function error()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}
	
	public function logout()
	{
		if(isset($_SESSION['admin_session_id'])){
			$user = $this->model('User');
			if($user->sessionOut($user_type="admin",$_SESSION['admin_session_id'])){
				unset($_SESSION['admin_session_id']);
				session_destroy();
			}	
		}
		header("Location:".COREPATH);
	}

}

?>