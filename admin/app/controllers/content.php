<?php

class Content extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$check 		= $user->check_query(CONTENT_TBL,"id"," 1 ");
			$this->view('home/managecontents', 
				array(	
					'active_menu' 	=> 'content',
					'meta_title'  	=> 'Manage Content - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Content',
					'scripts'		=> 'content',
					'check' 		=>  $check,
					'list' 			=>  $user->manageContent(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}



	public function add()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_content_key'])){
				$_SESSION['add_content_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addcontent', 
				array(	
					'active_menu' 	=> 'add_content',
					'meta_title'	=> 'Add Content - '.COMPANY_NAME,
					'page_title'  	=> 'Add Content ',
					'scripts'		=> 'add_content',	
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}


	public function edit($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_content_key'])){
				$_SESSION['edit_content_key'] = $user->generateRandomString("40");
			}
	        $check = $user->check_query(CONTENT_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(CONTENT_TBL,"*"," id='$token' ");
	            $this->view('home/editcontent', 
	              array(
	                    'active_menu' 	=> 'content',
						'meta_title'	=> 'Edit Content - '.COMPANY_NAME,
						'page_title'  	=> 'Edit Content ',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($token),
						'scripts'		=> 'content',
						'desc' 			=>  $user->publishContent($info['description']),
						'short_desc' 	=>  $user->publishContent($info['short_description']),
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}

	public function error()
	{	
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>