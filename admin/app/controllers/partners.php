<?php

class Partners extends Controller
{

	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$check 		= $user->check_query(PARTNERS_TBL,"id"," 1 ");
			$this->view('home/managepartner', 
				array(	
					'active_menu' 	=> 'partners',
					'meta_title'  	=> 'Manage Partners - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Partners',
					'scripts'		=> 'partners',
					'check' 		=>  $check,
					'list' 			=>  $user->managePartners(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}
	

	public function add()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_partner_key'])){
				$_SESSION['add_partner_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addpartner', 
				array(	
					'active_menu' 	=> 'add_partners',
					'meta_title'	=> 'Add Partner - '.COMPANY_NAME,
					'page_title'  	=> 'Add Partner ',
					'scripts'		=> 'add_partners',	
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}


	public function edit($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_partner_key'])){
				$_SESSION['edit_partner_key'] = $user->generateRandomString("40");
			}
	        $check = $user->check_query(PARTNERS_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(PARTNERS_TBL,"*"," id='$token' ");
	            $this->view('home/editpartner', 
	              array(
	                    'active_menu' 	=> 'partners',
						'meta_title'	=> 'Edit Partner - '.COMPANY_NAME,
						'page_title'  	=> 'Edit Partner ',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($token),
						'scripts'		=> 'partners',
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}


	public function error()
	{	
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>