<?php

class Tree extends Controller
{

	public function index()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$this->view('home/managetree', 
				array(	
					'active_menu' 	=> 'tree',
					'meta_title'  	=> 'Manage TREE - '.COMPANY_NAME,
					'page_title'  	=> 'Manage TREE',
					'scripts'		=> 'tree',
					'list' 			=>  $user->manageTree(),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}
	

	public function add()
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_tree_key'])){
				$_SESSION['add_tree_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addtree', 
				array(	
					'active_menu' 	=> 'add_tree',
					'meta_title'	=> 'Add TREE - '.COMPANY_NAME,
					'page_title'  	=> 'Add TREE ',
					'scripts'		=> 'add_tree',	
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}


	public function edit($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_tree_key'])){
				$_SESSION['edit_tree_key'] = $user->generateRandomString("40");
			}
	        $check = $user->check_query(TREE_TBL,"id"," id='$token' ");
	        if($check){
	            $info 	= $user->getDetails(TREE_TBL,"*"," id='$token' ");
	            $this->view('home/edittree', 
	              array(
	                    'active_menu' 	=> 'tree',
						'meta_title'	=> 'Edit TREE - '.COMPANY_NAME,
						'page_title'  	=> 'Edit TREE ',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($token),
						'scripts'		=> 'tree',
						'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])   
	                ));
	        }else{
	        	$user = $this->model('User');
				$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->UserInfo($_SESSION["admin_session_id"])
				));
			}		
	    }else{
	    	$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
	    }
	}

	public function images($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['tree_images_key'])){
				$_SESSION['tree_images_key'] = $user->generateRandomString("40");
			}
			$check = $user->check_query(TREE_TBL,"id"," id='$token' ");
	        if($check){
				$info = $user->getDetails(TREE_TBL,"*"," id='$token'  ");
				$this->view('home/addtreeimages', 
					[	
						'active_menu' 	  	=> 'tree',
						'meta_title'	  	=> 'Add TREE Images - '.COMPANY_NAME,
						'page_title'  	  	=> 'Add TREE Images',
						'redirect'		  	=> '',
						'images'			=>	$user->getTreeImages($token),
						'token'			  	=>  $user->encryptData($token),
						'info'			  	=>  $info,
						'scripts'		  	=> 'tree',
						'user'   		  	=>  $user->userInfo($_SESSION["admin_session_id"])
					]);
			}else{
				$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'active_menu' 	=> 'awards',
						'user'   		=>  $user->userInfo($_SESSION["smaavins_admin_id"])
					]);	
			}	
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);				
	    }
	}

	public function video($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user 		= $this->model('User');
			$user_id 	= $_SESSION["admin_session_id"];
			$title		= $user->getDetails(TREE_TBL, "title", "id ='$token'");
			$this->view('home/managetreevideo', 
				array(	
					'active_menu' 	=> 'tree',
					'meta_title'  	=> 'Manage TREE - '.COMPANY_NAME,
					'page_title'  	=> 'Manage Video',
					'scripts'		=> 'tree',
					'videotoken'	=>  $token,
					'title'			=>  $title['title'],
					'list' 			=>  $user->manageTreevideo($token),
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

	public function addvideo($token="")
	{
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_tree_video_key'])){
				$_SESSION['add_tree_video_key'] = $user->generateRandomString("40");
			}
			$title		= $user->getDetails(TREE_TBL, "title", "id ='$token'");
			$this->view('home/addtreevideo', 
				array(	
					'active_menu' 	=> 'add_tree',
					'meta_title'	=> 'Add TREE  - '.COMPANY_NAME,
					'page_title'  	=> 'Add TREE Video ',
					'scripts'		=> 'add_tree',
					'title'			=>  $title['title'],
					'token'			=>  $token,
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function error()
	{	
		if(isset($_SESSION["admin_session_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				array(
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'user'   		=>  $user->userInfo($_SESSION["admin_session_id"])
				));
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				));
		}
	}

}

?>