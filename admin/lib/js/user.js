$(document).ready(function() {


    /*----------------------------
        Login Functions
    ------------------------------*/

    // User Login

    $("#login_auth").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Please Enter your Email Address",
            },
            password: {
                required: "Please Enter your Password",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=userLogin",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path;
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Edit Admin Profile

    $("#editUserProfile").validate({
        rules: {
            a_ad_name: {
                required: true
            },
            a_ad_mobile: {
                required: true,
            },
            a_ad_email: {
                required: true,
            }
        },
        messages: {
            a_ad_name: {
                required: "Please Enter your Name",
            },
            a_ad_mobile: {
                required: "Please Enter Mobile Number",
            },
            a_ad_email: {
                required: "Please Enter Your Email",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=editUserProfile",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "home?e=success";
                        //location.reload();
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });


    // Change Admin Password

    $("#changeAdminPassword").validate({
        rules: {
            password: {
                required: true
            },
            new_password: {
                required: true,
            },
            re_password: {
                required: true,
            }
        },
        messages: {
            password: {
                required: "Please Enter your Password",
            },
            new_password: {
                required: "Please Enter New Password",
            },
            re_password: {
                required: "Please Re-type Your New Password",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=changeAdminPassword",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "home?u=success";
                        //location.reload();
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });




// Delete Message

    $(".deleteContactUs").click(function() {
        var id = $(this).data("id");
        var option = $(this).data("option");
        swal({
            title: "Are you sure?",
            text: "You will Remove from this message !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {

            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deleteContactUs",
                    dataType: "html",
                    data: { result: id },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        swal.close();
                        $(".page_loading").hide();
                        if (data == 1) {
                            $("#cotact_row_"+option).remove();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Product retained", "error");
            }
        });
        return false;
    });




     /*----------------------------
        add case study Category
    ------------------------------*/

  



    $("#addEvent").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('addEvent');
            var formData = new FormData(formname);
            if (photo != "") {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#customerImage").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=addEvent",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = core_path + "events?a=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                            swal.close();
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });

            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=addEvent",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "events?a=success";
                            return true;
                        } else {
                            $(".form-error").html(data);
                            $(".form-error").show(data);
                            swal.close();
                        }
                        return false;
                    }
                });
            }
            return false;
        }
    });

     // Edit Intgredient

    $("#editEvents").validate({

        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            if (photo != "") {
                var formname = document.getElementById('editEvents');
                var formData = new FormData(formname);
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            if (photo != "") {
                                $("#customerImage").val(data[1]);
                            }
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=editEvents",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location.href = core_path + "events?e=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });
            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=editEvents",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location.href = core_path + "events?e=success";
                            return true;
                        } else {
                            $(".form-error").show();
                            $(".form-error").html(data);
                        }
                        return false;
                    }
                });
            }

            return false;
        }
    });


  // Remove Image on Edit page

    $(".removeEventImage").click(function() {
        var id = $(this).data("item");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Image !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value =  id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=removeEventImage",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            swal("Deleted!", "Image has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Cancelled", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Customer Image retained", "error");
            }
        });
        return false;
    });




    // Employee Active and Inactive

    $(".eventActive").click(function() {
        var value = $(this).data("option");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=eventActive",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    location.reload();
                } else {
                    swal("Error", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                }
            }
        });
        return false;
    });
    




     /*----------------------------
                 Content
    ------------------------------*/

    $("#addContent").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('addContent');
            var formData = new FormData(formname);
            if (photo != "") {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#customerImage").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=addContent",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = core_path + "content?a=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                            swal.close();
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });

            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=addContent",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "content?a=success";
                            return true;
                        } else {
                            $(".form-error").html(data);
                            $(".form-error").show(data);
                            swal.close();
                        }
                        return false;
                    }
                });
            }
            return false;
        }
    });



     // Edit Intgredient

    $("#editContent").validate({

        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            if (photo != "") {
                var formname = document.getElementById('editContent');
                var formData = new FormData(formname);
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            if (photo != "") {
                                $("#customerImage").val(data[1]);
                            }
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=editContent",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location.href = core_path + "content?e=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".form-error").html(data[1]);
                            return false;
                        }
                    }
                });
            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=editContent",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location.href = core_path + "content?e=success";
                            return true;
                        } else {
                            $(".form-error").show();
                            $(".form-error").html(data);
                        }
                        return false;
                    }
                });
            }

            return false;
        }
    });


  // Remove Image on Edit page

    $(".removeContentImage").click(function() {
        var id = $(this).data("item");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Image !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value =  id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=removeContentImage",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            swal("Deleted!", "Image has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Cancelled", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Image retained", "error");
            }
        });
        return false;
    });




    // Employee Active and Inactive

    $(".contentActive").click(function() {
        var value = $(this).data("option");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=contentActive",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {

                    location.reload();
                } else {
                     swal("Error", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                }
            }
        });
        return false;
    });






     /*----------------------------
                 Content
    ------------------------------*/

    $("#addLogo").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('addLogo');
            var formData = new FormData(formname);
            if (photo != "") {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#customerImage").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=addLogo",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = core_path + "logo?a=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                            swal.close();
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });

            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=addLogo",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "logo?a=success";
                            return true;
                        } else {
                            $(".form-error").html(data);
                            $(".form-error").show(data);
                            swal.close();
                        }
                        return false;
                    }
                });
            }
            return false;
        }
    });



     // Edit 

    $("#editLogo").validate({

        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            if (photo != "") {
                var formname = document.getElementById('editLogo');
                var formData = new FormData(formname);
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            if (photo != "") {
                                $("#customerImage").val(data[1]);
                            }
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=editLogo",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location.href = core_path + "logo?e=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".form-error").html(data[1]);
                            return false;
                        }
                    }
                });
            } else {
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=editLogo",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location.href = core_path + "logo?e=success";
                            return true;
                        } else {
                            $(".form-error").show();
                            $(".form-error").html(data);
                        }
                        return false;
                    }
                });
            }

            return false;
        }
    });


  // Remove Image on Edit page

    $(".removeLogoImage").click(function() {
        var id = $(this).data("item");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Image !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value =  id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=removeLogoImage",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            swal("Deleted!", "Image has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Cancelled", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Image retained", "error");
            }
        });
        return false;
    });




    // Employee Active and Inactive

    $(".logoActive").click(function() {
        var value = $(this).data("option");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=logoActive",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {

                    location.reload();
                } else {
                     swal("Error", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                }
            }
        });
        return false;
    });





    /*----------------------------
            Add Partner
    ------------------------------*/

    $("#addPartner").validate({
        rules: {
            title: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please Enter  Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('addPartner');
            var formData = new FormData(formname);
            if (photo != "") {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#customerImage").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=addPartner",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = core_path + "partners?a=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                            return false;
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".imgerr").html(data[1]);
                            return false;
                        }
                    }
                });

            } else {
                swal("Error", "Please Enter Partners Logo.", "error");
                return false;
            }
            return false;
        }
    });

    // Edit 

    $("#editPartner").validate({

        rules: {
            title: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please Enter Partner Name",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            if (photo != "") {
                var formname = document.getElementById('editPartner');
                var formData = new FormData(formname);
                $.ajax({
                    url: core_path + "resource/ajax_post_image.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            if (photo != "") {
                                $("#customerImage").val(data[1]);
                            }
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=editPartner",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location.href = core_path + "partners?e=success";
                                            return true;
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                        return false;
                                    }
                                });
                            }
                        } else {
                            $(".form-error").html(data[1]);
                            return false;
                        }
                    }
                });
            } else {
                var photo = $("#customerImage").val();
                if(photo == ""){
                    swal("Error", "Please Upload Partner Logo", "error");
                    return false;
                }
                var content = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=editPartner",
                    dataType: "html",
                    data: content,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location.href = core_path + "partners?e=success";
                            return true;
                        } else {
                            $(".form-error").show();
                            $(".form-error").html(data);
                        }
                        return false;
                    }
                });
            }

            return false;
        }
    });


    // Remove Image on Edit page

    $(".removePartnerImage").click(function() {
        var id = $(this).data("item");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Logo !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value =  id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=removePartnerImage",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            swal("Deleted!", "Logo has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Cancelled", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Logo retained", "error");
            }
        });
        return false;
    });

    // Partner Active and Inactive

    $(".partnerActive").click(function() {
        var value = $(this).data("option");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=partnerActive",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    location.reload();
                } else {
                     swal("Error", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                }
            }
        });
        return false;
    });

    // Partner Delete

    $(".partnerDelete").click(function() {
        var value = $(this).data("option");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Partner !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=partnerDelete",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            location.reload();
                        } else {
                             swal("Error", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Partner retained", "error");
            }
        });
        return false;
    });


    /*----------------------------------
                CSR Management
    ------------------------------------*/

    $("#addCsr").validate({
        rules: {
            title: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "Please Enter the title",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addCsr",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "csr?a=sucess";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Edit

    $("#editCsr").validate({
        rules: {
            title: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "Please Enter the title",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=editCsr",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "csr?e=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    //  Delete 

    $(".csrDelete").click(function() {
        var id = $(this).data("id");
        //alert(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this CSR !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=csrDelete",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "csr?e=success";
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "CSR retained", "error");
            }
        });
        return false;
    });

    //  Delete CSR Video 

    $(".csrVideoDelete").click(function() {
        var id = $(this).data("id");
        var item = $(this).data("item");
        //alert(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this CSR Video !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value = id;
            var value2 = item;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=csrVideoDelete",
                    dataType: "html",
                    data: { result: value, result2: value2  },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "csr?e=success";
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "CSR Video retained", "error");
            }
        });
        return false;
    });

    // CSR Image Section

    // upload multiple images

    $("#addCsrImage").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_multiple_file.php?page=addCsrImages",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "csr?e=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });

    $(document).ready(function(){
        $("#advideo").change(function () {
            var fileInput = document.getElementById("advideo");
            var fileUrl = URL.createObjectURL(fileInput.files[0]);
            $(".video").attr("src", fileUrl);
            $(".video-new").hide();
            $(".video-exists").show();
            $(".video-remove").show();
        });

        $("#vdvideo").change(function () {
            var fileInput = document.getElementById("vdvideo");
            var fileUrl = URL.createObjectURL(fileInput.files[0]);
            $(".video").attr("src", fileUrl);
            $(".video-new").hide();
            $(".video-exists").show();
            $(".video-remove").show();
        });
    });

    $(document).ready(function() {
        $(".video-remove").click(function(){
            const videoElement = document.querySelector('video');
            videoElement.pause();
            videoElement.removeAttribute('src');
            videoElement.load();
            $(".video-new").show();
            $(".video-exists").hide();
            $(".video-remove").hide();
        });
    });

    $("#addCsrVideo").validate({
        submitHandler: function(form) {
            var video = $("#advideo").val();
            var formname = document.getElementById('addCsrVideo');
            var formData = new FormData(formname);
            var content = $(form).serialize();
            if(video !=""){
                 $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_post_video.php",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#newVideo").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=addCsrVideo",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = core_path + "csr?p=success";
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                    }
                                });
                             }
                        } else {
                            $(".form-error").show();
                            $(".form-error").html(data[1]);
                            return false;
                        }
                    }
                });
            }else{
                swal("Cancelled", "Please Enter Video", "error");
            }
            return false;
        }
    });

    // Remove Gallery images

    $(".removeCsrImage").click(function() {
        var id = $(this).data("token");
        var option = $(this).data("option");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removeCsrImage",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    $("#pro_" + option).remove();
                    masonryInt();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
    });



    /*----------------------------------
            TREE Management
    ------------------------------------*/

    $("#addTree").validate({
        rules: {
            title: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "Please Enter the title",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addTree",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "tree?a=sucess";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Edit

    $("#editTree").validate({
        rules: {
            title: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "Please Enter the title",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=editTree",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "tree?e=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    //  Delete 

    $(".treeDelete").click(function() {
        var id = $(this).data("id");
        //alert(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this TREE !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=treeDelete",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "tree?e=success";
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Tree retained", "error");
            }
        });
        return false;
    });

    //  Delete Video

    $(".treeVideoDelete").click(function() {
        var id = $(this).data("id");
        var item = $(this).data("item");
        //alert(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this TREE Video !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            var value = id;
            var value2 = item;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=treeVideoDelete",
                    dataType: "html",
                    data: { result: value, result2: value2 },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location = core_path + "tree?e=success";
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Tree Video retained", "error");
            }
        });
        return false;
    });

    // CSR Image Section

    // upload multiple images

    $("#addTreeImage").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_multiple_file.php?page=addTreeImages",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "tree?e=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });


    $("#addTreeVideo").validate({
        submitHandler: function(form) {
            var video = $("#advideo").val();
            var formname = document.getElementById('addTreeVideo');
            var formData = new FormData(formname);
            var content = $(form).serialize();
            if(video !=""){
                 $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_post_video.php",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        data = data.split('`');
                        if (data[0] == 1) {
                            $("#newVideo").val(data[1]);
                            if (data[0] == 1) {
                                var content = $(form).serialize();
                                $.ajax({
                                    type: "POST",
                                    url: core_path + "resource/ajax_redirect.php?page=addTreeVideo",
                                    dataType: "html",
                                    data: content,
                                    beforeSend: function() {
                                        $(".page_loading").show();
                                    },
                                    success: function(data) {
                                        $(".page_loading").hide();
                                        if (data == 1) {
                                            window.location = core_path + "tree?p=success";
                                        } else {
                                            $(".form-error").html(data);
                                        }
                                    }
                                });
                             }
                        } else {
                            $(".form-error").show();
                            $(".form-error").html(data[1]);
                            return false;
                        }
                    }
                });
            }else{
                swal("Cancelled", "Please Enter Video", "error");
            }
            return false;
        }
    });

    // Remove Gallery images

    $(".removeTreeImage").click(function() {
        var id = $(this).data("token");
        var option = $(this).data("option");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removeTreeImage",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    $("#pro_" + option).remove();
                    masonryInt();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
    });



});